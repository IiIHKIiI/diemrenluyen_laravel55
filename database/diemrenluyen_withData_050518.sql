-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2018 at 04:55 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diemrenluyen`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(117, '2018_02_25_000001_create_tbl_nhomquyen', 1),
(118, '2018_02_25_000002_create_tbl_khoa', 1),
(119, '2018_02_25_000003_create_tbl_bomon_donvi', 1),
(120, '2018_02_25_000004_create_tbl_canbo', 1),
(121, '2018_02_25_000005_create_tbl_nganh', 1),
(122, '2018_02_25_000006_create_tbl_lop', 1),
(123, '2018_02_25_000007_create_tbl_loaitinhtranghoctap', 1),
(124, '2018_02_25_000008_create_tbl_loaikyluat', 1),
(125, '2018_02_25_000009_create_tbl_sinhvien', 1),
(126, '2018_02_25_000010_create_tbl_tinhtranghoctap', 1),
(127, '2018_02_25_000011_create_tbl_kyluat', 1),
(128, '2018_02_25_000012_create_users_table', 1),
(129, '2018_02_25_000013_create_password_resets_table', 1),
(130, '2018_02_25_000014_create_tbl_loaichucvu_bcs', 1),
(131, '2018_02_25_000015_create_tbl_chucvu_bcs', 1),
(132, '2018_02_25_000016_create_tbl_chucvu_quanly', 1),
(133, '2018_02_25_000017_create_tbl_quanly', 1),
(134, '2018_02_25_000018_create_tbl_phanquyen', 1),
(135, '2018_02_25_000019_create_tbl_namhoc', 1),
(136, '2018_02_25_000021_create_tbl_hocky', 1),
(137, '2018_02_25_000022_create_tbl_chitiet_cvht', 1),
(138, '2018_02_25_000023_create_tbl_thongbaochung', 1),
(139, '2018_02_25_000024_create_tbl_loaidiem', 1),
(140, '2018_02_25_000025_create_tbl_quyetdinhtieuchi', 1),
(141, '2018_02_25_000026_create_tbl_chitiettieuchi', 1),
(142, '2018_02_25_000027_create_tbl_tieuchidanhgia', 1),
(143, '2018_02_25_000028_create_tbl_thoigiandanhgia', 1),
(144, '2018_02_25_000029_create_tbl_bangdiemdanhgia', 1),
(145, '2018_02_25_000030_create_tbl_chitietdanhgia', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bangdiemdanhgia`
--

CREATE TABLE `tbl_bangdiemdanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `id_thoigiandanhgia` int(10) UNSIGNED NOT NULL,
  `trangthaichung` smallint(6) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_sv` int(11) DEFAULT NULL,
  `trangthai_sv` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_bancansu` int(11) DEFAULT NULL,
  `trangthai_bancansu` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_cvht` int(11) DEFAULT NULL,
  `trangthai_cvht` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_hoidongkhoa` int(11) DEFAULT NULL,
  `trangthai_hoidongkhoa` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_hoidongtruong` int(11) DEFAULT NULL,
  `trangthai_hoidongtruong` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_bangdiemdanhgia`
--

INSERT INTO `tbl_bangdiemdanhgia` (`id`, `id_sv`, `id_thoigiandanhgia`, `trangthaichung`, `tong_diemdanhgia_sv`, `trangthai_sv`, `tong_diemdanhgia_bancansu`, `trangthai_bancansu`, `tong_diemdanhgia_cvht`, `trangthai_cvht`, `tong_diemdanhgia_hoidongkhoa`, `trangthai_hoidongkhoa`, `tong_diemdanhgia_hoidongtruong`, `trangthai_hoidongtruong`, `created_at`, `updated_at`) VALUES
(2, 99, 1, 1, 65, 1, 65, 1, 67, 1, 65, 1, 67, 1, NULL, NULL),
(3, 100, 1, 1, 84, 1, 85, 1, 85, 1, 86, 1, 85, 1, NULL, NULL),
(4, 101, 1, 1, 53, 1, 53, 1, 53, 1, 53, 1, 53, 1, NULL, NULL),
(5, 102, 1, 1, 78, 1, 78, 1, 76, 1, 76, 1, 76, 1, NULL, NULL),
(6, 103, 1, 1, 67, 1, 67, 1, 67, 1, 67, 1, 67, 1, NULL, NULL),
(7, 104, 1, 1, 86, 1, 86, 1, 86, 1, 86, 1, 86, 1, NULL, NULL),
(8, 105, 1, 1, 65, 1, 64, 1, 64, 1, 64, 1, 64, 1, NULL, NULL),
(9, 106, 1, 1, 75, 1, 75, 1, 75, 1, 75, 1, 75, 1, NULL, NULL),
(10, 107, 1, 1, 75, 1, 75, 1, 75, 1, 75, 1, 75, 1, NULL, NULL),
(11, 108, 1, 1, 87, 1, 87, 1, 86, 1, 86, 1, 86, 1, NULL, NULL),
(12, 110, 1, 1, 92, 1, 92, 1, 92, 1, 92, 1, 92, 1, NULL, NULL),
(13, 111, 1, 1, 19, 1, 90, 1, 90, 1, 90, 1, 90, 1, NULL, '2018-04-23 10:02:57'),
(14, 112, 1, 1, 57, 1, 57, 1, 57, 1, 57, 1, 57, 1, NULL, NULL),
(15, 113, 1, 1, 87, 1, 87, 1, 87, 1, 88, 1, 88, 1, NULL, NULL),
(16, 114, 1, 1, 85, 1, 85, 1, 85, 1, 85, 1, 86, 1, NULL, NULL),
(17, 115, 1, 1, 69, 1, 69, 1, 69, 1, 69, 1, 69, 1, NULL, NULL),
(18, 116, 1, 1, 66, 1, 66, 1, 66, 1, 66, 1, 66, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bomon_donvi`
--

CREATE TABLE `tbl_bomon_donvi` (
  `id` int(10) UNSIGNED NOT NULL,
  `mabomon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenbomon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tructhuoc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_bomon_donvi`
--

INSERT INTO `tbl_bomon_donvi` (`id`, `mabomon`, `tenbomon`, `tructhuoc`) VALUES
(1, 'BM.GD-QP', 'Bộ môn Giáo dục Quốc Phòng', 1),
(2, 'BM.GD-TC', 'Bộ môn Giáo dục Thể chất', 1),
(3, 'P.QLKH-HTQT', 'Phòng Quản lý Khoa học và Hợp tác quốc tế', 1),
(4, 'P.HCTH', 'Phòng Hành chính Tổng hợp', 1),
(5, 'P.ĐT', 'Phòng Đào tạo', 1),
(6, 'P.KH-TV', 'Phòng Kế hoạch Tài vụ', 1),
(7, 'P.TC-CT', 'Phòng Tổ chức Chính trị', 1),
(8, 'P.QT-TB', 'Phòng Quản trị Thiết bị', 1),
(9, 'P.KT-KDCL', 'Phòng Kháo thí và Kiểm định chất lượng', 1),
(10, 'P.CT-SV', 'Phòng Công tác sinh viên', 1),
(11, 'P.TT-PC', 'Phòng Thanh tra Pháp chế', 1),
(12, 'TV', 'Thư viện', 1),
(13, 'TTTH', 'Trung tâm Tin học', 1),
(14, 'TTNN', 'Trung tâm Ngoại ngữ', 1),
(15, 'NCKH-XHNV', 'Trung tâm nghiên cứu Khoa học xã hội và Nhân văn', 1),
(16, 'PTNT', 'Trung tâm Phát triển nông thôn', 1),
(17, 'TNNL-PTCD', 'Trung tậm Tạo nguồn Nhân lực và Phát triển Cộng đồng', 1),
(18, 'BGH', 'Ban Giám hiệu', 1),
(19, 'TTDVKTX', 'Trung tâm Dịch vụ Ký túc xá', 1),
(20, 'PTTH-SP', 'Trường Phổ thông Thực hành Sư phạm', 1),
(21, 'BM.CNTT', 'Bộ Môn Công Nghệ Thông Tin', 5),
(22, 'BM.KTPM', 'Bộ Môn Kỹ Thuật Phần Mềm', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_canbo`
--

CREATE TABLE `tbl_canbo` (
  `id` int(10) UNSIGNED NOT NULL,
  `macanbo` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotencanbo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioitinh` tinyint(1) NOT NULL,
  `ngaysinh` date NOT NULL,
  `cmnd` bigint(20) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdt` bigint(20) NOT NULL,
  `diachi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_bomon_donvi` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_canbo`
--

INSERT INTO `tbl_canbo` (`id`, `macanbo`, `hotencanbo`, `gioitinh`, `ngaysinh`, `cmnd`, `email`, `sdt`, `diachi`, `id_bomon_donvi`) VALUES
(1, '0503', 'Đoàn Thanh Nghị', 0, '1976-06-04', 123456789, 'dtnghi@agu.edu.vn', 939222535, 'An Giang', 21),
(2, '0505', 'Nguyễn Văn Hòa', 0, '1974-05-28', 123456790, 'nvhoa@agu.edu.vn', 1238024499, 'An Giang', 21),
(3, '0514', 'Trương Thị Diễm', 1, '1979-06-27', 123456791, 'ttdiem@agu.edu.vn', 913115360, 'An Giang', 21),
(4, '0516', 'Nguyễn Quang Huy', 0, '1976-01-09', 123456792, 'nqhuy@agu.edu.vn', 0, 'An Giang', 21),
(5, '0517', 'Lê Thị Minh Nguyệt', 1, '1978-12-12', 123456793, 'ltmnguyet@agu.edu.vn', 988965947, 'An Giang', 21),
(6, '0518', 'Nguyễn Thị Lan Quyên', 1, '1979-03-21', 123456794, 'ntlquyen@agu.edu.vn', 0, 'An Giang', 21),
(7, '0521', 'Lê Văn Toán', 0, '1977-10-04', 123456796, 'lvtoan@agu.edu.vn', 0, 'An Giang', 21);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chitietdanhgia`
--

CREATE TABLE `tbl_chitietdanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_bangdiemdanhgia` int(10) UNSIGNED NOT NULL,
  `id_tieuchi` int(10) UNSIGNED NOT NULL,
  `diem` int(11) NOT NULL,
  `filename_minhchung` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loaiuserdanhgia` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_chitietdanhgia`
--

INSERT INTO `tbl_chitietdanhgia` (`id`, `id_user`, `id_bangdiemdanhgia`, `id_tieuchi`, `diem`, `filename_minhchung`, `loaiuserdanhgia`, `created_at`, `updated_at`) VALUES
(1, 110, 13, 2, 2, NULL, 5, '2018-04-21 04:14:20', '2018-04-21 04:14:20'),
(7, 110, 13, 3, 2, NULL, 5, NULL, NULL),
(9, 111, 14, 2, 2, NULL, 5, NULL, NULL),
(10, 111, 14, 6, 4, NULL, 5, NULL, NULL),
(11, 110, 13, 4, 2, NULL, 5, NULL, NULL),
(18, 110, 13, 2, 2, NULL, 1, '2018-04-23 10:02:57', '2018-04-23 10:02:57'),
(19, 110, 13, 3, 2, NULL, 1, '2018-04-23 10:02:57', '2018-04-23 10:02:57'),
(20, 110, 13, 4, 2, NULL, 1, '2018-04-23 10:02:57', '2018-04-23 10:02:57'),
(21, 110, 13, 8, 8, NULL, 1, '2018-04-23 10:02:57', '2018-04-23 10:02:57'),
(22, 110, 13, 10, 2, NULL, 1, '2018-04-23 10:02:57', '2018-04-23 10:02:57'),
(23, 110, 13, 11, 2, NULL, 1, '2018-04-23 10:02:57', '2018-04-23 10:02:57'),
(24, 110, 13, 14, 1, 'MinhChung_Example.pdf', 1, '2018-04-23 10:02:57', '2018-04-23 10:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chitiettieuchi`
--

CREATE TABLE `tbl_chitiettieuchi` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_quyetdinhtieuchi` int(10) UNSIGNED NOT NULL,
  `chimuc_tieuchi` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tentieuchi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `khongdanhgia` tinyint(1) NOT NULL,
  `id_tieuchicha` int(10) UNSIGNED DEFAULT NULL,
  `id_loaidiem` int(10) UNSIGNED DEFAULT NULL,
  `is_tieuchigoc` tinyint(1) NOT NULL DEFAULT '0',
  `can_minhchung` tinyint(1) NOT NULL DEFAULT '0',
  `diemthuong` tinyint(1) NOT NULL DEFAULT '0',
  `diemtoida` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_chitiettieuchi`
--

INSERT INTO `tbl_chitiettieuchi` (`id`, `id_quyetdinhtieuchi`, `chimuc_tieuchi`, `tentieuchi`, `khongdanhgia`, `id_tieuchicha`, `id_loaidiem`, `is_tieuchigoc`, `can_minhchung`, `diemthuong`, `diemtoida`, `created_at`, `updated_at`) VALUES
(1, 1, 'I', 'Đánh giá về ý thức học tập (tối đa 20 điểm)', 1, NULL, NULL, 1, 0, 0, 20, '2018-03-13 02:35:54', '2018-03-13 02:35:54'),
(2, 1, '1', 'Đi học đúng giờ, chuẩn bị bài đầy đủ, tích cực phát biểu trong giờ học,...', 0, 1, 2, 0, 0, 0, 2, '2018-03-13 02:36:18', '2018-03-13 02:36:18'),
(3, 1, '2', 'Không vi phạm các lỗi sau: làm mất phiếu kết quả đăng ký học phần, làm mất mật khẩu đăng nhập website ĐKHP,...', 0, 1, 2, 0, 0, 0, 2, '2018-03-13 02:36:49', '2018-03-13 02:36:49'),
(4, 1, '3', 'Thực hiện tốt việc lấy ý kiến phản hồi từ người học trực tuyến', 0, 1, 2, 0, 0, 0, 2, '2018-03-13 02:37:34', '2018-03-13 02:37:34'),
(5, 1, '4', 'Kết quả học tập:', 1, 1, NULL, 0, 0, 0, 10, '2018-03-13 02:38:00', '2018-03-13 02:38:00'),
(6, 1, '4.1', 'Có ĐTBCHT từ 5 -> cận 7 hoặc từ 2.00 -> 2.39', 0, 5, 1, 0, 0, 0, 4, '2018-03-13 02:38:28', '2018-03-13 02:38:28'),
(7, 1, '4.2', 'Có ĐTBCHT từ 7 -> cận 8 hoặc từ 2.50 -> 3.19', 0, 5, 1, 0, 0, 0, 6, '2018-03-13 02:38:52', '2018-03-13 02:38:52'),
(8, 1, '4.3', 'Có ĐTBCHT từ  -> cận 9 hoặc từ 3.20 -> 3.59', 0, 5, 1, 0, 0, 0, 8, '2018-03-13 02:41:08', '2018-03-13 02:41:08'),
(9, 1, '4.4', 'Có ĐTBCHT từ 9 -> cận 10 hoặc từ 3.60 -> 4.00', 0, 5, 1, 0, 0, 0, 10, '2018-03-13 02:48:33', '2018-03-13 02:48:33'),
(10, 1, '5', 'Tham gia tích cực Hội nghị học tốt và rèn luyện nghiệp vụ chuyên ngành từ cấp bộ môn trở lên', 0, 1, 2, 0, 0, 0, 2, '2018-03-13 02:49:00', '2018-03-13 02:49:00'),
(11, 1, '6', 'Tham gia một trong các kỳ thi từ cấp trường trở lên tổ chức; tham gia nghiên cứu khoa học, tham gia các hoạt động nghệ thuật,...', 0, 1, 2, 0, 1, 0, 2, '2018-03-13 02:49:40', '2018-03-13 02:49:40'),
(12, 1, '*', 'Điểm thưởng (được cộng nhưng tổng điểm của tiêu chí này không quá 20 điểm)', 1, 1, NULL, 0, 0, 1, 5, '2018-03-13 02:50:16', '2018-03-13 02:50:16'),
(13, 1, '1', 'Được khen thưởng về các thành tích trên, cụ thể như sau:', 1, 12, NULL, 0, 0, 0, 3, '2018-03-13 02:50:40', '2018-03-13 02:50:40'),
(14, 1, '1.1', 'Cấp khoa', 0, 13, 2, 0, 1, 0, 1, '2018-03-13 02:51:32', '2018-03-13 02:51:32'),
(15, 1, '1.2', 'Cấp trường', 0, 13, 2, 0, 1, 0, 2, '2018-03-13 02:52:15', '2018-03-13 02:52:15'),
(16, 1, '1.3', 'Cấp tỉnh hoặc tương đương', 0, 13, 2, 0, 1, 0, 3, '2018-03-13 02:52:38', '2018-03-13 02:52:38'),
(17, 1, '2', 'Tinh thần vượt khó, phấn đấu vươn lên trong học tập', 0, 12, 2, 0, 1, 0, 2, '2018-03-13 02:53:25', '2018-03-13 02:53:25'),
(18, 1, 'II', 'Đánh giá về ý thức và kết quả chấp hành nội quy, quy chế trong nhà trường (tối đa 25 điêm)', 1, NULL, NULL, 1, 0, 1, 25, '2018-03-19 21:52:13', '2018-03-19 21:52:13'),
(19, 1, '1', 'Chấp hành tốt nội quy của nhà trường (căn cứ nội quy của nhà trường ban hành kèm theo):', 1, 18, NULL, 0, 0, 0, 16, '2018-03-19 21:53:24', '2018-03-19 21:53:24'),
(20, 1, '1.1', 'Phẩm chất đạo đức', 0, 19, 2, 0, 0, 0, 4, '2018-03-19 21:54:16', '2018-03-19 21:57:07'),
(21, 1, '1.2', 'Tác phong sinh viên', 0, 19, 2, 0, 0, 0, 6, '2018-03-19 21:54:46', '2018-03-19 21:56:21'),
(22, 1, '1.3', 'Bảo vệ tài sản, giữ gìn vệ sinh trong khuôn viên nhà trường', 0, 19, 2, 0, 0, 0, 6, '2018-03-19 21:55:56', '2018-03-19 21:56:29'),
(23, 1, '2', 'Chấp hành tốt Quy chế nội trú, ngoại trú', 1, 18, NULL, 0, 0, 0, 5, '2018-03-19 22:03:08', '2018-03-19 22:03:08'),
(24, 1, '2.1', 'Đăng ký với nhà trường về địa chỉ ngoại trú của mình đúng quy định', 0, 23, 2, 0, 0, 0, 2, '2018-03-19 22:04:19', '2018-03-19 22:04:19'),
(25, 1, '2.2', 'Có phiếu nhận xét của CA địa phương hoặc BQL KTX về ý thức chấp hành những quy định nơi cư trú.', 0, 23, 2, 0, 0, 0, 3, '2018-03-19 22:05:39', '2018-03-19 22:05:39'),
(26, 1, '3', 'Chấp hành  tốt Quy chế đào tạo, quy chế thi, kiểm tra (bị cảnh báo về kết quả học tập, đi thi hộ hoặc nhờ người thi hộ, ...). Nếu vi phạm, mỗi lần vi phạm trừ 2 điểm', 0, 18, 2, 0, 0, 0, 4, '2018-03-19 22:09:02', '2018-03-28 17:19:11'),
(27, 1, 'III', 'Đánh giá về ý thức và kết quả việc tham gia các hoạt động chính trị - xã hội, văn hóa, văn nghệ, thể thao và phòng chống TNXH (tối đa 20 điểm):', 1, NULL, NULL, 1, 0, 0, 20, '2018-03-19 22:10:28', '2018-03-19 22:10:28'),
(28, 1, '1', 'Tham gia có kết quả \"Tuần lễ sinh hoạt công dân HSSV\" đầu khóa hoặc các buổi báo cáo thời sự, pháp luật, chính trị (điểm SHCD x 0.5)', 0, 27, 2, 0, 0, 0, 5, '2018-03-19 22:12:07', '2018-03-19 22:12:07'),
(29, 1, '2', 'Tham gia tốt các hoạt động văn hóa văn nghệ, TDTT, CLB, đội nhóm.', 0, 27, 2, 0, 1, 0, 4, '2018-03-19 22:14:22', '2018-03-19 22:14:22'),
(30, 1, '3', 'Tham gia các hoạt động chính trị - xã hội do khoa, trường tổ chức như mít-ting, tuần hành, cổ động, tuyên truyền; tham gia các cuộc thi viết, viết bài cho Enews (2 điểm /1 hoạt động, nếu trên 4 hoạt động sẽ được xem xét cộng thêm điểm thưởng theo quy định )', 0, 27, 2, 0, 1, 0, 8, '2018-03-19 22:17:22', '2018-03-19 22:17:22'),
(31, 1, '4', 'Tham gia phòng chống các TNXH: ma túy, mại dâm, cam kết không sử dụng, tàng trữ, vận chuyển, mua bán ma túy và các chất kích thích khác.', 0, 27, 2, 0, 1, 0, 3, '2018-03-19 22:18:59', '2018-03-19 22:18:59'),
(32, 1, '*', 'Điểm thưởng (được cộng nhưng tổng số điểm của tiêu chí này không vượt quá 20 điểm)', 1, 27, NULL, 0, 0, 1, 14, '2018-03-19 22:21:23', '2018-03-19 22:21:23'),
(33, 1, '1', 'Được khen thưởng các hoạt động Hội thao, Văn nghệ, trong công tác phòng chống tệ nạn XH của nhà trường hoặc có thành tích xuất sắc trông CLB, đội nhóm, cụ thể như sau:', 1, 32, NULL, 0, 0, 0, 14, '2018-03-21 09:06:50', '2018-03-21 09:06:50'),
(34, 1, '1.1', 'Khoa khen thưởng', 0, 33, 2, 0, 1, 0, 2, '2018-03-21 09:07:35', '2018-03-21 09:07:35'),
(35, 1, '1.2', 'Trường khen thưởng', 0, 33, 2, 0, 1, 0, 3, '2018-03-21 09:08:04', '2018-03-21 09:08:04'),
(36, 1, '1.3', 'Cấp tỉnh', 0, 33, 2, 0, 1, 0, 4, '2018-03-21 09:08:37', '2018-03-21 09:08:37'),
(37, 1, '1.4', 'Cấp khu vực, toàn quốc', 0, 33, 2, 0, 1, 0, 5, '2018-03-21 09:09:07', '2018-03-21 09:09:07'),
(38, 1, 'IV', 'Đánh giá về phẩn chất công dân, quan hệ cộng đồng (tối đa 25 điểm):', 1, NULL, NULL, 1, 0, 0, 25, '2018-03-21 09:10:31', '2018-03-21 09:10:31'),
(39, 1, '1', 'Chấp hành tốt các chủ trương của Đảng, chính sách và pháp luật của Nhà nước (nếu vi phạm không được điền mục này)', 0, 38, 2, 0, 0, 0, 4, '2018-03-21 09:12:02', '2018-03-21 09:12:02'),
(40, 1, '2', 'Tích cực tham gia các hoạt động xã hội, từ thiện (đóng góp đầy đủ các loại quỹ xây dựng nhà tình nghĩa, quỹ khuyến học, Đoàn phí, Hội phí,...)', 0, 38, 2, 0, 0, 0, 5, '2018-03-21 09:15:11', '2018-03-21 09:15:11'),
(41, 1, '3', 'Tham gia các hoạt động tình nguyện vì cộng đồng (tối đa 16đ, nếu sinh viên tham gia các hoạt động vượt 16 điểm sẽ được xem xét cộngthêm điểm thưởng theo quy định):', 1, 38, NULL, 0, 0, 0, 16, '2018-03-21 09:18:00', '2018-03-21 09:18:00'),
(42, 1, '3.1', 'Hiến máu nhân đạo: 5 điểm/ 1 lần', 0, 41, 2, 0, 1, 0, 10, '2018-03-21 09:18:55', '2018-03-21 09:18:55'),
(43, 1, '3.2', 'Tham gia tình nguyện viên hỗ trợ các hoạt động của trường, tư vấn tuyển sinh, đón tân sinh viên: 4 điểm/1 lần', 0, 41, 2, 0, 1, 0, 20, '2018-03-21 09:20:34', '2018-03-21 09:20:34'),
(44, 1, '3.3', 'Chiến dịch mùa hè xanh: 10 điểm (cộng 2 học kỳ)', 0, 41, 2, 0, 1, 0, 10, '2018-03-21 09:21:54', '2018-03-21 09:21:54'),
(45, 1, '3.4', 'Tiếp sức mùa thi: 5 điểm/ 1 lần (cộng 1 học kỳ)', 0, 41, 2, 0, 1, 0, 5, '2018-03-21 09:23:27', '2018-03-21 09:23:27'),
(46, 1, '3.5', 'Công trình thanh niên: 4 điểm / 1 công trình', 0, 41, 2, 0, 1, 0, 12, '2018-03-21 09:24:09', '2018-03-21 09:24:09'),
(47, 1, '3.6', 'Tham gia sinh hoạt hè (có giấy xác nhận): 3 điểm', 0, 41, 2, 0, 1, 0, 3, '2018-03-21 09:24:58', '2018-03-28 08:57:24'),
(48, 1, '3.7', 'Trực thư viện: tối thiểu 40 giờ/ 1 học kỳ (4 điểm)', 0, 41, 2, 0, 1, 0, 4, '2018-03-21 09:25:43', '2018-03-21 09:27:38'),
(49, 1, '3.8', 'Trực đội cờ đỏ: tối thiểu 20 buổi/ 1 học kỳ', 0, 41, 2, 0, 1, 0, 4, '2018-03-21 09:26:19', '2018-03-21 09:26:19'),
(50, 1, '3.9', 'Các hoạt động tình nguyện khác: 3 điểm/ 1 hoạt động', 0, 41, 2, 0, 1, 0, 15, '2018-03-21 09:28:30', '2018-03-21 09:28:30'),
(51, 1, '*', 'Điểm thưởng (được cộng nhưng tổng số điểm của tiêu chí này không vượt quá 25 điểm)', 1, 38, NULL, 0, 0, 1, 14, '2018-03-21 09:30:07', '2018-03-21 09:30:07'),
(52, 1, '1', 'Được khen thưởng về các thành tích trên hoặc khen thưởng sao tháng giêng, sinh viên 5 tốt, cụ thể như sau:', 1, 51, NULL, 0, 0, 0, 14, '2018-03-21 09:31:07', '2018-03-21 09:31:07'),
(53, 1, '1.1', 'Cấp khoa', 0, 52, 2, 0, 1, 0, 2, '2018-03-21 09:31:40', '2018-03-21 09:31:40'),
(54, 1, '1.2', 'Cấp trường', 0, 52, 2, 0, 1, 0, 3, '2018-03-21 09:31:57', '2018-03-21 09:31:57'),
(55, 1, '1.3', 'Cấp tỉnh', 0, 52, 2, 0, 1, 0, 4, '2018-03-21 09:32:30', '2018-03-21 09:32:30'),
(56, 1, '1.4', 'Cấp khu vực, trung ương và toàn quốc', 0, 52, 2, 0, 1, 0, 5, '2018-03-21 09:33:06', '2018-03-21 09:33:06'),
(57, 1, 'V', 'Đánh giá về ý thức và kết quả tham gia phụ trách lớp, đoàn thể và các tổ chức khác trong Nhà trường (tối đa 10 điểm)', 1, NULL, NULL, 1, 0, 0, 10, '2018-03-21 09:35:09', '2018-03-21 09:35:09'),
(58, 1, '1', 'Ban cán sự lớp, Ban chấp hành chi đoàn, Ban chấp hành chi hội, thành viên đội cờ đỏ, UVBCH Đoàn Khoa, Khối hoàn thành tốt nhiệm vụ:', 0, 57, 2, 0, 0, 0, 10, '2018-03-21 09:38:31', '2018-03-21 09:38:31'),
(59, 1, '1.1', 'Ban cán sự lớp học phần, tổ trưởng và các đối tượng còn lại', 0, 58, 2, 0, 0, 0, 8, '2018-03-21 09:40:14', '2018-03-21 09:40:14'),
(60, 1, '2', 'Sinh viên có hoạt động xuất sắc căn cứ theo danh sách đề nghị của tập thể lớp (tối đa 30% sĩ số lớp)', 0, 57, 2, 0, 0, 0, 5, '2018-03-21 09:41:58', '2018-03-21 09:41:58'),
(61, 1, '3', 'Sinh viên không thuộc đối tượng ở mục 1, 2 của tiêu chí này', 0, 57, 2, 0, 0, 0, 3, '2018-03-21 09:42:56', '2018-03-21 09:42:56'),
(62, 1, '4', 'Nếu tập thể được khen thưởng từ cấp trường trở lên thì tất cả các sinh viên của lớp trừ đối tượng ở mục 1 tiêu chí này được', 0, 57, 2, 0, 1, 0, 5, '2018-03-21 09:44:33', '2018-03-21 09:44:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chitiet_cvht`
--

CREATE TABLE `tbl_chitiet_cvht` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_hockybatdau` int(10) UNSIGNED NOT NULL,
  `id_hockyketthuc` int(10) UNSIGNED NOT NULL,
  `id_lop` int(10) UNSIGNED NOT NULL,
  `id_canbo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_chitiet_cvht`
--

INSERT INTO `tbl_chitiet_cvht` (`id`, `id_hockybatdau`, `id_hockyketthuc`, `id_lop`, `id_canbo`) VALUES
(1, 1, 2, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chucvu_bcs`
--

CREATE TABLE `tbl_chucvu_bcs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `id_loaichucvu_bcs` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chucvu_quanly`
--

CREATE TABLE `tbl_chucvu_quanly` (
  `id` int(10) UNSIGNED NOT NULL,
  `machucvu_quanly` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenchucvu_quanly` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_chucvu_quanly`
--

INSERT INTO `tbl_chucvu_quanly` (`id`, `machucvu_quanly`, `tenchucvu_quanly`) VALUES
(1, 'TK', 'Trưởng Khoa'),
(2, 'PK', 'Phó Khoa'),
(3, 'TBM', 'Trưởng Bộ Môn'),
(4, 'PBM', 'Phó Bộ Môn'),
(5, 'VT', 'Văn Thư'),
(6, 'KTV', 'Kỹ Thuật Viên'),
(7, 'CV', 'Chuyên Viên'),
(8, 'GV', 'Giảng Viên');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hocky`
--

CREATE TABLE `tbl_hocky` (
  `id` int(10) UNSIGNED NOT NULL,
  `mahocky` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgbatdau` date NOT NULL,
  `tgketthuc` date NOT NULL,
  `id_namhoc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_hocky`
--

INSERT INTO `tbl_hocky` (`id`, `mahocky`, `tgbatdau`, `tgketthuc`, `id_namhoc`) VALUES
(1, 'Học Kỳ I', '2017-07-01', '2018-01-01', 1),
(2, 'Học Kỳ II', '2018-01-15', '2018-06-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_khoa`
--

CREATE TABLE `tbl_khoa` (
  `id` int(10) UNSIGNED NOT NULL,
  `makhoa` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenkhoa` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_khoa`
--

INSERT INTO `tbl_khoa` (`id`, `makhoa`, `tenkhoa`) VALUES
(1, 'TRUONG', 'Trường'),
(2, 'K.SP', 'Khoa Sư Phạm'),
(3, 'K.NN-TN-TN', 'Khoa Nông Nghiệp - Tài Nguyên Thiên Nhiên'),
(4, 'K.KT-QT-KD', 'Khoa Kinh Tế - Quản Trị Kinh Doanh'),
(5, 'K.CNTT', 'Khoa Công Nghệ Thông tin'),
(6, 'K.KT-CN-MT', 'Khoa Kỹ Thuật - Công Nghệ - Môi Trường'),
(7, 'K.NN', 'Khoa Ngoại Ngữ'),
(8, 'K.L-KH-CT', 'Khoa Luật Và Khoa Học Chính Trị'),
(9, 'K.DL-VH-NT', 'Khoa Du Lịch Và Văn Hóa - Nghệ Thuật');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kyluat`
--

CREATE TABLE `tbl_kyluat` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `id_loaikyluat` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `tgbatdau` date DEFAULT NULL,
  `tgketthuc` date DEFAULT NULL,
  `filename_quyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_kyluat`
--

INSERT INTO `tbl_kyluat` (`id`, `id_sv`, `id_loaikyluat`, `trangthai`, `tgbatdau`, `tgketthuc`, `filename_quyetdinh`, `created_at`, `updated_at`) VALUES
(3, 99, 1, 1, NULL, NULL, 'New Text Document.pdf', '2018-05-05 08:55:35', '2018-05-05 08:55:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaichucvu_bcs`
--

CREATE TABLE `tbl_loaichucvu_bcs` (
  `id` int(10) UNSIGNED NOT NULL,
  `machucvu_bcs` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenchucvu_bcs` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_loaichucvu_bcs`
--

INSERT INTO `tbl_loaichucvu_bcs` (`id`, `machucvu_bcs`, `tenchucvu_bcs`) VALUES
(1, 'BT', 'Bí Thư'),
(2, 'PBT', 'Phó Bí Thư'),
(3, 'LT', 'Lớp Trưởng'),
(4, 'CHT', 'Chi Hội Trưởng'),
(5, 'CHP', 'Chi Hội Phó'),
(6, 'LPHT', 'Lớp Phó Học Tập'),
(7, 'LPVT', 'Lớp Phó Văn Thể'),
(8, 'LPĐS', 'Lớp Phó Đời Sống'),
(9, 'UV', 'Ủy Viên');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaidiem`
--

CREATE TABLE `tbl_loaidiem` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenloaidiem` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_loaidiem`
--

INSERT INTO `tbl_loaidiem` (`id`, `tenloaidiem`) VALUES
(1, 'Chọn'),
(2, 'Điền');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaikyluat`
--

CREATE TABLE `tbl_loaikyluat` (
  `id` int(10) UNSIGNED NOT NULL,
  `maloai_kyluat` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenloai_kyluat` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_loaikyluat`
--

INSERT INTO `tbl_loaikyluat` (`id`, `maloai_kyluat`, `tenloai_kyluat`) VALUES
(1, 'KT', 'Khiển trách'),
(2, 'CC', 'Cảnh cáo học vụ'),
(3, 'ĐC', 'Đình chỉ học'),
(4, 'BTH', 'Buộc thôi học ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaitinhtranghoctap`
--

CREATE TABLE `tbl_loaitinhtranghoctap` (
  `id` int(10) UNSIGNED NOT NULL,
  `maloai_tthoctap` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenloai_tthoctap` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_loaitinhtranghoctap`
--

INSERT INTO `tbl_loaitinhtranghoctap` (`id`, `maloai_tthoctap`, `tenloai_tthoctap`) VALUES
(1, 'DH', 'Đang học'),
(2, 'BL', 'Bảo lưu'),
(3, 'TH', 'Thôi học');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lop`
--

CREATE TABLE `tbl_lop` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenlop` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nambatdau` year(4) NOT NULL,
  `namketthuc` year(4) NOT NULL,
  `hedaotao` tinyint(1) NOT NULL,
  `id_nganh` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_lop`
--

INSERT INTO `tbl_lop` (`id`, `tenlop`, `nambatdau`, `namketthuc`, `hedaotao`, `id_nganh`) VALUES
(1, 'DH15TH1', 2014, 2018, 1, 1),
(2, 'DH15TH2', 2014, 2018, 1, 1),
(3, 'DH15PM', 2014, 2018, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_namhoc`
--

CREATE TABLE `tbl_namhoc` (
  `id` int(10) UNSIGNED NOT NULL,
  `manamhoc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_namhoc`
--

INSERT INTO `tbl_namhoc` (`id`, `manamhoc`) VALUES
(1, '2018 - 2019');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nganh`
--

CREATE TABLE `tbl_nganh` (
  `id` int(10) UNSIGNED NOT NULL,
  `manganh` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tennganh` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_bomon_donvi` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_nganh`
--

INSERT INTO `tbl_nganh` (`id`, `manganh`, `tennganh`, `id_bomon_donvi`) VALUES
(1, 'CNTT', 'Ngành Công Nghệ Thông Tin', 21),
(2, 'KTPM', 'Ngành Kỹ Thuật Phần Mềm', 22);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nhomquyen`
--

CREATE TABLE `tbl_nhomquyen` (
  `id` int(10) UNSIGNED NOT NULL,
  `manhomquyen` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tennhomquyen` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_nhomquyen`
--

INSERT INTO `tbl_nhomquyen` (`id`, `manhomquyen`, `tennhomquyen`) VALUES
(1, 'SV', 'Sinh viên'),
(2, 'BCS', 'Ban cán sự'),
(3, 'CVHT', 'Cố vấn học tập'),
(4, 'HDKHOA', 'Hội đồng khoa'),
(5, 'HDTRUONG', 'Hội đồng trường'),
(6, 'PCTSV', 'Phòng CTSV'),
(7, 'DONVIKHAC', 'Đơn vị khác'),
(8, 'ADMIN', 'Người quản trị hệ thống');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_phanquyen`
--

CREATE TABLE `tbl_phanquyen` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_nhomquyen` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_phanquyen`
--

INSERT INTO `tbl_phanquyen` (`id`, `id_user`, `id_nhomquyen`, `trangthai`) VALUES
(1, 1, 8, 1),
(98, 98, 1, 1),
(99, 99, 1, 1),
(100, 100, 1, 1),
(101, 101, 1, 1),
(102, 102, 1, 1),
(103, 103, 1, 1),
(104, 104, 1, 1),
(105, 105, 1, 1),
(106, 106, 1, 1),
(107, 107, 1, 1),
(108, 108, 1, 1),
(109, 109, 1, 1),
(111, 111, 1, 1),
(112, 112, 1, 1),
(113, 113, 1, 1),
(114, 114, 1, 1),
(115, 115, 1, 1),
(116, 116, 1, 1),
(117, 117, 1, 1),
(118, 118, 1, 1),
(119, 119, 1, 1),
(120, 120, 1, 1),
(121, 121, 1, 1),
(122, 122, 1, 1),
(123, 123, 1, 1),
(124, 124, 1, 1),
(125, 125, 1, 1),
(126, 126, 1, 1),
(127, 127, 1, 1),
(128, 128, 1, 1),
(129, 129, 1, 1),
(130, 130, 1, 1),
(131, 131, 1, 1),
(132, 132, 1, 1),
(133, 133, 1, 1),
(134, 134, 1, 1),
(135, 135, 1, 1),
(136, 136, 1, 1),
(137, 137, 1, 1),
(138, 138, 1, 1),
(139, 139, 1, 1),
(140, 140, 1, 1),
(141, 141, 1, 1),
(143, 142, 4, 1),
(144, 110, 1, 1),
(145, 110, 2, 1),
(146, 143, 5, 1),
(147, 146, 3, 1),
(148, 144, 6, 1),
(149, 147, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quanly`
--

CREATE TABLE `tbl_quanly` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_canbo` int(10) UNSIGNED NOT NULL,
  `id_chucvu_quanly` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_quanly`
--

INSERT INTO `tbl_quanly` (`id`, `id_canbo`, `id_chucvu_quanly`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-04-14 06:28:14', '2018-04-14 06:28:14'),
(2, 2, 2, '2018-04-14 06:28:15', '2018-04-14 06:28:15'),
(3, 3, 8, '2018-04-14 06:28:17', '2018-04-14 06:28:17'),
(4, 4, 8, '2018-04-14 06:28:17', '2018-04-14 06:28:17'),
(5, 5, 8, '2018-04-14 06:28:18', '2018-04-14 06:28:18'),
(6, 6, 8, '2018-04-14 06:28:18', '2018-04-14 06:28:18'),
(7, 7, 8, '2018-04-14 06:28:18', '2018-04-14 06:28:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quyetdinhtieuchi`
--

CREATE TABLE `tbl_quyetdinhtieuchi` (
  `id` int(10) UNSIGNED NOT NULL,
  `maquyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename_quyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_quyetdinhtieuchi`
--

INSERT INTO `tbl_quyetdinhtieuchi` (`id`, `maquyetdinh`, `filename_quyetdinh`, `created_at`, `updated_at`) VALUES
(1, '162015QĐ-BGDĐT', '162015QĐ-BGDĐT.pdf', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sinhvien`
--

CREATE TABLE `tbl_sinhvien` (
  `id` int(10) UNSIGNED NOT NULL,
  `mssv` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hoten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioitinh` tinyint(1) NOT NULL,
  `ngaysinh` date NOT NULL,
  `cmnd` bigint(20) NOT NULL,
  `sdt_canhan` bigint(20) NOT NULL,
  `sdt_giadinh` bigint(20) DEFAULT NULL,
  `diachi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_lop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_sinhvien`
--

INSERT INTO `tbl_sinhvien` (`id`, `mssv`, `hoten`, `gioitinh`, `ngaysinh`, `cmnd`, `sdt_canhan`, `sdt_giadinh`, `diachi`, `email`, `id_lop`) VALUES
(99, 'DTH146716', 'Phan Văn Chiều', 0, '1995-04-27', 352358241, 988766378, NULL, 'Unknown', 'phanvanchieucntt@gmail.com', 2),
(100, 'DTH146720', 'Nguyễn Phạm Duy', 0, '1996-07-01', 352356776, 965420640, NULL, 'Chợ Mới', 'npduy280@gmail.com', 2),
(101, 'DTH146722', 'Trương Hoàng Duy', 0, '1996-03-04', 352257588, 1639855770, NULL, 'Châu Thành', 'thduy40@gmail.com', 2),
(102, 'DTH146723', 'Phạm Huỳnh Kim Dương', 1, '1996-03-18', 352285708, 972972955, NULL, 'Long Xuyên', 'duongphk183@gmail.com', 2),
(103, 'DTH146726', 'Trần Hải Đăng', 0, '1996-07-23', 352359990, 917478833, NULL, 'Châu Thành', 'dangtran.agu@gmail.com', 2),
(104, 'DTH146731', 'Lê Thị Phương Giao', 1, '1995-12-03', 352309279, 978771450, NULL, 'Thoại Sơn', 'legiao0312@gmail.com', 2),
(105, 'DTH146734', 'Lê Trung Hậu', 0, '1996-11-23', 371715082, 1646181081, NULL, 'Phú Quốc', 'lthau.15th@gmail.com', 2),
(106, 'DTH146735', 'Lê Phước Hiền', 0, '1996-07-02', 352455772, 0, NULL, 'Chợ Mới', 'caydinh202@yahoo.com.vn', 2),
(107, 'DTH146739', 'Phạm Văn Hiếu', 0, '1995-10-09', 352343438, 986103270, NULL, 'Châu Phú', 'pvanhieu15th@gmail.com', 2),
(108, 'DTH146741', 'Trương Minh Hiếu', 0, '1996-10-03', 352350322, 1689777911, NULL, 'Chợ Mới', 'tmhieu15th@gmail.com', 2),
(109, 'DTH146746', 'Trần Quốc Huy', 0, '1996-01-06', 352341121, 1662665922, NULL, 'Tri Tôn', 'huytqcntt@gmail.com', 2),
(110, 'DTH146748', 'Nguyễn Thị Ngọc Huyền', 1, '1996-07-03', 352351202, 1262888878, NULL, 'Thoại Sơn', 'lengochuyen291097@gmail.com', 2),
(111, 'DTH146751', 'Trần Dương Hoàng Khải', 0, '1996-10-29', 352216896, 976995081, NULL, 'Tịnh Biên', 'tdhk96@gmail.com', 2),
(112, 'DTH146753', 'Mai Thành Khang', 0, '1996-09-26', 352477049, 1664087194, NULL, 'Thoại Sơn', 'mtkhang113@gmail.com', 2),
(113, 'DTH146754', 'Nguyễn Đăng Khoa', 0, '1996-11-13', 352320867, 968061146, NULL, 'Chợ Mới', 'gasuytu1311@gmail.com', 2),
(114, 'DTH146757', 'Lê Thị Trúc Linh', 1, '1996-10-31', 352396173, 1645373419, NULL, 'Tịnh Biên', 'lttlinh.agu@gmail.com', 2),
(115, 'DTH146770', 'Phùng Công Nguyên', 0, '1996-04-22', 352453008, 973588927, NULL, 'Long Xuyên', 'pcnguyenlxag02@gmail.com', 2),
(116, 'DTH146771', 'Đoàn Thanh Nhân', 0, '1996-07-22', 352280201, 888207897, NULL, 'Long Xuyên', 'dtnhan1996@gmail.com', 2),
(117, 'DTH146775', 'Bùi Thanh Phong', 0, '1996-08-20', 352353003, 987124091, NULL, 'Long Xuyên', 'thanhphonglx96@gmail.com', 2),
(118, 'DTH146782', 'Trần Tiến Phương', 1, '1996-12-02', 352345052, 988592717, NULL, 'Long Xuyên', 'nhthai96@gmail.com', 2),
(119, 'DTH146785', 'Lâm Quí', 0, '1996-01-30', 352285230, 948919936, NULL, 'Long Xuyên', 'lqui_15th@agu.edu.vn', 2),
(120, 'DTH146787', 'Nguyễn Thị Linh Sang', 1, '1996-10-18', 352349463, 1647400811, NULL, 'Thoại Sơn', 'ntlsang15th@gmail.com', 2),
(121, 'DTH146791', 'Lâm Hoàng Sơn', 0, '1996-10-30', 352337407, 1642904464, NULL, 'Thoại Sơn', 'lhson3010@gmail.com', 2),
(122, 'DTH146796', 'Nguyễn Hồng Thái', 0, '1996-06-09', 352258540, 988592717, NULL, 'Tịnh Biên', 'nhthai96@gmail.com', 2),
(123, 'DTH146799', 'Trương Hòa Thạnh', 0, '1996-06-16', 352227572, 985323932, NULL, 'Chợ Mới', 'ththanh15th@gmail.com', 2),
(124, 'DTH146802', 'Phan Hoàng Thịnh', 0, '1996-02-09', 352345631, 907039772, NULL, 'Long Xuyên', 'phanhoangthinh1996@gmail.com', 2),
(125, 'DTH146807', 'Hồ Thị Anh Thư', 1, '1996-11-25', 352418359, 907041691, NULL, 'Long Xuyên', 'hoanhthu2511@gmail.com', 2),
(126, 'DTH146812', 'Hồ Tân Tiến', 0, '1996-07-10', 352285140, 927776366, NULL, 'Long Xuyên', 'tantien1007@gmail.com', 2),
(127, 'DTH146815', 'Trần Thị Thùy Trang', 1, '1996-10-18', 371734378, 1686490851, NULL, 'Kiên Lương', 'tranthithuytrang181096@gmail.com', 2),
(128, 'DTH146817', 'Ngô Minh Trí', 0, '1996-02-28', 352322011, 1643053349, NULL, 'Phú Tân', '0ngominhtri0@gmail.com', 2),
(129, 'DTH146820', 'Nguyễn Thị Thu Trinh', 1, '1996-07-17', 352372784, 989419915, NULL, 'Châu Thành', 'ntttrinh170796@gmail.com', 2),
(130, 'DTH146822', 'Nguyễn Nhựt Trường', 0, '1996-12-22', 352438313, 1884899984, NULL, 'Unknown', 'nntruong1996@gmail.com', 2),
(131, 'DTH146823', 'Trương Ngọc Tự', 0, '1996-09-16', 341803783, 1225811563, NULL, 'Tân Châu', 'truongngoctudh15th@gmail.com', 2),
(132, 'DTH146824', 'Lưu Văn Vẹn', 0, '1996-01-28', 352283023, 983163497, NULL, 'Unknown', 'luuven15th@gmail.com', 2),
(133, 'DTH146825', 'Trần Thanh Việt', 0, '1996-10-11', 352315132, 972781097, NULL, 'Chợ Mới', 'vietdesigner.ankhoi@gmail.com', 2),
(134, 'DTH146827', 'Trần Tuấn Vủ', 0, '1996-09-27', 352265847, 1256864727, NULL, 'Phú Tân', 'tuanvutran2112@gmail.com', 2),
(135, 'DTH147310', 'Dương Hữu Duy', 0, '1996-10-08', 352418115, 917467819, NULL, 'Long Xuyên', 'dduy3409@gmail.com', 2),
(136, 'DTH147311', 'Nguyễn Văn Nhựt Duy', 0, '1995-11-24', 352300909, 962858012, NULL, 'Châu Phú', 'duynhut0095@gmail.com', 2),
(137, 'DTH147324', 'Nguyễn Thị Phượng Hằng', 1, '1996-06-01', 352307351, 1639883628, NULL, 'Châu Phú', 'nguyenhang161996@ gmail.com', 2),
(138, 'DTH147341', 'Nguyễn Văn Linh', 0, '1995-08-19', 352427112, 888010814, NULL, 'Tân Châu', 'nvlinh912@gmail.com', 2),
(139, 'DTH147349', 'Diệp Văn Nam', 0, '1996-02-20', 352238697, 1644174627, NULL, 'Châu Đốc', 'diepnam356@gmail.com', 2),
(140, 'DTH147362', 'Huê Nhứt Sĩ', 0, '1996-08-11', 352317164, 1676321806, NULL, 'Thoại Sơn', 'huesi147362@gmail.com', 2),
(141, 'DTH147368', 'Ngô An Toàn', 0, '1996-09-09', 352266756, 962055425, NULL, 'Châu Thành', 'ngoantoancntt@gmail.com', 2),
(142, 'DTH147377', 'Vỏ Hửu Vinh', 0, '1996-06-23', 352337870, 1654059922, NULL, 'Thoại Sơn', 'huuvinh0077@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thoigiandanhgia`
--

CREATE TABLE `tbl_thoigiandanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_hocky` int(10) UNSIGNED NOT NULL,
  `id_tieuchidanhgia` int(10) UNSIGNED NOT NULL,
  `chopheptre` smallint(6) NOT NULL,
  `diemtru` smallint(6) NOT NULL,
  `tgbatdau_sv` datetime NOT NULL,
  `tgketthuc_sv` datetime NOT NULL,
  `tgbatdau_bancansu` datetime NOT NULL,
  `tgketthuc_bancansu` datetime NOT NULL,
  `tgbatdau_cvht` datetime NOT NULL,
  `tgketthuc_cvht` datetime NOT NULL,
  `tgbatdau_hoidongkhoa` datetime NOT NULL,
  `tgketthuc_hoidongkhoa` datetime NOT NULL,
  `tgbatdau_hoidongtruong` datetime NOT NULL,
  `tgketthuc_hoidongtruong` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_thoigiandanhgia`
--

INSERT INTO `tbl_thoigiandanhgia` (`id`, `id_hocky`, `id_tieuchidanhgia`, `chopheptre`, `diemtru`, `tgbatdau_sv`, `tgketthuc_sv`, `tgbatdau_bancansu`, `tgketthuc_bancansu`, `tgbatdau_cvht`, `tgketthuc_cvht`, `tgbatdau_hoidongkhoa`, `tgketthuc_hoidongkhoa`, `tgbatdau_hoidongtruong`, `tgketthuc_hoidongtruong`) VALUES
(1, 2, 1, 0, 0, '2018-04-19 16:18:48', '2018-04-20 16:18:58', '2018-04-21 16:19:01', '2018-04-22 16:19:04', '2018-04-23 16:19:07', '2018-04-24 16:19:10', '2018-04-25 15:00:00', '2018-04-26 21:48:41', '2018-04-27 21:49:02', '2018-04-28 21:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thongbaochung`
--

CREATE TABLE `tbl_thongbaochung` (
  `id` int(10) UNSIGNED NOT NULL,
  `tieude` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_hocky` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `filename_thongbao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tieuchidanhgia`
--

CREATE TABLE `tbl_tieuchidanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_quyetdinhtieuchi` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `tgbatdau` date NOT NULL,
  `tgketthuc` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_tieuchidanhgia`
--

INSERT INTO `tbl_tieuchidanhgia` (`id`, `id_quyetdinhtieuchi`, `trangthai`, `tgbatdau`, `tgketthuc`) VALUES
(1, 1, 1, '2016-01-07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tinhtranghoctap`
--

CREATE TABLE `tbl_tinhtranghoctap` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_loaitthoctap` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `trangthai` smallint(6) NOT NULL,
  `tgbatdau` date NOT NULL,
  `tgketthuc` date NOT NULL,
  `filename_quyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_tinhtranghoctap`
--

INSERT INTO `tbl_tinhtranghoctap` (`id`, `id_loaitthoctap`, `id_sv`, `trangthai`, `tgbatdau`, `tgketthuc`, `filename_quyetdinh`, `created_at`, `updated_at`) VALUES
(98, 1, 99, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:39', '2018-04-14 03:11:39'),
(99, 1, 100, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:40', '2018-04-14 03:11:40'),
(100, 1, 101, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:41', '2018-04-14 03:11:41'),
(101, 1, 102, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:41', '2018-04-14 03:11:41'),
(102, 1, 103, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:42', '2018-04-14 03:11:42'),
(103, 1, 104, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(104, 1, 105, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(105, 1, 106, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(106, 1, 107, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(107, 1, 108, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:44', '2018-04-14 03:11:44'),
(108, 1, 109, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:44', '2018-04-14 03:11:44'),
(109, 1, 110, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:44', '2018-04-14 03:11:44'),
(110, 1, 111, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:44', '2018-04-14 03:11:44'),
(111, 1, 112, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:45', '2018-04-14 03:11:45'),
(112, 1, 113, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:46', '2018-04-14 03:11:46'),
(113, 1, 114, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:46', '2018-04-14 03:11:46'),
(114, 1, 115, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:48', '2018-04-14 03:11:48'),
(115, 1, 116, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:49', '2018-04-14 03:11:49'),
(116, 1, 117, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:50', '2018-04-14 03:11:50'),
(117, 1, 118, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:52', '2018-04-14 03:11:52'),
(118, 1, 119, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:52', '2018-04-14 03:11:52'),
(119, 1, 120, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:54', '2018-04-14 03:11:54'),
(120, 1, 121, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:54', '2018-04-14 03:11:54'),
(121, 1, 122, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:55', '2018-04-14 03:11:55'),
(122, 1, 123, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:55', '2018-04-14 03:11:55'),
(123, 1, 124, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:56', '2018-04-14 03:11:56'),
(124, 1, 125, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:56', '2018-04-14 03:11:56'),
(125, 1, 126, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:56', '2018-04-14 03:11:56'),
(126, 1, 127, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(127, 1, 128, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(128, 1, 129, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(129, 1, 130, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(130, 1, 131, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(131, 1, 132, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:59', '2018-04-14 03:11:59'),
(132, 1, 133, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:11:59', '2018-04-14 03:11:59'),
(133, 1, 134, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(134, 1, 135, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(135, 1, 136, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(136, 1, 137, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(137, 1, 138, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(138, 1, 139, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(139, 1, 140, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(140, 1, 141, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(141, 1, 142, 1, '2018-04-14', '2022-04-14', NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `ms_sv_canbo` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loaiuser` smallint(6) NOT NULL,
  `isfirst` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `trangthai`, `ms_sv_canbo`, `loaiuser`, `isfirst`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '$2y$10$d6MwWDi5HyGBkryRpfXG.OAKM8X13eVJ6EUzfCRSCff5EBR1HM7t.', 1, NULL, 3, 0, NULL, '2018-04-14 02:21:52', '2018-04-14 02:21:52'),
(98, 'DTH146716', '$2y$10$xDbqcPLsO/jFzHUIan0yY.2OoqaVSgTU8SZrDGMZaLtHUIvRJIcM6', 1, 'DTH146716', 1, 1, NULL, '2018-04-14 03:11:39', '2018-04-14 03:11:39'),
(99, 'DTH146720', '$2y$10$Qsn.B5OBsBe4HIApBWB4/uoAh6KtD8g1Gr4ljF3LgOcwDo9TnKuZy', 1, 'DTH146720', 1, 1, NULL, '2018-04-14 03:11:41', '2018-04-14 03:11:41'),
(100, 'DTH146722', '$2y$10$tWGRSWgIVetkhOfaWxSw3eDFX2QwSWetso7Sjrfh3PDa99/HdxCDW', 1, 'DTH146722', 1, 1, NULL, '2018-04-14 03:11:41', '2018-04-14 03:11:41'),
(101, 'DTH146723', '$2y$10$uoNH5Iw6NC.4BM1FS.agl.02l5g3yHUhAZvUZjLenGzM.YrLhLMbG', 1, 'DTH146723', 1, 1, NULL, '2018-04-14 03:11:42', '2018-04-14 03:11:42'),
(102, 'DTH146726', '$2y$10$TKeoGQ/LYLpuZrhpxzRut.Ttp/GnqUqJkVB0jkqYVsiZ.Be/iBatu', 1, 'DTH146726', 1, 1, NULL, '2018-04-14 03:11:42', '2018-04-14 03:11:42'),
(103, 'DTH146731', '$2y$10$BWNSTWXcF72qhQejV8wku.rpR5DiRH.ijMmmuGJVDIBeGMIgkP/JC', 1, 'DTH146731', 1, 1, NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(104, 'DTH146734', '$2y$10$rRIFILgdq4GLijoR3KgAQe8EPgHGnJ2JMGcD53Qgxf5OF1E0V9x/G', 1, 'DTH146734', 1, 1, NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(105, 'DTH146735', '$2y$10$yr5a30.nDJxyVL6PUgeRx.Hwh/P/wImTt/YIfOe6WzxTv7QAVeM6O', 1, 'DTH146735', 1, 1, NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(106, 'DTH146739', '$2y$10$KQ1S9QE418bOU4cp73bkRullZIyCSqTfYHt4qAym5lm8jx6Q.taG.', 1, 'DTH146739', 1, 1, NULL, '2018-04-14 03:11:43', '2018-04-14 03:11:43'),
(107, 'DTH146741', '$2y$10$8NtmAEmIrV22QbO0lZpKOujEvahoIYF.ogO9Bxhw.RUtFrqKKlE4i', 1, 'DTH146741', 1, 1, NULL, '2018-04-14 03:11:44', '2018-04-14 03:11:44'),
(108, 'DTH146746', '$2y$10$b5UscV9qCSP6vJ0f9cdhfO0zDZxFY3QBieqqQQlZLPUQv7N4yNfim', 1, 'DTH146746', 1, 1, NULL, '2018-04-14 03:11:44', '2018-04-14 03:11:44'),
(109, 'DTH146748', '$2y$10$FDq8QkaLAxZWkz7adcUuzeVaveBFv2R5QxvbASPDfZwWOveJCzzeO', 1, 'DTH146748', 1, 1, NULL, '2018-04-14 03:11:44', '2018-04-14 03:11:44'),
(110, 'DTH146751', '$2y$10$3U2pdq4jooWVdKdMA/wnZOSw/Juk48Zh3hhxllYGkGOh2BO.1iATm', 1, 'DTH146751', 1, 0, NULL, '2018-04-14 03:11:45', '2018-04-18 01:09:01'),
(111, 'DTH146753', '$2y$10$qyrySgLmECt50hx7qV3XMONknmLQuEwckaw81V5CqBhqzMU/aCOGm', 1, 'DTH146753', 1, 1, NULL, '2018-04-14 03:11:45', '2018-04-14 03:11:45'),
(112, 'DTH146754', '$2y$10$2EsCo74Th4yym3lZ5HGDcO6sDHU4rWjjPluD35hGI058DrjXfAte6', 1, 'DTH146754', 1, 1, NULL, '2018-04-14 03:11:46', '2018-04-14 03:11:46'),
(113, 'DTH146757', '$2y$10$fyVg4NqGyAEe.JFyUpGe6uFI/I1HiYEctUWWeXS5/ISWrtTPcls0C', 1, 'DTH146757', 1, 1, NULL, '2018-04-14 03:11:47', '2018-04-14 03:11:47'),
(114, 'DTH146770', '$2y$10$WJN94uhSTZ8g.FXznBlI9et3VHbu1cjWBpjS/9mUISV9KPE5VNeAC', 1, 'DTH146770', 1, 1, NULL, '2018-04-14 03:11:48', '2018-04-14 03:11:48'),
(115, 'DTH146771', '$2y$10$iIFMZpW6jcQfxpfe336XpumsyMVoLZWR/TYJEHsECsWvh1i141upa', 1, 'DTH146771', 1, 1, NULL, '2018-04-14 03:11:50', '2018-04-14 03:11:50'),
(116, 'DTH146775', '$2y$10$nE76qh2eNvX8mXi/v0AMCeA9CigBTLjNQAAczlxz6NHShHnhpUeaq', 1, 'DTH146775', 1, 1, NULL, '2018-04-14 03:11:51', '2018-04-14 03:11:51'),
(117, 'DTH146782', '$2y$10$veoupkVPs4MP5tqKCYWtw.MsOQxkuVK3yXDjlh1IYy/aauPAVnY9W', 1, 'DTH146782', 1, 1, NULL, '2018-04-14 03:11:52', '2018-04-14 03:11:52'),
(118, 'DTH146785', '$2y$10$8P39eK9mrXMG6Qj.PoPI9uLtV7u470MLYXurCVNhOlMVj/uZdSPze', 1, 'DTH146785', 1, 1, NULL, '2018-04-14 03:11:53', '2018-04-14 03:11:53'),
(119, 'DTH146787', '$2y$10$l1zas/NUmXxn2dCde0Wc8.OT/19wVxkyVih6vPz/gxJv6mUWqrmey', 1, 'DTH146787', 1, 1, NULL, '2018-04-14 03:11:54', '2018-04-14 03:11:54'),
(120, 'DTH146791', '$2y$10$Y5se7fCdf.m.VqTdvafYoe4GObKm8o7kZbSklTz4HD.l/g7wfOq8m', 1, 'DTH146791', 1, 1, NULL, '2018-04-14 03:11:54', '2018-04-14 03:11:54'),
(121, 'DTH146796', '$2y$10$SUgyN5K9LZ2lk/l.TwqR6eGxOm2FLs2sSUXhRbjIUlkGRxp70Cv1u', 1, 'DTH146796', 1, 1, NULL, '2018-04-14 03:11:55', '2018-04-14 03:11:55'),
(122, 'DTH146799', '$2y$10$cDAhLNnboKKoXOMO3UcwnOBmPh5ySpEqJEQGW6iwWC9MfYOy.Vhru', 1, 'DTH146799', 1, 1, NULL, '2018-04-14 03:11:56', '2018-04-14 03:11:56'),
(123, 'DTH146802', '$2y$10$uB6tV5NWCQ.5qjzPMwioBOcWdP7gY7wX1HUw1LI.l32WBoFStr7iu', 1, 'DTH146802', 1, 1, NULL, '2018-04-14 03:11:56', '2018-04-14 03:11:56'),
(124, 'DTH146807', '$2y$10$Jgz.x6f7YOOqriNPVeWKzOgh4Nv.DFw4Nr1S1eRF9goZo/.7C0ocO', 1, 'DTH146807', 1, 1, NULL, '2018-04-14 03:11:56', '2018-04-14 03:11:56'),
(125, 'DTH146812', '$2y$10$Iy4WsB8JP05sMtDzmz1gHOsfqlX1UQsWtZ7BfiGVKcAk54vA2f0C6', 1, 'DTH146812', 1, 1, NULL, '2018-04-14 03:11:57', '2018-04-14 03:11:57'),
(126, 'DTH146815', '$2y$10$lxD7zY99Tch6PR8Zx6.bk.B8oS.R2gjdtbo1PIvSFSGIg7NH1n0zK', 1, 'DTH146815', 1, 1, NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(127, 'DTH146817', '$2y$10$g/NpLdJPBKzZSALDVJUWE.lQT3yBJHnyHGmShFe4RBvMYbGhEMt5K', 1, 'DTH146817', 1, 1, NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(128, 'DTH146820', '$2y$10$uPV0gmIUporMPKoxovypX.BEVq6UVzES3Kcj4S0PLijhyLrWsi3aW', 1, 'DTH146820', 1, 1, NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(129, 'DTH146822', '$2y$10$Y26xAuSyhSRbmOvyXmHaNuUHKmdo8bZWmdWkiwPUzuGfxdrfhvZhW', 1, 'DTH146822', 1, 1, NULL, '2018-04-14 03:11:58', '2018-04-14 03:11:58'),
(130, 'DTH146823', '$2y$10$0E/qC5WdRRo8He8Uihz3SOWCRLqJ.Pn/tjpiVc3qm7CfhSZPyBURa', 1, 'DTH146823', 1, 1, NULL, '2018-04-14 03:11:59', '2018-04-14 03:11:59'),
(131, 'DTH146824', '$2y$10$R60bXp5de2iX59VHN3boyePRj20a1//eARDxqLEX6IOauGnruvNIq', 1, 'DTH146824', 1, 1, NULL, '2018-04-14 03:11:59', '2018-04-14 03:11:59'),
(132, 'DTH146825', '$2y$10$u2oYSu6dPFJW1bFaWJqOteLFyw/CCuHelkFVn35p5Dd1zYwUbtrjm', 1, 'DTH146825', 1, 1, NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(133, 'DTH146827', '$2y$10$TpPC1Uf9ePa5CWizZHMcMex/0MeCp5i6dIYGBDJI1lwZf6NwWYmT2', 1, 'DTH146827', 1, 1, NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(134, 'DTH147310', '$2y$10$6AF8HjCScQ1TcDdGx3wDw.WqCdVvG05Ko9AnEDq8NukGPmwmC5Yh6', 1, 'DTH147310', 1, 1, NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(135, 'DTH147311', '$2y$10$vuyuw95NqhJACHqULSiF5emwI5lU6gd2ICRRqTcwCEPDdYXyyC0iy', 1, 'DTH147311', 1, 1, NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(136, 'DTH147324', '$2y$10$J4Y9QaV/ko4Trz045J8xq.lEYZe/fnAJS64e/VjwHtTkm39YOaJcq', 1, 'DTH147324', 1, 1, NULL, '2018-04-14 03:12:00', '2018-04-14 03:12:00'),
(137, 'DTH147341', '$2y$10$VYUH2MPXoHunLBCkH2p21eUDZZPqxlpQ4z2pm4K8NOUg3HbuYZX7y', 1, 'DTH147341', 1, 1, NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(138, 'DTH147349', '$2y$10$gy.v1lUUPrMS0zwCAzP8oOhjcXTTIIl6rjthB5J62Yi.XUy9QElUm', 1, 'DTH147349', 1, 1, NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(139, 'DTH147362', '$2y$10$q/q2vAJGW9pN.mHkTqP/AeVYDlo/w0q8OxFw7ZU4egXqHJNr7Q0Ma', 1, 'DTH147362', 1, 1, NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(140, 'DTH147368', '$2y$10$4GWmJWB8yG3b779Mz.6MNOQIHEwK83VRe4O0TS7QMc5AaXdHNxu4y', 1, 'DTH147368', 1, 1, NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(141, 'DTH147377', '$2y$10$RbwZxeMsaY58LnirQwym3.nNIpsp3TFnuEsqgm8r8EBfB9.00rQFi', 1, 'DTH147377', 1, 1, NULL, '2018-04-14 03:12:01', '2018-04-14 03:12:01'),
(142, '0503', '$2y$10$43NRWTycxFL01cDo/8AGZOkuUXmc5fq1d/m8FmIF4SAtLxgeOOAKa', 1, '0503', 2, 1, NULL, '2018-04-14 06:28:14', '2018-04-14 06:28:14'),
(143, '0505', '$2y$10$O6dFC2MdBq4azLtVRbG3MOAbk1vK9rQt.mAWSRPRM8NwEjV.4ygka', 1, '0505', 2, 1, NULL, '2018-04-14 06:28:15', '2018-04-14 06:28:15'),
(144, '0514', '$2y$10$5FLmpwrHaJa7Q4nxXKa3zeo8oPkd6opROTW5jcl1mA.KTcJksk0Yi', 1, '0514', 2, 1, NULL, '2018-04-14 06:28:16', '2018-04-14 06:28:16'),
(145, '0516', '$2y$10$wItrAisPdPShZ/jP7eWvReI1BFBY30gnDZOjTJCdD38LZBTFXNb4a', 1, '0516', 2, 1, NULL, '2018-04-14 06:28:17', '2018-04-14 06:28:17'),
(146, '0517', '$2y$10$/sRWBsfJ9Cx6tl.UdCTDQeHKxdZ1Becv6nLdtTxR4DkEZbdcpnnue', 1, '0517', 2, 1, NULL, '2018-04-14 06:28:18', '2018-04-14 06:28:18'),
(147, '0518', '$2y$10$OQ2LD8XwF560rIxdTGl3O.yB2VybPVIVXSCSE693tIzxvgWxclx06', 1, '0518', 2, 1, NULL, '2018-04-14 06:28:18', '2018-04-14 06:28:18'),
(148, '0521', '$2y$10$5jkfIqkR44G8VMV9A0tk5ejYiRc3M844s/akM5.YjSHy1Hb9Q06o.', 1, '0521', 2, 1, NULL, '2018-04-14 06:28:18', '2018-04-14 06:28:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_username_index` (`username`) USING BTREE;

--
-- Indexes for table `tbl_bangdiemdanhgia`
--
ALTER TABLE `tbl_bangdiemdanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_bangdiemdanhgia_id_sv_foreign` (`id_sv`) USING BTREE,
  ADD KEY `tbl_bangdiemdanhgia_id_thoigiandanhgia_foreign` (`id_thoigiandanhgia`) USING BTREE;

--
-- Indexes for table `tbl_bomon_donvi`
--
ALTER TABLE `tbl_bomon_donvi`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_bomon_donvi_mabomon_unique` (`mabomon`) USING BTREE,
  ADD KEY `tbl_bomon_donvi_tructhuoc_foreign` (`tructhuoc`) USING BTREE;

--
-- Indexes for table `tbl_canbo`
--
ALTER TABLE `tbl_canbo`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_canbo_macanbo_unique` (`macanbo`) USING BTREE,
  ADD KEY `tbl_canbo_id_bomon_donvi_foreign` (`id_bomon_donvi`) USING BTREE;

--
-- Indexes for table `tbl_chitietdanhgia`
--
ALTER TABLE `tbl_chitietdanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chitietdanhgia_id_user_foreign` (`id_user`) USING BTREE,
  ADD KEY `tbl_chitietdanhgia_id_tieuchi_foreign` (`id_tieuchi`) USING BTREE,
  ADD KEY `tbl_chitietdanhgia_id_bangdiemdanhgia_foreign` (`id_bangdiemdanhgia`) USING BTREE;

--
-- Indexes for table `tbl_chitiettieuchi`
--
ALTER TABLE `tbl_chitiettieuchi`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chitiettieuchi_id_loaidiem_foreign` (`id_loaidiem`) USING BTREE,
  ADD KEY `tbl_chitiettieuchi_id_quyetdinhtieuchi_foreign` (`id_quyetdinhtieuchi`) USING BTREE;

--
-- Indexes for table `tbl_chitiet_cvht`
--
ALTER TABLE `tbl_chitiet_cvht`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_lop_foreign` (`id_lop`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_canbo_foreign` (`id_canbo`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_hockybatdau_foreign` (`id_hockybatdau`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_hockyketthuc_foreign` (`id_hockyketthuc`) USING BTREE;

--
-- Indexes for table `tbl_chucvu_bcs`
--
ALTER TABLE `tbl_chucvu_bcs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chucvu_bcs_id_sv_foreign` (`id_sv`) USING BTREE,
  ADD KEY `tbl_chucvu_bcs_id_loaichucvu_bcs_foreign` (`id_loaichucvu_bcs`) USING BTREE;

--
-- Indexes for table `tbl_chucvu_quanly`
--
ALTER TABLE `tbl_chucvu_quanly`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_chucvu_quanly_machucvu_quanly_unique` (`machucvu_quanly`) USING BTREE;

--
-- Indexes for table `tbl_hocky`
--
ALTER TABLE `tbl_hocky`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_hocky_mahocky_unique` (`mahocky`) USING BTREE,
  ADD KEY `tbl_hocky_id_namhoc_foreign` (`id_namhoc`) USING BTREE;

--
-- Indexes for table `tbl_khoa`
--
ALTER TABLE `tbl_khoa`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_khoa_makhoa_unique` (`makhoa`) USING BTREE;

--
-- Indexes for table `tbl_kyluat`
--
ALTER TABLE `tbl_kyluat`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_kyluat_id_sv_foreign` (`id_sv`) USING BTREE,
  ADD KEY `tbl_kyluat_id_loaikyluat_foreign` (`id_loaikyluat`) USING BTREE;

--
-- Indexes for table `tbl_loaichucvu_bcs`
--
ALTER TABLE `tbl_loaichucvu_bcs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_loaichucvu_bcs_machucvu_bcs_unique` (`machucvu_bcs`) USING BTREE;

--
-- Indexes for table `tbl_loaidiem`
--
ALTER TABLE `tbl_loaidiem`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_loaikyluat`
--
ALTER TABLE `tbl_loaikyluat`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_loaikyluat_maloai_kyluat_unique` (`maloai_kyluat`) USING BTREE;

--
-- Indexes for table `tbl_loaitinhtranghoctap`
--
ALTER TABLE `tbl_loaitinhtranghoctap`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_loaitinhtranghoctap_maloai_tthoctap_unique` (`maloai_tthoctap`) USING BTREE;

--
-- Indexes for table `tbl_lop`
--
ALTER TABLE `tbl_lop`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_lop_tenlop_unique` (`tenlop`) USING BTREE,
  ADD KEY `tbl_lop_id_nganh_foreign` (`id_nganh`) USING BTREE;

--
-- Indexes for table `tbl_namhoc`
--
ALTER TABLE `tbl_namhoc`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_namhoc_manamhoc_unique` (`manamhoc`) USING BTREE;

--
-- Indexes for table `tbl_nganh`
--
ALTER TABLE `tbl_nganh`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_nganh_manganh_unique` (`manganh`) USING BTREE,
  ADD KEY `tbl_nganh_id_bomon_donvi_foreign` (`id_bomon_donvi`) USING BTREE;

--
-- Indexes for table `tbl_nhomquyen`
--
ALTER TABLE `tbl_nhomquyen`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_phanquyen`
--
ALTER TABLE `tbl_phanquyen`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_phanquyen_id_user_foreign` (`id_user`) USING BTREE,
  ADD KEY `tbl_phanquyen_id_nhomquyen_foreign` (`id_nhomquyen`) USING BTREE;

--
-- Indexes for table `tbl_quanly`
--
ALTER TABLE `tbl_quanly`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_quanly_id_canbo_foreign` (`id_canbo`) USING BTREE,
  ADD KEY `tbl_quanly_id_chucvu_quanly_foreign` (`id_chucvu_quanly`) USING BTREE;

--
-- Indexes for table `tbl_quyetdinhtieuchi`
--
ALTER TABLE `tbl_quyetdinhtieuchi`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_quyetdinhtieuchi_maquyetdinh_unique` (`maquyetdinh`) USING BTREE;

--
-- Indexes for table `tbl_sinhvien`
--
ALTER TABLE `tbl_sinhvien`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_sinhvien_mssv_unique` (`mssv`) USING BTREE,
  ADD KEY `tbl_sinhvien_id_lop_foreign` (`id_lop`) USING BTREE;

--
-- Indexes for table `tbl_thoigiandanhgia`
--
ALTER TABLE `tbl_thoigiandanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_thoigiandanhgia_id_hocky_unique` (`id_hocky`) USING BTREE,
  ADD KEY `tbl_thoigiandanhgia_id_tieuchidanhgia_foreign` (`id_tieuchidanhgia`) USING BTREE;

--
-- Indexes for table `tbl_thongbaochung`
--
ALTER TABLE `tbl_thongbaochung`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_thongbaochung_id_user_foreign` (`id_user`) USING BTREE,
  ADD KEY `tbl_thongbaochung_id_hocky_foreign` (`id_hocky`) USING BTREE;

--
-- Indexes for table `tbl_tieuchidanhgia`
--
ALTER TABLE `tbl_tieuchidanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_tieuchidanhgia_id_quyetdinhtieuchi_foreign` (`id_quyetdinhtieuchi`) USING BTREE;

--
-- Indexes for table `tbl_tinhtranghoctap`
--
ALTER TABLE `tbl_tinhtranghoctap`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_tinhtranghoctap_id_loaitthoctap_foreign` (`id_loaitthoctap`) USING BTREE,
  ADD KEY `tbl_tinhtranghoctap_id_sv_foreign` (`id_sv`) USING BTREE;

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_users_username_unique` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `tbl_bangdiemdanhgia`
--
ALTER TABLE `tbl_bangdiemdanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_bomon_donvi`
--
ALTER TABLE `tbl_bomon_donvi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_canbo`
--
ALTER TABLE `tbl_canbo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_chitietdanhgia`
--
ALTER TABLE `tbl_chitietdanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_chitiettieuchi`
--
ALTER TABLE `tbl_chitiettieuchi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tbl_chitiet_cvht`
--
ALTER TABLE `tbl_chitiet_cvht`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_chucvu_bcs`
--
ALTER TABLE `tbl_chucvu_bcs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_chucvu_quanly`
--
ALTER TABLE `tbl_chucvu_quanly`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_hocky`
--
ALTER TABLE `tbl_hocky`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_khoa`
--
ALTER TABLE `tbl_khoa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_kyluat`
--
ALTER TABLE `tbl_kyluat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_loaichucvu_bcs`
--
ALTER TABLE `tbl_loaichucvu_bcs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_loaidiem`
--
ALTER TABLE `tbl_loaidiem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_loaikyluat`
--
ALTER TABLE `tbl_loaikyluat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_loaitinhtranghoctap`
--
ALTER TABLE `tbl_loaitinhtranghoctap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_lop`
--
ALTER TABLE `tbl_lop`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_namhoc`
--
ALTER TABLE `tbl_namhoc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_nganh`
--
ALTER TABLE `tbl_nganh`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_nhomquyen`
--
ALTER TABLE `tbl_nhomquyen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_phanquyen`
--
ALTER TABLE `tbl_phanquyen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `tbl_quanly`
--
ALTER TABLE `tbl_quanly`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_quyetdinhtieuchi`
--
ALTER TABLE `tbl_quyetdinhtieuchi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_sinhvien`
--
ALTER TABLE `tbl_sinhvien`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `tbl_thoigiandanhgia`
--
ALTER TABLE `tbl_thoigiandanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_thongbaochung`
--
ALTER TABLE `tbl_thongbaochung`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_tieuchidanhgia`
--
ALTER TABLE `tbl_tieuchidanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_tinhtranghoctap`
--
ALTER TABLE `tbl_tinhtranghoctap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_bangdiemdanhgia`
--
ALTER TABLE `tbl_bangdiemdanhgia`
  ADD CONSTRAINT `tbl_bangdiemdanhgia_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`),
  ADD CONSTRAINT `tbl_bangdiemdanhgia_id_thoigiandanhgia_foreign` FOREIGN KEY (`id_thoigiandanhgia`) REFERENCES `tbl_thoigiandanhgia` (`id`);

--
-- Constraints for table `tbl_bomon_donvi`
--
ALTER TABLE `tbl_bomon_donvi`
  ADD CONSTRAINT `tbl_bomon_donvi_tructhuoc_foreign` FOREIGN KEY (`tructhuoc`) REFERENCES `tbl_khoa` (`id`);

--
-- Constraints for table `tbl_canbo`
--
ALTER TABLE `tbl_canbo`
  ADD CONSTRAINT `tbl_canbo_id_bomon_donvi_foreign` FOREIGN KEY (`id_bomon_donvi`) REFERENCES `tbl_bomon_donvi` (`id`);

--
-- Constraints for table `tbl_chitietdanhgia`
--
ALTER TABLE `tbl_chitietdanhgia`
  ADD CONSTRAINT `tbl_chitietdanhgia_id_bangdiemdanhgia_foreign` FOREIGN KEY (`id_bangdiemdanhgia`) REFERENCES `tbl_bangdiemdanhgia` (`id`),
  ADD CONSTRAINT `tbl_chitietdanhgia_id_tieuchi_foreign` FOREIGN KEY (`id_tieuchi`) REFERENCES `tbl_chitiettieuchi` (`id`),
  ADD CONSTRAINT `tbl_chitietdanhgia_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_chitiettieuchi`
--
ALTER TABLE `tbl_chitiettieuchi`
  ADD CONSTRAINT `tbl_chitiettieuchi_id_loaidiem_foreign` FOREIGN KEY (`id_loaidiem`) REFERENCES `tbl_loaidiem` (`id`),
  ADD CONSTRAINT `tbl_chitiettieuchi_id_quyetdinhtieuchi_foreign` FOREIGN KEY (`id_quyetdinhtieuchi`) REFERENCES `tbl_quyetdinhtieuchi` (`id`);

--
-- Constraints for table `tbl_chitiet_cvht`
--
ALTER TABLE `tbl_chitiet_cvht`
  ADD CONSTRAINT `tbl_chitiet_cvht_id_canbo_foreign` FOREIGN KEY (`id_canbo`) REFERENCES `tbl_canbo` (`id`),
  ADD CONSTRAINT `tbl_chitiet_cvht_id_hockybatdau_foreign` FOREIGN KEY (`id_hockybatdau`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_chitiet_cvht_id_hockyketthuc_foreign` FOREIGN KEY (`id_hockyketthuc`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_chitiet_cvht_id_lop_foreign` FOREIGN KEY (`id_lop`) REFERENCES `tbl_lop` (`id`);

--
-- Constraints for table `tbl_chucvu_bcs`
--
ALTER TABLE `tbl_chucvu_bcs`
  ADD CONSTRAINT `tbl_chucvu_bcs_id_loaichucvu_bcs_foreign` FOREIGN KEY (`id_loaichucvu_bcs`) REFERENCES `tbl_loaichucvu_bcs` (`id`),
  ADD CONSTRAINT `tbl_chucvu_bcs_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`);

--
-- Constraints for table `tbl_hocky`
--
ALTER TABLE `tbl_hocky`
  ADD CONSTRAINT `tbl_hocky_id_namhoc_foreign` FOREIGN KEY (`id_namhoc`) REFERENCES `tbl_namhoc` (`id`);

--
-- Constraints for table `tbl_kyluat`
--
ALTER TABLE `tbl_kyluat`
  ADD CONSTRAINT `tbl_kyluat_id_loaikyluat_foreign` FOREIGN KEY (`id_loaikyluat`) REFERENCES `tbl_loaikyluat` (`id`),
  ADD CONSTRAINT `tbl_kyluat_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`);

--
-- Constraints for table `tbl_lop`
--
ALTER TABLE `tbl_lop`
  ADD CONSTRAINT `tbl_lop_id_nganh_foreign` FOREIGN KEY (`id_nganh`) REFERENCES `tbl_nganh` (`id`);

--
-- Constraints for table `tbl_nganh`
--
ALTER TABLE `tbl_nganh`
  ADD CONSTRAINT `tbl_nganh_id_bomon_donvi_foreign` FOREIGN KEY (`id_bomon_donvi`) REFERENCES `tbl_bomon_donvi` (`id`);

--
-- Constraints for table `tbl_phanquyen`
--
ALTER TABLE `tbl_phanquyen`
  ADD CONSTRAINT `tbl_phanquyen_id_nhomquyen_foreign` FOREIGN KEY (`id_nhomquyen`) REFERENCES `tbl_nhomquyen` (`id`),
  ADD CONSTRAINT `tbl_phanquyen_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_quanly`
--
ALTER TABLE `tbl_quanly`
  ADD CONSTRAINT `tbl_quanly_id_canbo_foreign` FOREIGN KEY (`id_canbo`) REFERENCES `tbl_canbo` (`id`),
  ADD CONSTRAINT `tbl_quanly_id_chucvu_quanly_foreign` FOREIGN KEY (`id_chucvu_quanly`) REFERENCES `tbl_chucvu_quanly` (`id`);

--
-- Constraints for table `tbl_sinhvien`
--
ALTER TABLE `tbl_sinhvien`
  ADD CONSTRAINT `tbl_sinhvien_id_lop_foreign` FOREIGN KEY (`id_lop`) REFERENCES `tbl_lop` (`id`);

--
-- Constraints for table `tbl_thoigiandanhgia`
--
ALTER TABLE `tbl_thoigiandanhgia`
  ADD CONSTRAINT `tbl_thoigiandanhgia_id_hocky_foreign` FOREIGN KEY (`id_hocky`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_thoigiandanhgia_id_tieuchidanhgia_foreign` FOREIGN KEY (`id_tieuchidanhgia`) REFERENCES `tbl_tieuchidanhgia` (`id`);

--
-- Constraints for table `tbl_thongbaochung`
--
ALTER TABLE `tbl_thongbaochung`
  ADD CONSTRAINT `tbl_thongbaochung_id_hocky_foreign` FOREIGN KEY (`id_hocky`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_thongbaochung_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_tieuchidanhgia`
--
ALTER TABLE `tbl_tieuchidanhgia`
  ADD CONSTRAINT `tbl_tieuchidanhgia_id_quyetdinhtieuchi_foreign` FOREIGN KEY (`id_quyetdinhtieuchi`) REFERENCES `tbl_quyetdinhtieuchi` (`id`);

--
-- Constraints for table `tbl_tinhtranghoctap`
--
ALTER TABLE `tbl_tinhtranghoctap`
  ADD CONSTRAINT `tbl_tinhtranghoctap_id_loaitthoctap_foreign` FOREIGN KEY (`id_loaitthoctap`) REFERENCES `tbl_loaitinhtranghoctap` (`id`),
  ADD CONSTRAINT `tbl_tinhtranghoctap_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
