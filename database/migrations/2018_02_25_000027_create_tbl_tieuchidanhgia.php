<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTieuchidanhgia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tieuchidanhgia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_quyetdinhtieuchi');
            $table->boolean('trangthai');
            $table->date('tgbatdau');
            $table->date('tgketthuc')->nullable();

            $table->foreign('id_quyetdinhtieuchi')->references('id')->on('tbl_quyetdinhtieuchi');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tieuchidanhgia');
    }
}
