<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLoaichucvuBcs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_loaichucvu_bcs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('machucvu_bcs', 6)->unique();
            $table->text('tenchucvu_bcs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_loaichucvu_bcs');
    }
}
