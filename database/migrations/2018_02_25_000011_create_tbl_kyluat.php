<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKyluat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kyluat', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_sv');
            $table->unsignedInteger('id_loaikyluat');
            $table->boolean('trangthai');
            $table->date('tgbatdau')->nullable();
            $table->date('tgketthuc')->nullable();
            $table->string('filename_quyetdinh')->nullable();
            $table->timestamps();

            $table->foreign('id_sv')->references('id')->on('tbl_sinhvien');
            $table->foreign('id_loaikyluat')->references('id')->on('tbl_loaikyluat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kyluat');
    }
}
