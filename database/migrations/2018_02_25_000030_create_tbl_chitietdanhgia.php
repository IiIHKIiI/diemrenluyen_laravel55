<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblChitietdanhgia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_chitietdanhgia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_user');
            $table->unsignedInteger('id_bangdiemdanhgia');
            $table->unsignedInteger('id_tieuchi');
            $table->integer('diem');
            $table->string('filename_minhchung')->nullable();
            $table->tinyInteger('loaiuserdanhgia');
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('tbl_users');
            $table->foreign('id_tieuchi')->references('id')->on('tbl_chitiettieuchi');
            $table->foreign('id_bangdiemdanhgia')->references('id')->on('tbl_bangdiemdanhgia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_chitietdanhgia');
    }
}
