<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_lop', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('tenlop', 7)->unique();
            $table->year('nambatdau');
            $table->year('namketthuc');
            $table->boolean('hedaotao'); // 0 : Cao Đẳng | 1: Đại Học
            $table->unsignedInteger('id_nganh');

            $table->foreign('id_nganh')->references('id')->on('tbl_nganh');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_lop');
    }
}
