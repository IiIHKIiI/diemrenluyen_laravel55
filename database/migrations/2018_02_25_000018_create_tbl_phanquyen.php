<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPhanquyen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_phanquyen', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->unsignedInteger('id_user');
            $table->unsignedInteger('id_nhomquyen');
            $table->boolean('trangthai');

            $table->foreign('id_user')->references('id')->on('tbl_users');
            $table->foreign('id_nhomquyen')->references('id')->on('tbl_nhomquyen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_phanquyen');
    }
}
