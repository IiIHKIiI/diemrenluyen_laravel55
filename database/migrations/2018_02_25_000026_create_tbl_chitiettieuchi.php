<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblChitiettieuchi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_chitiettieuchi', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_quyetdinhtieuchi');
            $table->string('chimuc_tieuchi',4);
            $table->text('tentieuchi');
            $table->boolean('khongdanhgia');
            $table->unsignedInteger('id_tieuchicha')->nullable();
            $table->unsignedInteger('id_loaidiem')->nullable();
            $table->boolean('is_tieuchigoc')->default('0');
            $table->boolean('can_minhchung')->default('0');
            $table->boolean('diemthuong')->default('0');
            $table->smallInteger('diemtoida');
            $table->timestamps();

            $table->foreign('id_loaidiem')->references('id')->on('tbl_loaidiem');
            $table->foreign('id_quyetdinhtieuchi')->references('id')->on('tbl_quyetdinhtieuchi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_chitiettieuchi');
    }
}
