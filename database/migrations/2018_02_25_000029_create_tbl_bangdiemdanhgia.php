<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblBangdiemdanhgia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bangdiemdanhgia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_sv');
            $table->unsignedInteger('id_thoigiandanhgia');
            $table->smallInteger('trangthaichung')->default('0');
            $table->integer('tong_diemdanhgia_sv')->nullable();
            $table->boolean('trangthai_sv')->default('0');
            $table->integer('tong_diemdanhgia_bancansu')->nullable();
            $table->boolean('trangthai_bancansu')->default('0');
            $table->integer('tong_diemdanhgia_cvht')->nullable();
            $table->boolean('trangthai_cvht')->default('0');
            $table->integer('tong_diemdanhgia_hoidongkhoa')->nullable();
            $table->boolean('trangthai_hoidongkhoa')->default('0');
            $table->integer('tong_diemdanhgia_hoidongtruong')->nullable();
            $table->boolean('trangthai_hoidongtruong')->default('0');

            $table->timestamps();

            $table->foreign('id_sv')->references('id')->on('tbl_sinhvien');
            $table->foreign('id_thoigiandanhgia')->references('id')->on('tbl_thoigiandanhgia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_bangdiemdanhgia');
    }
}
