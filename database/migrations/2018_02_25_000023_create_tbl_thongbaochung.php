<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblThongbaochung extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_thongbaochung', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('tieude');
            $table->longtext('noidung');
            $table->unsignedInteger('id_user');
            $table->unsignedInteger('id_hocky');
            $table->boolean('trangthai');
            $table->string('filename_thongbao')->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('tbl_users');
            $table->foreign('id_hocky')->references('id')->on('tbl_hocky');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_thongbaochung');
    }
}
