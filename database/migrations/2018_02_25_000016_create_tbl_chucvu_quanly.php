<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblChucvuQuanly extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_chucvu_quanly', function (Blueprint $table) {
            $table->increments('id');
            $table->string('machucvu_quanly', 6)->unique();
            $table->text('tenchucvu_quanly');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_chucvu_quanly');
    }
}
