<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblChitietCvht extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_chitiet_cvht', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_hockybatdau');
            $table->unsignedInteger('id_hockyketthuc');
            $table->unsignedInteger('id_lop');
            $table->unsignedInteger('id_canbo');

            $table->foreign('id_lop')->references('id')->on('tbl_lop');
            $table->foreign('id_canbo')->references('id')->on('tbl_canbo');
            $table->foreign('id_hockybatdau')->references('id')->on('tbl_hocky');
            $table->foreign('id_hockyketthuc')->references('id')->on('tbl_hocky');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_chitiet_cvht');
    }
}
