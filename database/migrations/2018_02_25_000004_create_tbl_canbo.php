<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCanbo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_canbo', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('macanbo', 4)->unique();
            $table->text('hotencanbo');
            $table->boolean('gioitinh');
            $table->date('ngaysinh');
            $table->bigInteger('cmnd');
            $table->string('email');
            $table->bigInteger('sdt');
            $table->text('diachi');
            $table->unsignedInteger('id_bomon_donvi');

            $table->foreign('id_bomon_donvi')->references('id')->on('tbl_bomon_donvi');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_canbo');
    }
}
