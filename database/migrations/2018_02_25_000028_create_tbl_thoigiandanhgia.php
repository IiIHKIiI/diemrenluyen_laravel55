<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblThoigiandanhgia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_thoigiandanhgia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_hocky')->unique();
            $table->unsignedInteger('id_tieuchidanhgia');
            $table->smallInteger('chopheptre');
            $table->smallInteger('diemtru');
            $table->datetime('tgbatdau_sv');
            $table->datetime('tgketthuc_sv');
            $table->datetime('tgbatdau_bancansu');
            $table->datetime('tgketthuc_bancansu');
            $table->datetime('tgbatdau_cvht');
            $table->datetime('tgketthuc_cvht');
            $table->datetime('tgbatdau_hoidongkhoa');
            $table->datetime('tgketthuc_hoidongkhoa');
            $table->datetime('tgbatdau_hoidongtruong');
            $table->datetime('tgketthuc_hoidongtruong');

            $table->foreign('id_hocky')->references('id')->on('tbl_hocky');
            $table->foreign('id_tieuchidanhgia')->references('id')->on('tbl_tieuchidanhgia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_thoigiandanhgia');
    }
}
