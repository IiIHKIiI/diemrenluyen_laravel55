<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblChucvuBcs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_chucvu_bcs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_sv');
            $table->unsignedInteger('id_loaichucvu_bcs');
            $table->timestamps();

            $table->foreign('id_sv')->references('id')->on('tbl_sinhvien');
            $table->foreign('id_loaichucvu_bcs')->references('id')->on('tbl_loaichucvu_bcs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_chucvu_bcs');
    }
}
