<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblQuanly extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_quanly', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_canbo');
            $table->unsignedInteger('id_chucvu_quanly');
            $table->timestamps();

            $table->foreign('id_canbo')->references('id')->on('tbl_canbo');
            $table->foreign('id_chucvu_quanly')->references('id')->on('tbl_chucvu_quanly');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_quanly');
    }
}
