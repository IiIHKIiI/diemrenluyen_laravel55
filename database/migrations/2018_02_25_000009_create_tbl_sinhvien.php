<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSinhvien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sinhvien', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('mssv', 9)->unique();
            $table->text('hoten');
            $table->boolean('gioitinh');
            $table->date('ngaysinh');
            $table->bigInteger('cmnd');
            $table->bigInteger('sdt_canhan');
            $table->bigInteger('sdt_giadinh')->nullable();
            $table->text('diachi');
            $table->string('email');
            $table->unsignedInteger('id_lop');
            
            $table->foreign('id_lop')->references('id')->on('tbl_lop');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_sinhvien');
    }
}
