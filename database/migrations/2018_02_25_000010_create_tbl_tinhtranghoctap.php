<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTinhtranghoctap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tinhtranghoctap', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('id_loaitthoctap');
            $table->unsignedInteger('id_sv');
            $table->smallInteger('trangthai');
            $table->date('tgbatdau');
            $table->date('tgketthuc');
            $table->string('filename_quyetdinh')->nullable();
            $table->timestamps();

            $table->foreign('id_loaitthoctap')->references('id')->on('tbl_loaitinhtranghoctap');
            $table->foreign('id_sv')->references('id')->on('tbl_sinhvien');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tinhtranghoctap');
    }
}
