<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_chucvu_quanly_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'TK',
                'tenchucvu_quanly'  =>  'Trưởng Khoa',
                
            ]
        );
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'PK',
                'tenchucvu_quanly'  =>  'Phó Khoa',
                
            ]
        );
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'TBM',
                'tenchucvu_quanly'  =>  'Trưởng Bộ Môn',
                
            ]
        );
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'PBM',
                'tenchucvu_quanly'  =>  'Phó Bộ Môn',
                
            ]
        );
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'VT',
                'tenchucvu_quanly'  =>  'Văn Thư',
                
            ]
        );
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'KTV',
                'tenchucvu_quanly'  =>  'Kỹ Thuật Viên',
                
            ]
        );
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'CV',
                'tenchucvu_quanly'  =>  'Chuyên Viên',
                
            ]
        );
        DB::table('tbl_chucvu_quanly')->insert(
            [
                'machucvu_quanly'   =>  'GV',
                'tenchucvu_quanly'  =>  'Giảng Viên',
                
            ]
        );
    }
}
