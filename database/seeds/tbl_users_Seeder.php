<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tbl_users_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_users')->insert([
            'username' => 'Administrator',
            'password' => bcrypt('admin'),
            'trangthai' => 1,
            'ms_sv_canbo' => null,
            'loaiuser' => 3,
            'isfirst' => 0,
            'remember_token' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        // DB::table('tbl_users')->insert([
        //     'username' => 'DTH146751',
        //     'password' => bcrypt('123'),
        //     'trangthai' => 1,
        //     'ms_sv_canbo' => 'DTH146751',
        //     'loaiuser' => 1,
        //     'isfirst' => 0,
        //     'remember_token' => null,
        //     'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        // ]);

        // DB::table('tbl_users')->insert([
        //     'username' => 'DTH146723',
        //     'password' => bcrypt('123'),
        //     'trangthai' => 1,
        //     'ms_sv_canbo' => 'DTH146723',
        //     'loaiuser' => 1,
        //     'isfirst' => 0,
        //     'remember_token' => null,
        //     'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        // ]);

        // DB::table('tbl_users')->insert([
        //     'username' => 'DTH146726',
        //     'password' => bcrypt('123'),
        //     'trangthai' => 1,
        //     'ms_sv_canbo' => 'DTH146726',
        //     'loaiuser' => 1,
        //     'isfirst' => 0,
        //     'remember_token' => null,
        //     'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        // ]);
    }
}
