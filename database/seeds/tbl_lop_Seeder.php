<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_lop_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_lop')->insert(
            [
                'tenlop'     => 'DH15TH1',
                'nambatdau'  => 2014,
                'namketthuc' => 2018,
                'hedaotao'   => 1,
                'id_nganh'   => 1 
            ]
        );
        DB::table('tbl_lop')->insert(
            [
                'tenlop'     => 'DH15TH2',
                'nambatdau'  => 2014,
                'namketthuc' => 2018,
                'hedaotao'   => 1,
                'id_nganh'   => 1 
            ]
        );
        DB::table('tbl_lop')->insert(
            [
                'tenlop'     => 'DH15PM',
                'nambatdau'  => 2014,
                'namketthuc' => 2018,
                'hedaotao'   => 1,
                'id_nganh'   => 2 
            ]
        );
    }
}
