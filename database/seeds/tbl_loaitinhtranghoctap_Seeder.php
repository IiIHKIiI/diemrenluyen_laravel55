<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_loaitinhtranghoctap_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_loaitinhtranghoctap')->insert(
            [
                'maloai_tthoctap'     => 'DH',
                'tenloai_tthoctap'  => 'Đang học',
            ]
        );
        DB::table('tbl_loaitinhtranghoctap')->insert(
            [
                'maloai_tthoctap'     => 'BL',
                'tenloai_tthoctap'  => 'Bảo lưu',
            ]
        );
        DB::table('tbl_loaitinhtranghoctap')->insert(
            [
                'maloai_tthoctap'     => 'TH',
                'tenloai_tthoctap'  => 'Thôi học',
            ]
        );
    }
}
