<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_khoa_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'TRUONG',
            'tenkhoa'   => 'Trường'
        ]); 
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.SP',
            'tenkhoa'   => 'Khoa Sư Phạm'
        ]); 
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.NN-TN-TN',
            'tenkhoa'   => 'Khoa Nông Nghiệp - Tài Nguyên Thiên Nhiên'
        ]); 
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.KT-QT-KD',
            'tenkhoa'   => 'Khoa Kinh Tế - Quản Trị Kinh Doanh'
        ]); 
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.CNTT',
            'tenkhoa'   => 'Khoa Công Nghệ Thông tin'
        ]); 
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.KT-CN-MT',
            'tenkhoa'   => 'Khoa Kỹ Thuật - Công Nghệ - Môi Trường'
        ]);
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.NN',
            'tenkhoa'   => 'Khoa Ngoại Ngữ'
        ]); 
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.L-KH-CT',
            'tenkhoa'   => 'Khoa Luật Và Khoa Học Chính Trị'
        ]); 
        DB::table('tbl_khoa')->insert([
            'makhoa'    => 'K.DL-VH-NT',
            'tenkhoa'   => 'Khoa Du Lịch Và Văn Hóa - Nghệ Thuật'
        ]);
    }
}
