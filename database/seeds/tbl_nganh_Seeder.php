<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_nganh_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_nganh')->insert(
            [
                'manganh'   =>  'CNTT',
                'tennganh'  =>  'Ngành Công Nghệ Thông Tin',
                'id_bomon_donvi' =>  21
            ]
        );
        DB::table('tbl_nganh')->insert(
            [
                'manganh'   =>  'KTPM',
                'tennganh'  =>  'Ngành Kỹ Thuật Phần Mềm',
                'id_bomon_donvi' =>  22
            ]
        );
    }
}
