<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tbl_phanquyen_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tbl_phanquyen')->insert([
            'id_user' => 1,
            'id_nhomquyen' => 8,
            'trangthai' => 1, // 0: Bị khóa 1: Hoạt Động
        ]);

        // DB::table('tbl_phanquyen')->insert([
        //     'id_user' => 2,
        //     'id_nhomquyen' => 1,
        //     'trangthai' => 1, // 0: Bị khóa 1: Hoạt Động
        // ]);

        // DB::table('tbl_phanquyen')->insert([
        //     'id_user' => 3,
        //     'id_nhomquyen' => 1,
        //     'trangthai' => 1, // 0: Bị khóa 1: Hoạt Động
        // ]);

        // DB::table('tbl_phanquyen')->insert([
        //     'id_user' => 4,
        //     'id_nhomquyen' => 1,
        //     'trangthai' => 1, // 0: Bị khóa 1: Hoạt Động
        // ]);
    }
}
