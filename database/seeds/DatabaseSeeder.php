<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            tbl_nhomquyen_Seeder::class,
            tbl_khoa_Seeder::class,
            tbl_bomon_donvi_Seeder::class,
            tbl_nganh_Seeder::class,
            tbl_lop_Seeder::class,
            tbl_loaitinhtranghoctap_Seeder::class,
            tbl_loaikyluat_Seeder::class,
            tbl_sinhvien_Seeder::class,
            tbl_tinhtranghoctap_Seeder::class,
            tbl_users_Seeder::class,
            tbl_phanquyen_Seeder::class,
            tbl_loaichucvu_bcs_Seeder::class,
            tbl_chucvu_quanly_Seeder::class,
            tbl_loaidiem_Seeder::class
        ]);

    }
}
