<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_nhomquyen_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tbl_nhomquyen')->insert([
                'manhomquyen'   =>  'SV',
                'tennhomquyen'  =>  'Sinh viên'
            ]
        );
        DB::table('tbl_nhomquyen')->insert([
                'manhomquyen'   =>  'BCS',
                'tennhomquyen'  =>  'Ban cán sự'
            ]
        );
        DB::table('tbl_nhomquyen')->insert([
                'manhomquyen'   =>  'CVHT',
                'tennhomquyen'  =>  'Cố vấn học tập'
            ]
        );
        DB::table('tbl_nhomquyen')->insert([
                'manhomquyen'   =>  'HDKHOA',
                'tennhomquyen'  =>  'Hội đồng khoa'
            ]
        );
        DB::table('tbl_nhomquyen')->insert([
                'manhomquyen'   =>  'HDTRUONG',
                'tennhomquyen'  =>  'Hội đồng trường'
            ]
        );
        DB::table('tbl_nhomquyen')->insert([
                'manhomquyen'   =>  'PCTSV',
                'tennhomquyen'  =>  'Phòng CTSV'
            ]
        );
        DB::table('tbl_nhomquyen')->insert([
                'manhomquyen'   =>  'DONVIKHAC',
                'tennhomquyen'  =>  'Đơn vị khác'
            ]
        );
        DB::table('tbl_nhomquyen')->insert([
            'manhomquyen'   =>  'ADMIN',
            'tennhomquyen'  =>  'Người quản trị hệ thống'
        ]
    );
    }
}
