<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_loaichucvu_bcs_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'BT',
                'tenchucvu_bcs'  =>  'Bí Thư',
            ]
        );
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'PBT',
                'tenchucvu_bcs'  =>  'Phó Bí Thư',
            ]
        );
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'LT',
                'tenchucvu_bcs'  =>  'Lớp Trưởng',
            ]
        );
       
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'CHT',
                'tenchucvu_bcs'  =>  'Chi Hội Trưởng',
            ]
        );
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'CHP',
                'tenchucvu_bcs'  =>  'Chi Hội Phó',
            ]
        );
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'LPHT',
                'tenchucvu_bcs'  =>  'Lớp Phó Học Tập',
            ]
        );
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'LPVT',
                'tenchucvu_bcs'  =>  'Lớp Phó Văn Thể',
            ]
        );
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'LPĐS',
                'tenchucvu_bcs'  =>  'Lớp Phó Đời Sống',
            ]
        );
        DB::table('tbl_loaichucvu_bcs')->insert(
            [
                'machucvu_bcs'   =>  'UV',
                'tenchucvu_bcs'  =>  'Ủy Viên',
            ]
        );
        

    }
}
