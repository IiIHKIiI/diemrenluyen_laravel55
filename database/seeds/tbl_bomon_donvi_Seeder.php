<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tbl_bomon_donvi_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'BM.GD-QP',
                'tenbomon' => 'Bộ môn Giáo dục Quốc Phòng',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'BM.GD-TC',
                'tenbomon' => 'Bộ môn Giáo dục Thể chất',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.QLKH-HTQT',
                'tenbomon' => 'Phòng Quản lý Khoa học và Hợp tác quốc tế',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.HCTH',
                'tenbomon' => 'Phòng Hành chính Tổng hợp',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.ĐT',
                'tenbomon' => 'Phòng Đào tạo',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.KH-TV',
                'tenbomon' => 'Phòng Kế hoạch Tài vụ',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.TC-CT',
                'tenbomon' => 'Phòng Tổ chức Chính trị',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.QT-TB',
                'tenbomon' => 'Phòng Quản trị Thiết bị',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.KT-KDCL',
                'tenbomon' => 'Phòng Kháo thí và Kiểm định chất lượng',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.CT-SV',
                'tenbomon' => 'Phòng Công tác sinh viên',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'P.TT-PC',
                'tenbomon' => 'Phòng Thanh tra Pháp chế',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'TV',
                'tenbomon' => 'Thư viện',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'TTTH',
                'tenbomon' => 'Trung tâm Tin học',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'TTNN',
                'tenbomon' => 'Trung tâm Ngoại ngữ',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'NCKH-XHNV',
                'tenbomon' => 'Trung tâm nghiên cứu Khoa học xã hội và Nhân văn',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'PTNT',
                'tenbomon' => 'Trung tâm Phát triển nông thôn',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'TNNL-PTCD',
                'tenbomon' => 'Trung tậm Tạo nguồn Nhân lực và Phát triển Cộng đồng',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'BGH',
                'tenbomon' => 'Ban Giám hiệu',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'TTDVKTX',
                'tenbomon' => 'Trung tâm Dịch vụ Ký túc xá',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'PTTH-SP',
                'tenbomon' => 'Trường Phổ thông Thực hành Sư phạm',
                'tructhuoc' => 1,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'BM.CNTT',
                'tenbomon' => 'Bộ Môn Công Nghệ Thông Tin',
                'tructhuoc' => 5,
            ]
        );
        DB::table('tbl_bomon_donvi')->insert(
            [
                'mabomon' => 'BM.KTPM',
                'tenbomon' => 'Bộ Môn Kỹ Thuật Phần Mềm',
                'tructhuoc' => 5,
            ]
        );
    }
}
