<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_loaikyluat_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_loaikyluat')->insert(
            [
                'maloai_kyluat'     => 'KT',
                'tenloai_kyluat'  => 'Khiển trách',
            ]
        );
        DB::table('tbl_loaikyluat')->insert(
            [
                'maloai_kyluat'     => 'CCHV',
                'tenloai_kyluat'  => 'Cảnh cáo học vụ',
            ]
        );
        DB::table('tbl_loaikyluat')->insert(
            [
                'maloai_kyluat'     => 'ĐC',
                'tenloai_kyluat'  => 'Đình chỉ học',
            ]
        );
        DB::table('tbl_loaikyluat')->insert(
            [
                'maloai_tthoctap'     => 'BTH',
                'tenloai_tthoctap'  => 'Buộc thôi học',
            ]
        );
    }
}
