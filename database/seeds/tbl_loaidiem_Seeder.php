<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class tbl_loaidiem_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tbl_loaidiem')->insert(
            [
                'tenloaidiem'   =>  'Chọn'
            ]
        );
        DB::table('tbl_loaidiem')->insert(
            [
                'tenloaidiem'   =>  'Điền'
            ]
        );
    }
}
