-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2018 at 05:00 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diemrenluyen`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bangdiemdanhgia`
--

CREATE TABLE `tbl_bangdiemdanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `id_thoigiandanhgia` int(10) UNSIGNED NOT NULL,
  `trangthaichung` smallint(6) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_sv` int(11) DEFAULT NULL,
  `trangthai_sv` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_bancansu` int(11) DEFAULT NULL,
  `trangthai_bancansu` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_cvht` int(11) DEFAULT NULL,
  `trangthai_cvht` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_hoidongkhoa` int(11) DEFAULT NULL,
  `trangthai_hoidongkhoa` tinyint(1) NOT NULL DEFAULT '0',
  `tong_diemdanhgia_hoidongtruong` int(11) DEFAULT NULL,
  `trangthai_hoidongtruong` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bomon_donvi`
--

CREATE TABLE `tbl_bomon_donvi` (
  `id` int(10) UNSIGNED NOT NULL,
  `mabomon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenbomon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tructhuoc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_canbo`
--

CREATE TABLE `tbl_canbo` (
  `id` int(10) UNSIGNED NOT NULL,
  `macanbo` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotencanbo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioitinh` tinyint(1) NOT NULL,
  `ngaysinh` date NOT NULL,
  `cmnd` bigint(20) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdt` bigint(20) NOT NULL,
  `diachi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_bomon_donvi` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chitietdanhgia`
--

CREATE TABLE `tbl_chitietdanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_bangdiemdanhgia` int(10) UNSIGNED NOT NULL,
  `id_tieuchi` int(10) UNSIGNED NOT NULL,
  `diem` int(11) NOT NULL,
  `filename_minhchung` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loaiuserdanhgia` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chitiettieuchi`
--

CREATE TABLE `tbl_chitiettieuchi` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_quyetdinhtieuchi` int(10) UNSIGNED NOT NULL,
  `chimuc_tieuchi` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tentieuchi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `khongdanhgia` tinyint(1) NOT NULL,
  `id_tieuchicha` int(10) UNSIGNED DEFAULT NULL,
  `id_loaidiem` int(10) UNSIGNED DEFAULT NULL,
  `is_tieuchigoc` tinyint(1) NOT NULL DEFAULT '0',
  `can_minhchung` tinyint(1) NOT NULL DEFAULT '0',
  `diemthuong` tinyint(1) NOT NULL DEFAULT '0',
  `diemtoida` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chitiet_cvht`
--

CREATE TABLE `tbl_chitiet_cvht` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_hockybatdau` int(10) UNSIGNED NOT NULL,
  `id_hockyketthuc` int(10) UNSIGNED NOT NULL,
  `id_lop` int(10) UNSIGNED NOT NULL,
  `id_canbo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chucvu_bcs`
--

CREATE TABLE `tbl_chucvu_bcs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `id_loaichucvu_bcs` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chucvu_quanly`
--

CREATE TABLE `tbl_chucvu_quanly` (
  `id` int(10) UNSIGNED NOT NULL,
  `machucvu_quanly` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenchucvu_quanly` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hocky`
--

CREATE TABLE `tbl_hocky` (
  `id` int(10) UNSIGNED NOT NULL,
  `mahocky` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgbatdau` date NOT NULL,
  `tgketthuc` date NOT NULL,
  `id_namhoc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_khoa`
--

CREATE TABLE `tbl_khoa` (
  `id` int(10) UNSIGNED NOT NULL,
  `makhoa` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenkhoa` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kyluat`
--

CREATE TABLE `tbl_kyluat` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `id_loaikyluat` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `tgbatdau` date DEFAULT NULL,
  `tgketthuc` date DEFAULT NULL,
  `filename_quyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaichucvu_bcs`
--

CREATE TABLE `tbl_loaichucvu_bcs` (
  `id` int(10) UNSIGNED NOT NULL,
  `machucvu_bcs` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenchucvu_bcs` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaidiem`
--

CREATE TABLE `tbl_loaidiem` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenloaidiem` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaikyluat`
--

CREATE TABLE `tbl_loaikyluat` (
  `id` int(10) UNSIGNED NOT NULL,
  `maloai_kyluat` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenloai_kyluat` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loaitinhtranghoctap`
--

CREATE TABLE `tbl_loaitinhtranghoctap` (
  `id` int(10) UNSIGNED NOT NULL,
  `maloai_tthoctap` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenloai_tthoctap` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lop`
--

CREATE TABLE `tbl_lop` (
  `id` int(10) UNSIGNED NOT NULL,
  `tenlop` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nambatdau` year(4) NOT NULL,
  `namketthuc` year(4) NOT NULL,
  `hedaotao` tinyint(1) NOT NULL,
  `id_nganh` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_namhoc`
--

CREATE TABLE `tbl_namhoc` (
  `id` int(10) UNSIGNED NOT NULL,
  `manamhoc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nganh`
--

CREATE TABLE `tbl_nganh` (
  `id` int(10) UNSIGNED NOT NULL,
  `manganh` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tennganh` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_bomon_donvi` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nhomquyen`
--

CREATE TABLE `tbl_nhomquyen` (
  `id` int(10) UNSIGNED NOT NULL,
  `manhomquyen` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tennhomquyen` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_phanquyen`
--

CREATE TABLE `tbl_phanquyen` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_nhomquyen` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quanly`
--

CREATE TABLE `tbl_quanly` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_canbo` int(10) UNSIGNED NOT NULL,
  `id_chucvu_quanly` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quyetdinhtieuchi`
--

CREATE TABLE `tbl_quyetdinhtieuchi` (
  `id` int(10) UNSIGNED NOT NULL,
  `maquyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename_quyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sinhvien`
--

CREATE TABLE `tbl_sinhvien` (
  `id` int(10) UNSIGNED NOT NULL,
  `mssv` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hoten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioitinh` tinyint(1) NOT NULL,
  `ngaysinh` date NOT NULL,
  `cmnd` bigint(20) NOT NULL,
  `sdt_canhan` bigint(20) NOT NULL,
  `sdt_giadinh` bigint(20) DEFAULT NULL,
  `diachi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_lop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thoigiandanhgia`
--

CREATE TABLE `tbl_thoigiandanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_hocky` int(10) UNSIGNED NOT NULL,
  `id_tieuchidanhgia` int(10) UNSIGNED NOT NULL,
  `chopheptre` smallint(6) NOT NULL,
  `diemtru` smallint(6) NOT NULL,
  `tgbatdau_sv` datetime NOT NULL,
  `tgketthuc_sv` datetime NOT NULL,
  `tgbatdau_bancansu` datetime NOT NULL,
  `tgketthuc_bancansu` datetime NOT NULL,
  `tgbatdau_cvht` datetime NOT NULL,
  `tgketthuc_cvht` datetime NOT NULL,
  `tgbatdau_hoidongkhoa` datetime NOT NULL,
  `tgketthuc_hoidongkhoa` datetime NOT NULL,
  `tgbatdau_hoidongtruong` datetime NOT NULL,
  `tgketthuc_hoidongtruong` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thongbaochung`
--

CREATE TABLE `tbl_thongbaochung` (
  `id` int(10) UNSIGNED NOT NULL,
  `tieude` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_hocky` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `filename_thongbao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tieuchidanhgia`
--

CREATE TABLE `tbl_tieuchidanhgia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_quyetdinhtieuchi` int(10) UNSIGNED NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `tgbatdau` date NOT NULL,
  `tgketthuc` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tinhtranghoctap`
--

CREATE TABLE `tbl_tinhtranghoctap` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_loaitthoctap` int(10) UNSIGNED NOT NULL,
  `id_sv` int(10) UNSIGNED NOT NULL,
  `trangthai` smallint(6) NOT NULL,
  `tgbatdau` date NOT NULL,
  `tgketthuc` date NOT NULL,
  `filename_quyetdinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `ms_sv_canbo` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loaiuser` smallint(6) NOT NULL,
  `isfirst` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_username_index` (`username`) USING BTREE;

--
-- Indexes for table `tbl_bangdiemdanhgia`
--
ALTER TABLE `tbl_bangdiemdanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_bangdiemdanhgia_id_sv_foreign` (`id_sv`) USING BTREE,
  ADD KEY `tbl_bangdiemdanhgia_id_thoigiandanhgia_foreign` (`id_thoigiandanhgia`) USING BTREE;

--
-- Indexes for table `tbl_bomon_donvi`
--
ALTER TABLE `tbl_bomon_donvi`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_bomon_donvi_mabomon_unique` (`mabomon`) USING BTREE,
  ADD KEY `tbl_bomon_donvi_tructhuoc_foreign` (`tructhuoc`) USING BTREE;

--
-- Indexes for table `tbl_canbo`
--
ALTER TABLE `tbl_canbo`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_canbo_macanbo_unique` (`macanbo`) USING BTREE,
  ADD KEY `tbl_canbo_id_bomon_donvi_foreign` (`id_bomon_donvi`) USING BTREE;

--
-- Indexes for table `tbl_chitietdanhgia`
--
ALTER TABLE `tbl_chitietdanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chitietdanhgia_id_user_foreign` (`id_user`) USING BTREE,
  ADD KEY `tbl_chitietdanhgia_id_tieuchi_foreign` (`id_tieuchi`) USING BTREE,
  ADD KEY `tbl_chitietdanhgia_id_bangdiemdanhgia_foreign` (`id_bangdiemdanhgia`) USING BTREE;

--
-- Indexes for table `tbl_chitiettieuchi`
--
ALTER TABLE `tbl_chitiettieuchi`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chitiettieuchi_id_loaidiem_foreign` (`id_loaidiem`) USING BTREE,
  ADD KEY `tbl_chitiettieuchi_id_quyetdinhtieuchi_foreign` (`id_quyetdinhtieuchi`) USING BTREE;

--
-- Indexes for table `tbl_chitiet_cvht`
--
ALTER TABLE `tbl_chitiet_cvht`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_lop_foreign` (`id_lop`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_canbo_foreign` (`id_canbo`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_hockybatdau_foreign` (`id_hockybatdau`) USING BTREE,
  ADD KEY `tbl_chitiet_cvht_id_hockyketthuc_foreign` (`id_hockyketthuc`) USING BTREE;

--
-- Indexes for table `tbl_chucvu_bcs`
--
ALTER TABLE `tbl_chucvu_bcs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_chucvu_bcs_id_sv_foreign` (`id_sv`) USING BTREE,
  ADD KEY `tbl_chucvu_bcs_id_loaichucvu_bcs_foreign` (`id_loaichucvu_bcs`) USING BTREE;

--
-- Indexes for table `tbl_chucvu_quanly`
--
ALTER TABLE `tbl_chucvu_quanly`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_chucvu_quanly_machucvu_quanly_unique` (`machucvu_quanly`) USING BTREE;

--
-- Indexes for table `tbl_hocky`
--
ALTER TABLE `tbl_hocky`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_hocky_mahocky_unique` (`mahocky`) USING BTREE,
  ADD KEY `tbl_hocky_id_namhoc_foreign` (`id_namhoc`) USING BTREE;

--
-- Indexes for table `tbl_khoa`
--
ALTER TABLE `tbl_khoa`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_khoa_makhoa_unique` (`makhoa`) USING BTREE;

--
-- Indexes for table `tbl_kyluat`
--
ALTER TABLE `tbl_kyluat`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_kyluat_id_sv_foreign` (`id_sv`) USING BTREE,
  ADD KEY `tbl_kyluat_id_loaikyluat_foreign` (`id_loaikyluat`) USING BTREE;

--
-- Indexes for table `tbl_loaichucvu_bcs`
--
ALTER TABLE `tbl_loaichucvu_bcs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_loaichucvu_bcs_machucvu_bcs_unique` (`machucvu_bcs`) USING BTREE;

--
-- Indexes for table `tbl_loaidiem`
--
ALTER TABLE `tbl_loaidiem`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_loaikyluat`
--
ALTER TABLE `tbl_loaikyluat`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_loaikyluat_maloai_kyluat_unique` (`maloai_kyluat`) USING BTREE;

--
-- Indexes for table `tbl_loaitinhtranghoctap`
--
ALTER TABLE `tbl_loaitinhtranghoctap`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_loaitinhtranghoctap_maloai_tthoctap_unique` (`maloai_tthoctap`) USING BTREE;

--
-- Indexes for table `tbl_lop`
--
ALTER TABLE `tbl_lop`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_lop_tenlop_unique` (`tenlop`) USING BTREE,
  ADD KEY `tbl_lop_id_nganh_foreign` (`id_nganh`) USING BTREE;

--
-- Indexes for table `tbl_namhoc`
--
ALTER TABLE `tbl_namhoc`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_namhoc_manamhoc_unique` (`manamhoc`) USING BTREE;

--
-- Indexes for table `tbl_nganh`
--
ALTER TABLE `tbl_nganh`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_nganh_manganh_unique` (`manganh`) USING BTREE,
  ADD KEY `tbl_nganh_id_bomon_donvi_foreign` (`id_bomon_donvi`) USING BTREE;

--
-- Indexes for table `tbl_nhomquyen`
--
ALTER TABLE `tbl_nhomquyen`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_phanquyen`
--
ALTER TABLE `tbl_phanquyen`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_phanquyen_id_user_foreign` (`id_user`) USING BTREE,
  ADD KEY `tbl_phanquyen_id_nhomquyen_foreign` (`id_nhomquyen`) USING BTREE;

--
-- Indexes for table `tbl_quanly`
--
ALTER TABLE `tbl_quanly`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_quanly_id_canbo_foreign` (`id_canbo`) USING BTREE,
  ADD KEY `tbl_quanly_id_chucvu_quanly_foreign` (`id_chucvu_quanly`) USING BTREE;

--
-- Indexes for table `tbl_quyetdinhtieuchi`
--
ALTER TABLE `tbl_quyetdinhtieuchi`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_quyetdinhtieuchi_maquyetdinh_unique` (`maquyetdinh`) USING BTREE;

--
-- Indexes for table `tbl_sinhvien`
--
ALTER TABLE `tbl_sinhvien`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_sinhvien_mssv_unique` (`mssv`) USING BTREE,
  ADD KEY `tbl_sinhvien_id_lop_foreign` (`id_lop`) USING BTREE;

--
-- Indexes for table `tbl_thoigiandanhgia`
--
ALTER TABLE `tbl_thoigiandanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_thoigiandanhgia_id_hocky_unique` (`id_hocky`) USING BTREE,
  ADD KEY `tbl_thoigiandanhgia_id_tieuchidanhgia_foreign` (`id_tieuchidanhgia`) USING BTREE;

--
-- Indexes for table `tbl_thongbaochung`
--
ALTER TABLE `tbl_thongbaochung`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_thongbaochung_id_user_foreign` (`id_user`) USING BTREE,
  ADD KEY `tbl_thongbaochung_id_hocky_foreign` (`id_hocky`) USING BTREE;

--
-- Indexes for table `tbl_tieuchidanhgia`
--
ALTER TABLE `tbl_tieuchidanhgia`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_tieuchidanhgia_id_quyetdinhtieuchi_foreign` (`id_quyetdinhtieuchi`) USING BTREE;

--
-- Indexes for table `tbl_tinhtranghoctap`
--
ALTER TABLE `tbl_tinhtranghoctap`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_tinhtranghoctap_id_loaitthoctap_foreign` (`id_loaitthoctap`) USING BTREE,
  ADD KEY `tbl_tinhtranghoctap_id_sv_foreign` (`id_sv`) USING BTREE;

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `tbl_users_username_unique` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `tbl_bangdiemdanhgia`
--
ALTER TABLE `tbl_bangdiemdanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_bomon_donvi`
--
ALTER TABLE `tbl_bomon_donvi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_canbo`
--
ALTER TABLE `tbl_canbo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_chitietdanhgia`
--
ALTER TABLE `tbl_chitietdanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_chitiettieuchi`
--
ALTER TABLE `tbl_chitiettieuchi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tbl_chitiet_cvht`
--
ALTER TABLE `tbl_chitiet_cvht`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_chucvu_bcs`
--
ALTER TABLE `tbl_chucvu_bcs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_chucvu_quanly`
--
ALTER TABLE `tbl_chucvu_quanly`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_hocky`
--
ALTER TABLE `tbl_hocky`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_khoa`
--
ALTER TABLE `tbl_khoa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_kyluat`
--
ALTER TABLE `tbl_kyluat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_loaichucvu_bcs`
--
ALTER TABLE `tbl_loaichucvu_bcs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_loaidiem`
--
ALTER TABLE `tbl_loaidiem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_loaikyluat`
--
ALTER TABLE `tbl_loaikyluat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_loaitinhtranghoctap`
--
ALTER TABLE `tbl_loaitinhtranghoctap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_lop`
--
ALTER TABLE `tbl_lop`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_namhoc`
--
ALTER TABLE `tbl_namhoc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_nganh`
--
ALTER TABLE `tbl_nganh`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_nhomquyen`
--
ALTER TABLE `tbl_nhomquyen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_phanquyen`
--
ALTER TABLE `tbl_phanquyen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `tbl_quanly`
--
ALTER TABLE `tbl_quanly`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_quyetdinhtieuchi`
--
ALTER TABLE `tbl_quyetdinhtieuchi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_sinhvien`
--
ALTER TABLE `tbl_sinhvien`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `tbl_thoigiandanhgia`
--
ALTER TABLE `tbl_thoigiandanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_thongbaochung`
--
ALTER TABLE `tbl_thongbaochung`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_tieuchidanhgia`
--
ALTER TABLE `tbl_tieuchidanhgia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_tinhtranghoctap`
--
ALTER TABLE `tbl_tinhtranghoctap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_bangdiemdanhgia`
--
ALTER TABLE `tbl_bangdiemdanhgia`
  ADD CONSTRAINT `tbl_bangdiemdanhgia_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`),
  ADD CONSTRAINT `tbl_bangdiemdanhgia_id_thoigiandanhgia_foreign` FOREIGN KEY (`id_thoigiandanhgia`) REFERENCES `tbl_thoigiandanhgia` (`id`);

--
-- Constraints for table `tbl_bomon_donvi`
--
ALTER TABLE `tbl_bomon_donvi`
  ADD CONSTRAINT `tbl_bomon_donvi_tructhuoc_foreign` FOREIGN KEY (`tructhuoc`) REFERENCES `tbl_khoa` (`id`);

--
-- Constraints for table `tbl_canbo`
--
ALTER TABLE `tbl_canbo`
  ADD CONSTRAINT `tbl_canbo_id_bomon_donvi_foreign` FOREIGN KEY (`id_bomon_donvi`) REFERENCES `tbl_bomon_donvi` (`id`);

--
-- Constraints for table `tbl_chitietdanhgia`
--
ALTER TABLE `tbl_chitietdanhgia`
  ADD CONSTRAINT `tbl_chitietdanhgia_id_bangdiemdanhgia_foreign` FOREIGN KEY (`id_bangdiemdanhgia`) REFERENCES `tbl_bangdiemdanhgia` (`id`),
  ADD CONSTRAINT `tbl_chitietdanhgia_id_tieuchi_foreign` FOREIGN KEY (`id_tieuchi`) REFERENCES `tbl_chitiettieuchi` (`id`),
  ADD CONSTRAINT `tbl_chitietdanhgia_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_chitiettieuchi`
--
ALTER TABLE `tbl_chitiettieuchi`
  ADD CONSTRAINT `tbl_chitiettieuchi_id_loaidiem_foreign` FOREIGN KEY (`id_loaidiem`) REFERENCES `tbl_loaidiem` (`id`),
  ADD CONSTRAINT `tbl_chitiettieuchi_id_quyetdinhtieuchi_foreign` FOREIGN KEY (`id_quyetdinhtieuchi`) REFERENCES `tbl_quyetdinhtieuchi` (`id`);

--
-- Constraints for table `tbl_chitiet_cvht`
--
ALTER TABLE `tbl_chitiet_cvht`
  ADD CONSTRAINT `tbl_chitiet_cvht_id_canbo_foreign` FOREIGN KEY (`id_canbo`) REFERENCES `tbl_canbo` (`id`),
  ADD CONSTRAINT `tbl_chitiet_cvht_id_hockybatdau_foreign` FOREIGN KEY (`id_hockybatdau`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_chitiet_cvht_id_hockyketthuc_foreign` FOREIGN KEY (`id_hockyketthuc`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_chitiet_cvht_id_lop_foreign` FOREIGN KEY (`id_lop`) REFERENCES `tbl_lop` (`id`);

--
-- Constraints for table `tbl_chucvu_bcs`
--
ALTER TABLE `tbl_chucvu_bcs`
  ADD CONSTRAINT `tbl_chucvu_bcs_id_loaichucvu_bcs_foreign` FOREIGN KEY (`id_loaichucvu_bcs`) REFERENCES `tbl_loaichucvu_bcs` (`id`),
  ADD CONSTRAINT `tbl_chucvu_bcs_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`);

--
-- Constraints for table `tbl_hocky`
--
ALTER TABLE `tbl_hocky`
  ADD CONSTRAINT `tbl_hocky_id_namhoc_foreign` FOREIGN KEY (`id_namhoc`) REFERENCES `tbl_namhoc` (`id`);

--
-- Constraints for table `tbl_kyluat`
--
ALTER TABLE `tbl_kyluat`
  ADD CONSTRAINT `tbl_kyluat_id_loaikyluat_foreign` FOREIGN KEY (`id_loaikyluat`) REFERENCES `tbl_loaikyluat` (`id`),
  ADD CONSTRAINT `tbl_kyluat_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`);

--
-- Constraints for table `tbl_lop`
--
ALTER TABLE `tbl_lop`
  ADD CONSTRAINT `tbl_lop_id_nganh_foreign` FOREIGN KEY (`id_nganh`) REFERENCES `tbl_nganh` (`id`);

--
-- Constraints for table `tbl_nganh`
--
ALTER TABLE `tbl_nganh`
  ADD CONSTRAINT `tbl_nganh_id_bomon_donvi_foreign` FOREIGN KEY (`id_bomon_donvi`) REFERENCES `tbl_bomon_donvi` (`id`);

--
-- Constraints for table `tbl_phanquyen`
--
ALTER TABLE `tbl_phanquyen`
  ADD CONSTRAINT `tbl_phanquyen_id_nhomquyen_foreign` FOREIGN KEY (`id_nhomquyen`) REFERENCES `tbl_nhomquyen` (`id`),
  ADD CONSTRAINT `tbl_phanquyen_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_quanly`
--
ALTER TABLE `tbl_quanly`
  ADD CONSTRAINT `tbl_quanly_id_canbo_foreign` FOREIGN KEY (`id_canbo`) REFERENCES `tbl_canbo` (`id`),
  ADD CONSTRAINT `tbl_quanly_id_chucvu_quanly_foreign` FOREIGN KEY (`id_chucvu_quanly`) REFERENCES `tbl_chucvu_quanly` (`id`);

--
-- Constraints for table `tbl_sinhvien`
--
ALTER TABLE `tbl_sinhvien`
  ADD CONSTRAINT `tbl_sinhvien_id_lop_foreign` FOREIGN KEY (`id_lop`) REFERENCES `tbl_lop` (`id`);

--
-- Constraints for table `tbl_thoigiandanhgia`
--
ALTER TABLE `tbl_thoigiandanhgia`
  ADD CONSTRAINT `tbl_thoigiandanhgia_id_hocky_foreign` FOREIGN KEY (`id_hocky`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_thoigiandanhgia_id_tieuchidanhgia_foreign` FOREIGN KEY (`id_tieuchidanhgia`) REFERENCES `tbl_tieuchidanhgia` (`id`);

--
-- Constraints for table `tbl_thongbaochung`
--
ALTER TABLE `tbl_thongbaochung`
  ADD CONSTRAINT `tbl_thongbaochung_id_hocky_foreign` FOREIGN KEY (`id_hocky`) REFERENCES `tbl_hocky` (`id`),
  ADD CONSTRAINT `tbl_thongbaochung_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `tbl_users` (`id`);

--
-- Constraints for table `tbl_tieuchidanhgia`
--
ALTER TABLE `tbl_tieuchidanhgia`
  ADD CONSTRAINT `tbl_tieuchidanhgia_id_quyetdinhtieuchi_foreign` FOREIGN KEY (`id_quyetdinhtieuchi`) REFERENCES `tbl_quyetdinhtieuchi` (`id`);

--
-- Constraints for table `tbl_tinhtranghoctap`
--
ALTER TABLE `tbl_tinhtranghoctap`
  ADD CONSTRAINT `tbl_tinhtranghoctap_id_loaitthoctap_foreign` FOREIGN KEY (`id_loaitthoctap`) REFERENCES `tbl_loaitinhtranghoctap` (`id`),
  ADD CONSTRAINT `tbl_tinhtranghoctap_id_sv_foreign` FOREIGN KEY (`id_sv`) REFERENCES `tbl_sinhvien` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
