<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('dangnhap', 'User_Controller@dangnhap');

Route::group(['prefix' => 'admin', 'middleware' => 'jwt.auth'], function () {

    Route::post('dangky', 'User_Controller@dangky'); //

    Route::post('thongtinnguoidunghienhanh', 'User_Controller@getInfoUser'); // Lấy thông tin User đăng nhập
    Route::get('refreshToken', 'User_Controller@refreshToken'); // Làm mới lại token khi hết hạn

    // Route Khoa
    Route::group(['prefix' => 'quanlykhoa'], function () {
        Route::get('danhsachkhoa', 'Khoa_Controller@getDanhSachKhoa'); // Lấy danh sách các khoa
        Route::post('themkhoa', 'Khoa_Controller@postThemKhoa'); // Thêm khoa mới
        Route::put('suakhoa/{id}', 'Khoa_Controller@putSuaKhoa'); // Sửa thông tin khoa
        Route::get('thongtinkhoa/{id}', 'Khoa_Controller@getKhoaById'); // Tìm thông tin của khoa
        Route::delete('xoakhoa/{id}', 'Khoa_Controller@deleteXoaKhoa'); // Xóa khoa theo id
    });

    //Route Bộ Môn Đơn Vị
    Route::group(['prefix' => 'quanlybomon'], function () {
        Route::get('danhsachbomon', 'BoMon_DonVi_Controller@getDanhSachBoMonDonVi'); // Lấy danh sách các bộ môn đơn vị trực thuộc của trường
        Route::get('danhsachbomon_tructhuoc/{tructhuoc}', 'BoMon_DonVi_Controller@getDanhSachBoMonDonVi_TrucThuoc'); // Lấy danh sách các bộ môn đơn vị trực thuộc của trường
        Route::post('thembomon', 'BoMon_DonVi_Controller@postThemBoMon'); // Thêm bộ môn mới
        Route::put('suabomon/{id}', 'BoMon_DonVi_Controller@putSuaBoMon'); // Sửa thông tin bộ môn
        Route::get('thongtinbomon/{id}', 'BoMon_DonVi_Controller@getBoMonById'); // Tìm thông tin của bộ môn
        Route::delete('xoabomon/{id}', 'BoMon_DonVi_Controller@deleteXoaBoMon'); // Xóa bộ môn theo id
    });

    // Route Ngành
    Route::group(['prefix' => 'quanlynganh'], function () {
        Route::get('danhsachnganh', 'Nganh_Controller@getDanhSachNganh'); // Lấy danh sách các ngành
        Route::post('danhsachnganh_khoa', 'Nganh_Controller@postDanhSachNganh_ByKhoa'); // Lấy danh sách các ngành thuộc khoa
        Route::post('themnganh', 'Nganh_Controller@postThemNganh'); // Thêm ngành mới
        Route::put('suanganh/{id}', 'Nganh_Controller@putSuaNganh'); // Sửa thông tin ngành
        Route::get('thongtinnganh/{id}', 'Nganh_Controller@getNganhById'); // Tìm thông tin của ngành
        Route::delete('xoanganh/{id}', 'Nganh_Controller@deleteXoaNganh'); // Xóa ngành theo id
    });

    // Route Lớp
    Route::group(['prefix' => 'quanlylop'], function () {
        Route::get('danhsachlop', 'Lop_Controller@getDanhSachLop'); // Lấy danh sách các lớp
        Route::post('danhsachlop_nganh', 'Lop_Controller@getDanhSachLop_Nganh'); // Lấy danh sách các lớp thuộc ngành được chọn
        Route::post('themlop', 'Lop_Controller@postThemLop'); // Thêm ngành mới
        Route::put('sualop/{id}', 'Lop_Controller@putSuaLop'); // Sửa thông tin ngành
        Route::get('thongtinlop/{id}', 'Lop_Controller@getLopById'); // Tìm thông tin của ngành
        Route::delete('xoalop/{id}', 'Lop_Controller@deleteXoaLop'); // Xóa ngành theo id
    });

    // Route Sinh Viên
    Route::group(['prefix' => 'quanlysinhvien'], function () {
        Route::get('getidlop/{id_sv}', 'SinhVien_Controller@getIDLop'); // Lấy id lớp của sinh viên
        Route::get('danhsachchucvusinhvien', 'LoaiChucVuBCS_Controller@getDanhSachLoaiChucVuBCS'); // Lấy tất cả chức vụ của sinh viên
        Route::get('danhsachsinhvien', 'SinhVien_Controller@getDanhSachSinhVien'); // Lấy danh sách các sinh viên
        Route::post('danhsachsinhvien_lop', 'SinhVien_Controller@postDanhSachSinhVien_Lop'); // Lấy danh sách các lớp thuộc ngành được chọn
        Route::get('thongtinsinhvien/{id}', 'SinhVien_Controller@getThongTinSinhVien'); // Lấy thông tin sinh viên 
        Route::get('chitietthongtinsinhvien/{id}', 'SinhVien_Controller@getChiTietSinhVien'); // Lấy thông tin sinh viên để xem chi tiết
        Route::post('themsinhvien', 'SinhVien_Controller@postThemSinhvien'); // Thêm mới sinh viên tự tạo user với mật khẩu mặc định là 'user'
        Route::put('suasinhvien/{id}', 'SinhVien_Controller@putSuaSinhVien'); // Sửa thông tin sinh viên
        Route::delete('xoasinhvien/{id}', 'SinhVien_Controller@deleteXoaSinhVien'); // Xóa sinh viên  theo id

        Route::post('importdssinhvien', 'SinhVien_Controller@importDanhSachSinhVien'); // Import danh sách sinh viên
        // Route::post('exportdssinhvien', 'SinhVien_Controller@exportDanhSachSinhVien'); // Export danh sách sinh viên
    });

    // Route Tình Trạng Học Tập
    Route::group(['prefix' => 'quanlytinhtranghoctap'], function () {
        Route::post('danhsachtinhtranghoctap', 'TTHocTap_Controller@getAllDanhSachTHHT'); // Lấy danh sách tất cả thông tin học tập của sinh viên
        Route::get('thongtintthoctap/{id}', 'TTHocTap_Controller@getTTHTById'); // Tìm thông tin của ngành
        Route::get('danhsachloaitthoctap', 'LoaiTTHocTap_Controller@getAllLoaiTTHT'); // Lấy danh sách tất cả loại tt học tập
        Route::post('suatinhtranghoctap/{id}', 'TTHocTap_Controller@putSuaTTHT'); // Sửa thông tin học tập của sinh viên
        Route::delete('xoatinhtranghoctap/{id}', 'TTHocTap_Controller@deleteXoaTTHocTap'); // Xóa tình trạng học tập của sinh viên

        Route::post('demsoluongtthoctap', 'TTHocTap_Controller@demSoLuong'); // Đếm số lượng thống kê

        Route::get('getfilehoctap/{id}', 'TTHocTap_Controller@getFileTHHT'); // Lấy file quyết định học tập
    });

    // Route Tình Tình Hình Kỷ Luật
    Route::group(['prefix' => 'quanlykyluat'], function () {
        Route::post('danhsachkyluat', 'KyLuat_Controller@getAllDanhSachKyLuat'); // Lấy danh sách tất cả thông tin kỷ luật của sinh viên
        Route::get('thongtinkyluat/{id}', 'KyLuat_Controller@getKyLuatById'); // Tìm thông tin của ngành
        Route::get('danhsachloaikyluat', 'LoaiKyLuat_Controller@getAllLoaiKyLuat'); // Lấy danh sách tất cả loại kỷ luật
        Route::post('themkyluat', 'KyLuat_Controller@postThemKyLuat'); // Thêm thông tin kỷ luật của sinh viên
        Route::post('suakyluat/{id}', 'KyLuat_Controller@putSuaKyLuat'); // Sửa thông tin kỷ luật của sinh viên
        Route::delete('xoakyluat/{id}', 'KyLuat_Controller@deleteXoaKyLuat'); // Xóa kỷ luật của sinh viên

        Route::post('demsoluongkyluat', 'KyLuat_Controller@demSoLuong'); // Đếm số lượng thống kê

        Route::get('getfilekyluat/{id}', 'KyLuat_Controller@getFileKyLuat'); // Lấy file quyết định kỷ luật
    });

    // Route Quản Lý Tài Khoản
    Route::group(['prefix' => 'quanlytaikhoan'], function () {
        Route::post('danhsachtaikhoansinhvien', 'User_Controller@getDanhSachUserByLop'); // Lấy danh sách các tài khoản của sinh viên
        Route::post('danhsachtaikhoancanbo', 'User_Controller@getDanhSachUserByBoMon'); // Lấy danh sách các tài khoản của cán bộ

        Route::post('taotaikhoansv', 'User_Controller@createAccountSV'); // Tạo tài khoản cho sinh viên
        Route::post('taotaikhoancb', 'User_Controller@createAccountCB'); // Tạo tài khoản cho sinh viên

        Route::post('themphanquyen/{id}', 'PhanQuyen_Controller@postThemQuyen'); // Thêm phân quyền
        Route::get('danhsachphanquyen/{id}', 'PhanQuyen_Controller@getPhanQuyenByIDUser'); // Lấy danh sách phân quyền theo ID user

        Route::get('danhsachnhomquyensinhvien', 'NhomQuyen_Controller@getDSNhomQuyenToSVAdd'); // Lấy danh sách nhóm quyền để thêm nhóm quyền cho tài khoản sinh viên
        Route::get('danhsachnhomquyencanbo', 'NhomQuyen_Controller@getDSNhomQuyenToCBAdd'); // Lấy danh sách nhóm quyền để thêm nhóm quyền cho tài khoản cán bộ

        Route::put('khoahoackichhoattaikhoan/{id}', 'User_Controller@putLockOrOpenAccount'); // Khóa tài khoản
        Route::get('khoiphucmatkhau/{id}', 'User_Controller@resetPass'); // Khôi phục lại mật khẩu mặt định cho tài khoản
        Route::delete('xoataikhoansv/{id}', 'User_Controller@deleteAccount'); // Xóa tài khoản theo ID user

    });

    // Route Cán Bộ
    Route::group(['prefix' => 'quanlycanbo'], function () {
        Route::get('danhsachchucvuquanly', 'ChucVuQuanLy_Controller@getDanhSachChucVuQuanLy'); // Lấy danh sách chức vụ của cán bộ
        Route::post('danhsachcanbo_bomon', 'CanBo_Controller@postDanhSachCanBo_BoMon'); // Lấy danh sách cán bộ theo bộ môn trực thuộc
        Route::get('thongtincanbo/{id}', 'CanBo_Controller@getThongTinCanBoById'); // Thông tin cán bộ theo id
        Route::post('themcanbo', 'CanBo_Controller@postThemCanBo'); // Thêm cán bộ mới
        Route::put('suacanbo/{id}', 'CanBo_Controller@putSuaCanBo'); // Chỉnh sửa hông tin cán bộ theo id
        Route::delete('xoacanbo/{id}', 'CanBo_Controller@deleteXoaCanBo'); // Xóa cán bộ theo ID

        Route::post('importdscanbo', 'CanBo_Controller@importDanhSachCanBo'); // Import danh sách cán bộ
        // Route::post('exportdscanbo', 'CanBo_Controller@exportDanhSachCanBo'); // Export danh sách cán bộ


        /* Quản Lý Cố Vấn Học Tập */
        Route::post('danhsachcovanhoctap', 'CoVanHocTap_Controller@getDanhSachCoVanHocTap'); // Lấy danh sách cán bộ cố vấn học tập
        Route::post('danhsachcanbochuacovan', 'CanBo_Controller@postDanhSachCanBo_ChuaCoVan'); // Lấy danh sách cán bộ chưa phân công cố vấn
        Route::get('thongtincovanhoctap/{id}', 'CoVanHocTap_Controller@getThongTinCoVanHocTapById'); // Thông tin cán bộ cố vấn học tập theo id
        Route::post('themcovanhoctap', 'CoVanHocTap_Controller@postThemCoVanHocTap'); // Thêm cố vấn học tập mới
        Route::put('suacovanhoctap/{id}', 'CoVanHocTap_Controller@putSuaCoVanHocTap'); // Chỉnh sửa thông tin cố vấn học tập theo id
        Route::post('xoacovanhoctap', 'CoVanHocTap_Controller@postXoaCoVanHocTap'); // Xóa cố vấn học tập theo ID user

    });

    // Route Năm Học - Hoc Kỳ
    Route::group(['prefix' => 'quanlynamhochocky'], function () {
        Route::get('danhsachnamhoc', 'NamHocHocKy_Controller@getDanhSachNamHoc'); // Lấy danh sách các năm học
        Route::get('danhsachhocky_tgdanhgia', 'NamHocHocKy_Controller@getHocKy_TGDG'); // Lấy danh sách các học kỳ theo thời gian đánh giá
        Route::get('danhsachhocky/{id}', 'NamHocHocKy_Controller@getDanhSachHocKyByNamHoc'); // Lấy danh sách các học kỳ theo ID năm học trừ học kỳ đã có thời gian đánh giá
        Route::get('danhsachtatcahocky/{id}', 'NamHocHocKy_Controller@getDanhSachAllHocKyByNamHoc'); // Lấy danh sách các tất cả học kỳ theo ID năm học
        Route::get('danhsachnamhochocky', 'NamHocHocKy_Controller@getDanhSachNamHocHocKy'); // Lấy danh sách các năm học và học kỳ
        Route::get('danhsachhocky', 'NamHocHocKy_Controller@getDanhSachHocKy'); // Lấy danh sách các học kỳ
        Route::get('thongtinnamhoc/{id}', 'NamHocHocKy_Controller@getInfoNamHocHocKy'); // Lấy thông tin năm học và học kỳ theo id
        Route::post('themnamhochocky', 'NamHocHocKy_Controller@postThemNamHocHocKy'); // Thêm năm học và học kỳ
        Route::put('suanamhochocky/{id}', 'NamHocHocKy_Controller@putSuaNamHocHocKy'); // Sửa thông tin năm học và học kỳ
        Route::delete('xoanamhochocky/{id}', 'NamHocHocKy_Controller@deleteXoaNamHocHocKy'); // Xóa năm học và học kỳ
    });

    // Route Thời Gian Đánh Giá
    Route::group(['prefix' => 'quanlythoigiandanhgia'], function () {
        Route::get('danhsachthoigiandanhgia', 'ThoiGianDanhGia_Controller@getDanhSachThoiGianDanhGia'); // Lấy danh sách các thời gian đánh giá
        Route::get('thongtinthoigiandanhgia/{id}', 'ThoiGianDanhGia_Controller@getInfoThoiGianDanhGiaById'); // Lấy thông tin thời gian đánh giá theo id
        Route::get('thoigiandanhgia/{id}', 'ThoiGianDanhGia_Controller@getInfoThoiGianDanhGiaByIdHocKy'); // Lấy thông tin thời gian đánh giá theo theo Id học kỳ để hiển thị
        Route::post('themthoigiandanhgia', 'ThoiGianDanhGia_Controller@postThemThoiGianDanhGia'); // Thêm thời gian đánh giá
        Route::put('suathoigiandanhgia/{id}', 'ThoiGianDanhGia_Controller@putThemThoiGianDanhGia'); // Sửa thông tin thời gian đánh giá
        Route::delete('xoathoigiandanhgia/{id}', 'ThoiGianDanhGia_Controller@deleteThoiGianDanhGia'); // Xóa thời gian đánh giá
    });

    // Route Tiêu Chí Đánh Giá
    Route::group(['prefix' => 'quanlytieuchidanhgia'], function () {
        Route::get('danhsachloaidiem', 'ChiTietTieuChiDanhGia_Controller@getDanhSachLoaiDiem'); // Lấy danh sách các loại điểm

        Route::get('danhsachtieuchi/{id_tieuchi}', 'ChiTietTieuChiDanhGia_Controller@getTieuChiDanhGiaByIdTieuChi'); // Lấy danh sách các loại điểm

        Route::post('themquyetdinhtieuchidanhgia', 'TieuChiDanhGia_Controller@postThemQuyetDinhTieuChi'); // Thêm tệp quyết định thành lập các tiêu chí mới

        Route::get('danhsachtieuchivuathem/{id_quyetdinhtieuchi}', 'ChiTietTieuChiDanhGia_Controller@getDanhSachCacTieuChiVuaThem'); // Lấy danh sách các tiêu chí vừa được thêm vào
        Route::get('danhsachchimucvuathem/{id_quyetdinhtieuchi}', 'ChiTietTieuChiDanhGia_Controller@getDanhSachCacChiMucVuaThem'); // Lấy danh sách các chỉ mục vùa thêm vào
        Route::get('thongtintieuchi/{id}', 'ChiTietTieuChiDanhGia_Controller@getInfoTieuChi'); // Lấy thông tin tiêu chí theo Id
        Route::post('themtieuchidanhgia', 'ChiTietTieuChiDanhGia_Controller@postThemTieuChi'); // Thêm tiêu chí mới
        Route::put('suatieuchidanhgia/{id}', 'ChiTietTieuChiDanhGia_Controller@putSuaTieuChi'); // Sửa thông tin tiêu chí
        Route::delete('xoatieuchi/{idtieuchi}', 'ChiTietTieuChiDanhGia_Controller@deleteXoaTieuChi'); // Xóa tiêu chí

        Route::get('danhsachquyetdinhtieuchi', 'TieuChiDanhGia_Controller@getDanhSachQuyetDinh'); // Lấy danh sách các quyết định thành lập tiêu chí
        Route::put('capnhatthoigianketthucquyetdinh/{id}', 'TieuChiDanhGia_Controller@putCapNhatThoiGianKetThuc'); // Cập nhật thời gian kết thúc và khóa quyết định
        Route::get('thongtinquyetdinh/{id}', 'TieuChiDanhGia_Controller@getInfoQD'); // Lấy thông tin quyết định
        Route::put('suaquyetdinh/{id}', 'TieuChiDanhGia_Controller@putSuaQuyetDinh'); // Cập nhật thông tin quyết định
        Route::delete('xoaquyetdinh/{id}', 'TieuChiDanhGia_Controller@deleteXoaQuyetDinh'); // Xóa quyết định và các tiêu chí liên quan đến quyết định
    });

     // Route Thông Báo Đánh Giá
     Route::group(['prefix' => 'quanlythongbao'], function () {
        Route::get('danhsachthongbao', 'ThongBao_Controller@getAllDanhSachThongBao'); // Lấy danh sách thông báo
        Route::get('danhsachthongbaohienthi', 'ThongBao_Controller@getDanhSachThongBaoHienThi'); // Lấy danh sách thông báo
        Route::get('thongtinthongbao/{id}', 'ThongBao_Controller@getInfoThongBao'); // Lấy thông tin thông báo
        Route::post('themthongbao', 'ThongBao_Controller@postThemThongBao'); // Thêm thông báo mới
        Route::put('suathongbao/{id}', 'ThongBao_Controller@putSuaThongBao'); // Cập nhật thông tin thông báo
        Route::delete('xoathongbao/{id}', 'ThongBao_Controller@deleteXoaThongBao'); // Xóa thông báo
        Route::put('khoahoackichhoatthongbao/{id}', 'ThongBao_Controller@putLockOrOpenThongBao'); // Khóa thông báo
    });
});

Route::group(['prefix' => 'user', 'middleware' => 'jwt.auth'], function () {
    Route::post('kiemtrahocky', 'Chung_Controller@kiemTraHocKy'); // Lấy học kỳ hiện tại để hiện thi các  thông tin kèm theo

    Route::post('themdanhgia_sinhvien', 'ChiTietDanhGia_Controller@postThemDanhGia_SV'); // Thêm đánh giá của sinh viên
    Route::put('themdanhgia_khacsinhvien/{id_bangdiemdanhgia}', 'ChiTietDanhGia_Controller@postThemDanhGia_KhacSV'); // Thêm đánh giá của sinh viên

    Route::post('suadanhgia', 'ChiTietDanhGia_Controller@putSuaDanhGia'); // Sửa đánh giá của sinh viên

    Route::post('kiemtradanhgia', 'ChiTietDanhGia_Controller@checkDanhGia'); // Kiểm tra sinh viên hoặc cán bộ đã đánh giá chưa
    Route::post('taobangdiemdanhgia', 'ChiTietDanhGia_Controller@postTaoBangDiemDanhGia'); // Tạo bảng điểm đánh giá cho đầu mỗi học kỳ

    Route::get('lichsudanhgia/{id_sv}', 'ChiTietDanhGia_Controller@getLichSuDanhGia'); // Lấy danh sách lịch sử đánh giá của sinh viên theo id sinh viên

    Route::post('danhsachsinhvien_danhgia', 'SinhVien_Controller@postDanhSachSinhVien_CanBo_DanhGia'); // Lấy danh sách sinh viên theo lớp và thời gian đánh giá

    Route::post('lopcovan', 'Lop_Controller@getLopCoVanHocTapByIdCanBo'); // Lấy tên lớp cố vân theo id cán bộ cố vấn

    Route::post('danhsachlophoidongdanhgia', 'Lop_Controller@getLopHoiDongKhoaCoVan'); // Lấy danh sách lớp cố vân theo id cán bộ

    Route::post('thongtinbangdiem', 'Chung_Controller@getThongTinBangDiem'); // Lấy id bảng điểm đánh giá

    Route::post('chitietdanhgiasinhvien', 'ChiTietDanhGia_Controller@postThemDanhGia_SV'); // Chi tiết đánh giá

    Route::post('thongtindanhgia', 'ChiTietDanhGia_Controller@getChiTietDanhGia'); // Chi tiết đánh giá

    Route::get('thongtindanhgia/{id_bangdiemdanhgia}', 'ChiTietDanhGia_Controller@getChiTietDanhGiaByIdBangDiemDanhGia'); // Chi tiết đánh giá bằng ID bảng điểm đánh giá

    Route::post('duyetnhanhdanhgia', 'ChiTietDanhGia_Controller@duyetNhanhDanhGia'); // Duyệt nhanh đánh giá

    Route::put('doimatkhau/{id_user}', 'User_Controller@doiMatKhau'); // Đổi mật khẩu

    Route::post('exportketquadanhgia', 'ChiTietDanhGia_Controller@exportKetQuaDanhGia'); // Export kết quả đánh giá 

    Route::post('exportbangdiemdanhgia', 'ChiTietDanhGia_Controller@exportBangDiemDanhGia'); // Export bảng điếm đánh giá cả lớp
    
    Route::post('xemthongkediemtheohocky', 'ChiTietDanhGia_Controller@postThongKeDiemDanhGiaTheoHocKy'); // Xem thống kê điểm đánh giá

    Route::post('xemthongkediemtheolop_hocky', 'ChiTietDanhGia_Controller@postThongKeDiemDanhGiaTheoLop_HocKy'); // Xem thống kê điểm đánh giá

    Route::post('getFileMinhChung', 'ChiTietDanhGia_Controller@getFileMinhChung'); // Tải file minh chứng

});

