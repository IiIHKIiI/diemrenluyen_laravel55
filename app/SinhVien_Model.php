<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SinhVien_Model extends Model
{
    //
    protected $table = "tbl_sinhvien";
    public $timestamps = false;
}
