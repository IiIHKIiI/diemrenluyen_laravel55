<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lop_Model extends Model
{
    //
    protected $table = 'tbl_lop';
    public $timestamps = false;
}
