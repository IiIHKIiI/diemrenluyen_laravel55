<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Khoa_Model extends Model
{
    //
    protected $table = 'tbl_khoa';
    public $timestamps = false;
}
