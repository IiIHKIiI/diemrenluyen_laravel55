<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NamHoc_Model extends Model
{
    //
    protected $table = 'tbl_namhoc';
    public $timestamps = false;
}
