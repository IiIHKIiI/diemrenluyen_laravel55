<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TieuChiDanhGia_Model extends Model
{
    //
    protected $table = "tbl_tieuchidanhgia";
    public $timestamps = false;
}
