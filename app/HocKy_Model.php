<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HocKy_Model extends Model
{
    //
    protected $table = 'tbl_hocky';
    public $timestamps = false;
}
