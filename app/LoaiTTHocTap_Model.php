<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiTTHocTap_Model extends Model
{
    // 
    protected $table = 'tbl_loaitinhtranghoctap';
    public $timestamps = false;
}
