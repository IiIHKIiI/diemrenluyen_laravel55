<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiKyLuat_Model extends Model
{
    //
    protected $table = 'tbl_loaikyluat';
    public $timestamps = false;
}
