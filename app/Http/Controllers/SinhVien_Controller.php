<?php

namespace App\Http\Controllers;

use App\ChucVuBCS_Model;
use App\PhanQuyen_Model;
use App\SinhVien_Model;
use App\TinhTrangHocTap_Model;
use App\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Config;
class SinhVien_Controller extends Controller
{
    //

    public function importDanhSachSinhVien(Request $request)
    {
        set_time_limit(0);
        if ($request->hasFile('file_danhsach')) {
            $path = $request->file('file_danhsach')->getRealPath();

            $data = Excel::load($path, function ($reader) {
                $reader->setDateFormat('Y-m-d');
            },'UTF-8')->get();

            if (!empty($data) && $data->count()) {
                DB::beginTransaction();
                try {
                    foreach ($data as $key => $value) {
                        if (!empty($value)) {
                            $sinhvien = new SinhVien_Model;
                            $sinhvien->hoten = (string)$value->ho_va_ten;
                            $sinhvien->mssv = (string)$value->mssv;
                            if ($value->gioi_tinh == "Nam") {
                                $sinhvien->gioitinh = 0;
                            } else {
                                $sinhvien->gioitinh = 1;
                            }
                            $sinhvien->ngaysinh = $value->ngay_sinh;
                            $sinhvien->cmnd = (int)$value->cmnd;
                            if ($value->sdt_ca_nhan != null) {
                                $sinhvien->sdt_canhan = $value->sdt_ca_nhan;
                            } else {
                                $sinhvien->sdt_canhan = '0';
                            }
                            $sinhvien->sdt_giadinh = $value->sdt_gia_dinh;
                            $sinhvien->email = (string)$value->email;
                            if ($value->dia_chi != null) {
                                $sinhvien->diachi = (string)$value->dia_chi;
                            } else {
                                $sinhvien->diachi = 'Unknown';
                            }
                            $sinhvien->id_lop = $request->id_lop;
                            $sinhvien->save();

                            $tinhtranghoctap = new TinhTrangHocTap_Model;
                            $tinhtranghoctap->id_loaitthoctap = 1; // Đang học
                            $tinhtranghoctap->id_sv = $sinhvien->id;
                            $tinhtranghoctap->trangthai = 1; // Đã duyệt

                            if ($request->hedaotao == 1) {
                                $tinhtranghoctap->tgbatdau = Carbon::now()->format('Y-m-d');
                                $tinhtranghoctap->tgketthuc = Carbon::now()->addYears(4)->format('Y-m-d');
                            } else {
                                $tinhtranghoctap->tgbatdau = Carbon::now()->format('Y-m-d');
                                $tinhtranghoctap->tgketthuc = Carbon::now()->addYears(3)->format('Y-m-d');
                            }
                            $tinhtranghoctap->save();

                            $user = new User;
                            $user->username = $sinhvien->mssv;
                            // $user->password     = bcrypt($request->cmnd); // thay đổi
                            $user->password = bcrypt('123');
                            $user->trangthai = 1;
                            $user->ms_sv_canbo = $sinhvien->mssv;
                            $user->loaiuser = 1;
                            $user->isfirst = 1;
                            $user->save();

                            $phanquyen = new PhanQuyen_Model;
                            $phanquyen->id_user = $user->id;
                            $phanquyen->id_nhomquyen = 1;
                            $phanquyen->trangthai = 1;
                            $phanquyen->save();
                        }
                    }

                    DB::commit();

                } catch (\Throwable $e) {
                    DB::rollback();
                    throw $e;

                    return response()->json(['err_msg' => 'Không thành công'], 401);

                } catch (QueryException $e) {
                    $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                    return response()->json(['maloi' => $maLoi]);
                }
            }
        }

        $message = ['message' => 'Đã thêm danh sách sinh viên!!!'];
        return response()->json($message, 200);
    }

    public function postDanhSachSinhVien_Lop(Request $request)
    {
        $danhsachsinhvien = DB::table('tbl_sinhvien')
            ->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->leftjoin('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
            ->leftjoin('tbl_bomon_donvi', 'tbl_nganh.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->leftjoin('tbl_khoa', 'tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
            ->select('tbl_sinhvien.*', 'tbl_lop.tenlop', 'tbl_lop.nambatdau', 'tbl_lop.namketthuc', 'tbl_nganh.tennganh', 'tbl_lop.id_nganh', 'tbl_bomon_donvi.tructhuoc', 'tbl_khoa.tenkhoa')
            ->where('id_lop', 'like', $request->id_lop)
            ->get();

        $response = ['danhsach' => $danhsachsinhvien];

        return response()->json($response, 200);

    }

    public function getDanhSachSinhVien()
    {
        $sv = SinhVien_Model::all();
        $response = ['sv' => $sv];
        return response()->json($response, 200);
    }

    public function postDanhSachSinhVien_CanBo_DanhGia(Request $request)
    {
        $danhsach = DB::table('tbl_sinhvien')
            ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
            ->where([['tbl_sinhvien.id_lop', '=', $request->id_lop], ['tbl_bangdiemdanhgia.id_thoigiandanhgia', '=', $request->id_thoigiandanhgia]])
            ->select('tbl_sinhvien.mssv', 'tbl_sinhvien.hoten', 'tbl_sinhvien.ngaysinh', 'tbl_bangdiemdanhgia.*', 'tbl_thoigiandanhgia.id_hocky')
            ->get();

        $response = ['danhsach' => $danhsach];
        return response()->json($response, 200);
    }

    public function getThongTinSinhVien($id)
    {
        $sinhvien = DB::table('tbl_sinhvien')->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->leftjoin('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
            ->leftjoin('tbl_bomon_donvi', 'tbl_nganh.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->leftjoin('tbl_khoa', 'tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
            ->leftjoin('tbl_chucvu_bcs', 'tbl_chucvu_bcs.id_sv', '=', 'tbl_sinhvien.id')
            ->select('tbl_sinhvien.*', 'tbl_lop.hedaotao', 'tbl_chucvu_bcs.id_loaichucvu_bcs', 'tbl_lop.id_nganh', 'tbl_lop.tenlop', 'tbl_nganh.tennganh', 'tbl_bomon_donvi.tructhuoc', 'tbl_khoa.tenkhoa')
            ->where('tbl_sinhvien.id', '=', $id)
            ->first();

        $response = ['thongtin' => $sinhvien];
        return response()->json($response, 200);
    }

    public function getChiTietSinhVien($id)
    {
        $sinhvien = DB::table('tbl_sinhvien')->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->leftjoin('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
            ->leftjoin('tbl_bomon_donvi', 'tbl_nganh.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->leftjoin('tbl_khoa', 'tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
            ->leftjoin('tbl_chucvu_bcs', 'tbl_chucvu_bcs.id_sv', '=', 'tbl_sinhvien.id')
            ->select('tbl_sinhvien.*', 'tbl_lop.hedaotao', 'tbl_lop.tenlop', 'tbl_nganh.tennganh', 'tbl_khoa.tenkhoa')
            ->where('tbl_sinhvien.id', '=', $id)
            ->first();

        $ttht = DB::table('tbl_tinhtranghoctap')
            ->leftjoin('tbl_loaitinhtranghoctap', 'tbl_loaitinhtranghoctap.id', '=', 'tbl_tinhtranghoctap.id_loaitthoctap')
            ->where('id_sv', '=', $id)
            ->select('tbl_loaitinhtranghoctap.tenloai_tthoctap','tbl_tinhtranghoctap.tgbatdau', 'tbl_tinhtranghoctap.tgketthuc', 'tbl_tinhtranghoctap.filename_quyetdinh')
            ->first();

        $kl = DB::table('tbl_kyluat')
            ->leftjoin('tbl_loaikyluat', 'tbl_loaikyluat.id', '=', 'tbl_kyluat.id_loaikyluat')
            ->where('id_sv', '=', $id)
            ->select('tbl_loaikyluat.tenloai_kyluat', 'tbl_kyluat.tgbatdau', 'tbl_kyluat.tgketthuc', 'tbl_kyluat.filename_quyetdinh')
            ->get();

        $response = ['thongtin' => $sinhvien, 'ttht' => $ttht, 'kl' => $kl];
        return response()->json($response, 200);
    }


    public function postThemSinhvien(Request $request)
    {
        $message = [
            'mssv.required' => 'Mã số sinh viên không được bỏ trống',
            'hoten.required' => 'Họ tên không được bỏ trống',
            'cmnd.required' => 'CMND không được bỏ trống',
            'sdt_canhan.required' => 'Số điện thoại cá nhân không được bỏ trống',
            'diachi.required' => 'Địa chỉ không được bỏ trống',
            'email.required' => 'Email không được bỏ trống',
            'id_lop.required' => 'Chưa chọn lớp cho sinh viên',
        ];

        $validator = Validator::make($request->all(),
            [
                'mssv' => 'required',
                'hoten' => 'required',
                'gioitinh' => 'required',
                'ngaysinh' => 'required',
                'cmnd' => 'required',
                'sdt_canhan' => 'required',
                'diachi' => 'required',
                'email' => 'required',
                'id_lop' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();
            // $lop = Lop_Model::where('id', '=', $request->id_lop)->first();
            try {

                $sinhvien = new SinhVien_Model;
                $sinhvien->mssv = $request->mssv;
                $sinhvien->hoten = $request->hoten;
                $sinhvien->gioitinh = $request->gioitinh;
                $sinhvien->ngaysinh = $request->ngaysinh;
                $sinhvien->cmnd = $request->cmnd;
                $sinhvien->sdt_canhan = $request->sdt_canhan;
                $sinhvien->sdt_giadinh = $request->sdt_giadinh;
                $sinhvien->diachi = $request->diachi;
                $sinhvien->email = $request->email;
                $sinhvien->id_lop = $request->id_lop;
                $sinhvien->save();

                $tinhtranghoctap = new TinhTrangHocTap_Model;
                $tinhtranghoctap->id_loaitthoctap = 1; // Đang học
                $tinhtranghoctap->id_sv = $sinhvien->id;
                $tinhtranghoctap->trangthai = 1; // Đã duyệt

                if ($request->hedaotao == 1) {
                    $tinhtranghoctap->tgbatdau = Carbon::now()->format('Y-m-d');
                    $tinhtranghoctap->tgketthuc = Carbon::now()->addYears(4)->format('Y-m-d');
                } else {
                    $tinhtranghoctap->tgbatdau = Carbon::now()->format('Y-m-d');
                    $tinhtranghoctap->tgketthuc = Carbon::now()->addYears(3)->format('Y-m-d');
                }
                $tinhtranghoctap->save();

                $user = new User;
                $user->username = $request->mssv;
                // $user->password     = bcrypt($request->cmnd); // thay đổi
                $user->password = bcrypt('123');
                $user->trangthai = 1;
                $user->ms_sv_canbo = $request->mssv;
                $user->loaiuser = 1;
                $user->isfirst = 1;
                $user->save();

                if ($request->id_chucvu != '' || $request->id_chucvu != null) {
                    if ($request->id_chucvu == 2 || $request->id_chucvu == 3 || $request->id_chucvu == 4) { // Nếu chức vụ chọn là PBT hoặc LT hoặc CHT
                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = 2;
                        $chucvu->save();

                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = 3;
                        $chucvu->save();

                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = 4;
                        $chucvu->save();
                    } else {
                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = $request->id_chucvu;
                        $chucvu->save();
                    }

                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 1;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();

                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 2;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();
                } else {
                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 1;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();

                }

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;

                return response()->json(['err_msg' => 'Không thành công'], 401);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã thêm sinh viên "' . $sinhvien->hoten . '" với mã số sinh viên "' . $sinhvien->mssv . '"!!!'];
            return response()->json($message, 200);
        }
    }

    public function putSuaSinhvien(Request $request, $id)
    {
        $sinhvien = SinhVien_Model::find($id);
        $user = User::where('ms_sv_canbo', '=', $sinhvien->mssv)->first();
        $tinhtranghoctap = TinhTrangHocTap_Model::where('id_sv', '=', $id)->first();
        $chucvu = ChucVuBCS_Model::where('id_sv', '=', $id)->get();
        $phanquyen = PhanQuyen_Model::where('id_user', '=', $user->id)->get();
        $message = [
            'mssv.required' => 'Mã số sinh viên không được bỏ trống',
            'hoten.required' => 'Họ tên không được bỏ trống',
            'cmnd.required' => 'CMND không được bỏ trống',
            'sdt_canhan.required' => 'Số điện thoại cá nhân không được bỏ trống',
            'diachi.required' => 'Địa chỉ không được bỏ trống',
            'email.required' => 'Email không được bỏ trống',
            'id_lop.required' => 'Chưa chọn lớp cho sinh viên',
        ];

        $validator = Validator::make($request->all(),
            [
                'mssv' => 'required',
                'hoten' => 'required',
                'gioitinh' => 'required',
                'ngaysinh' => 'required',
                'cmnd' => 'required',
                'sdt_canhan' => 'required',
                'diachi' => 'required',
                'email' => 'required',
                'id_lop' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();

            try {
                $sinhvien->mssv = $request->mssv;
                $sinhvien->hoten = $request->hoten;
                $sinhvien->gioitinh = $request->gioitinh;
                $sinhvien->ngaysinh = $request->ngaysinh;
                $sinhvien->cmnd = $request->cmnd;
                $sinhvien->sdt_canhan = $request->sdt_canhan;
                $sinhvien->sdt_giadinh = $request->sdt_giadinh;
                $sinhvien->diachi = $request->diachi;
                $sinhvien->email = $request->email;
                $sinhvien->id_lop = $request->id_lop;
                $sinhvien->save();

                $user->ms_sv_canbo = $request->mssv;
                $user->username = $request->mssv;
                $user->save();

                if ($request->hedaotao != null || $request->hedaotao != '') {
                    if ($request->hedaotao == 1) {
                        $tinhtranghoctap->tgbatdau = Carbon::now()->format('Y-m-d');
                        $tinhtranghoctap->tgketthuc = Carbon::now()->addYears(4)->format('Y-m-d');
                    } else {
                        $tinhtranghoctap->tgbatdau = Carbon::now()->format('Y-m-d');
                        $tinhtranghoctap->tgketthuc = Carbon::now()->addYears(3)->format('Y-m-d');
                    }
                    $tinhtranghoctap->save();
                }

                foreach ($phanquyen as $pq) { // Xóa hết phân quyền
                    $pq->delete();
                }

                foreach ($chucvu as $cv) { // Xóa hết chức vụ
                    $cv->delete();
                }

                if ($request->id_chucvu != '' || $request->id_chucvu != null) { //Tạo lại phân quyền và chức vụ
                    if ($request->id_chucvu == 2 || $request->id_chucvu == 3 || $request->id_chucvu == 4) { // Nếu chức vụ chọn là PBT hoặc LT hoặc CHT
                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = 2;
                        $chucvu->save();

                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = 3;
                        $chucvu->save();

                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = 4;
                        $chucvu->save();
                    } else {
                        $chucvu = new ChucVuBCS_Model;
                        $chucvu->id_sv = $sinhvien->id;
                        $chucvu->id_loaichucvu_bcs = $request->id_chucvu;
                        $chucvu->save();
                    }

                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 1;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();

                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 2;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();
                } else {
                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 1;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();

                }

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['err_msg' => 'Không thành công'], 401);
            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã cập nhật thông tin sinh viên "' . $sinhvien->hoten . '" với mã số sinh viên "' . $sinhvien->mssv . '"!!!'];

            return response()->json($message, 200);
        }
    }

    public function deleteXoaSinhVien($id)
    {
        $sinhvien = SinhVien_Model::find($id);
        $user = User::where('ms_sv_canbo', '=', $sinhvien->mssv)->first();
        $chucvu = ChucVuBCS_Model::where('id_sv', '=', $id)->get();
        $tinhtranghoctap = TinhTrangHocTap_Model::where('id_sv', '=', $id)->get();
        $phanquyen = PhanQuyen_Model::where('id_user', '=', $user->id)->get();
        $old_name = $sinhvien->hoten;
        $old_mssv = $sinhvien->mssv;
        DB::beginTransaction();
        try {
            foreach ($phanquyen as $pq) {
                $pq->delete();
            }

            foreach ($chucvu as $cv) {
                $cv->delete();
            }

            foreach ($tinhtranghoctap as $ttht) {
                $ttht->delete();
            }

            $user->delete();

            $sinhvien->delete();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
            return response()->json(['err_msg' => 'Không thành công'], 401);
        }

        $message = ['message' => 'Đã xóa thông tin sinh viên "' . $sinhvien->hoten . '" với mã số sinh viên "' . $sinhvien->mssv . '"!!!'];

        return response()->json($message, 200);
    }

    public function getIDLop($id_sv)
    {
        $id_lop = DB::table('tbl_sinhvien')->where('id', '=', $id_sv)->select('id_lop')->first();
        $response = ['id_lop' => $id_lop];
        return response()->json($response, 200);
    }

}
