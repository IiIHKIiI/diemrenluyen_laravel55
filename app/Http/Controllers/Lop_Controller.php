<?php

namespace App\Http\Controllers;

use App\Lop_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Lop_Controller extends Controller
{
    //

    public function getDanhSachLop_Nganh(Request $request)
    {
        try {
            $danhsachlop = DB::table('tbl_lop')->join('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
                ->select('tbl_lop.*', 'tbl_nganh.tennganh')
                ->where('id_nganh', '=', $request->id_nganh)
                ->get();

            $response = ['danhsach' => $danhsachlop];

            return response()->json($response, 200);
        } catch (Exception $e) {

            return response()->json($e);
        }
    }

    public function getDanhSachLop()
    {
        try {
            $danhsachlop = DB::table('tbl_lop')->join('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
                ->select('tbl_lop.*', 'tbl_nganh.tennganh')
                ->get();
            $response = ['danhsach' => $danhsachlop];
            return response()->json($response, 200);
        } catch (Exception $e) {

            return response()->json($e);
        }
    }

    public function getLopById($id)
    {
        try {
            $lop = DB::table('tbl_lop')->leftjoin('tbl_sinhvien', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
                ->leftjoin('tbl_nganh', 'tbl_nganh.id', '=', 'tbl_lop.id_nganh')
                ->leftjoin('tbl_bomon_donvi', 'tbl_bomon_donvi.id', '=', 'tbl_nganh.id_bomon_donvi')
                ->leftjoin('tbl_khoa', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
                ->leftjoin('tbl_tinhtranghoctap', 'tbl_tinhtranghoctap.id_sv', '=', 'tbl_sinhvien.id')
                ->leftjoin('tbl_chitiet_cvht', 'tbl_chitiet_cvht.id_lop', '=', 'tbl_lop.id')
                ->leftjoin('tbl_canbo', 'tbl_canbo.id', '=', 'tbl_chitiet_cvht.id_canbo')
                ->select('tbl_lop.*', 'tbl_bomon_donvi.tructhuoc', 'tbl_canbo.hotencanbo', 'tbl_lop.tenlop', 'tbl_nganh.tennganh', 'tbl_khoa.tenkhoa')
                ->where('tbl_lop.id', '=', $id)
                ->first();

            $siso = DB::table('tbl_lop')->leftjoin('tbl_sinhvien', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
                ->where('tbl_lop.id', '=', $id)
                ->count();
            $response = ['lop' => $lop, 'siso' => $siso];
            return response()->json($response, 200);
        } catch (Exception $e) {

            return response()->json($e);
        }
    }

    public function getLopCoVanHocTapByIdCanBo(Request $request)
    {
        try {
            $lop = DB::table('tbl_lop')
                ->leftjoin('tbl_chitiet_cvht', 'tbl_chitiet_cvht.id_lop', '=', 'tbl_lop.id')
                ->select('tbl_lop.id', 'tbl_lop.tenlop')
                ->where('tbl_chitiet_cvht.id_canbo', '=', $request->id_canbo)
                ->first();

            $dssinhviendanhgia = DB::table('tbl_bangdiemdanhgia')
                ->leftjoin('tbl_sinhvien', 'tbl_sinhvien.id', '=', 'tbl_bangdiemdanhgia.id_sv')
                ->leftjoin('tbl_lop', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')
                ->where([['tbl_lop.id', '=', $lop->id], ['tbl_bangdiemdanhgia.id_thoigiandanhgia', '=', $request->id_thoigiandanhgia]])
                ->select('tbl_bangdiemdanhgia.trangthai_cvht')
                ->get();

            $check = true;

            foreach ($dssinhviendanhgia as $sv) {
                if ($sv->trangthai_cvht === 0) {
                    $check = false;
                    break;
                }
            }

            if ($check) {
                $response = ['lop' => $lop, 'trangthai' => 1];
            } else {
                $response = ['lop' => $lop, 'trangthai' => 0];
            }

            return response()->json($response, 200);
        } catch (Exception $e) {

            return response()->json($e);
        }
    }

    public function getLopHoiDongKhoaCoVan(Request $request)
    {
        try {
            $lop = DB::table('tbl_lop')
                ->leftjoin('tbl_nganh', 'tbl_nganh.id', '=', 'tbl_lop.id_nganh')
                ->leftjoin('tbl_bomon_donvi', 'tbl_bomon_donvi.id', '=', 'tbl_nganh.id_bomon_donvi')
                ->leftjoin('tbl_khoa', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
                ->leftjoin('tbl_canbo', 'tbl_canbo.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
                ->select('tbl_lop.id', 'tbl_lop.tenlop')
                ->where('tbl_canbo.id', '=', $request->id_canbo)
                ->get();

            $response = ['lop' => $lop];

            return response()->json($response, 200);
        } catch (Exception $e) {

            return response()->json($e);
        }
    }

    public function postThemLop(Request $request)
    {
        $message = [
            'tenlop.required' => 'Tên lớp không được bỏ trống',
            'nambatdau.required' => 'Năm bắt đầu không được bỏ trống',
            'namketthuc.required' => 'Năm kết thức không được bỏ trống',
            'id_nganh.required' => 'Chưa chọn ngành cho lớp',
        ];

        $validator = Validator::make($request->all(),
            [
                'tenlop' => 'required',
                'nambatdau' => 'required',
                'namketthuc' => 'required',
                'id_nganh' => 'required',

            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                $lop = new Lop_Model;
                $lop->tenlop = $request->tenlop;
                $lop->nambatdau = $request->nambatdau;
                $lop->namketthuc = $request->namketthuc;
                $lop->hedaotao = $request->hedaotao;
                $lop->id_nganh = $request->id_nganh;
                $lop->save();

                $message = ['message' => 'Đã thêm lớp "' . $lop->tenlop . '!!!'];
                return response()->json($message, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function putSuaLop(Request $request, $id)
    {
        $lop = Lop_Model::find($id);
        $message = [
            'tenlop.required' => 'Tên lớp không được bỏ trống',
            'nambatdau.required' => 'Năm bắt đầu không được bỏ trống',
            'namketthuc.required' => 'Năm kết thức không được bỏ trống',
            'id_nganh.required' => 'Chưa chọn ngành cho lớp',
        ];

        $validator = Validator::make($request->all(),
            [
                'tenlop' => 'required',
                'nambatdau' => 'required',
                'namketthuc' => 'required',
                'id_nganh' => 'required',

            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                $lop->tenlop = $request->tenlop;
                $lop->nambatdau = $request->nambatdau;
                $lop->namketthuc = $request->namketthuc;
                $lop->hedaotao = $request->hedaotao;
                $lop->id_nganh = $request->id_nganh;
                $lop->save();

                $message = ['message' => 'Đã cập nhật thông tin lớp "' . $lop->tenlop . '!!!'];
                return response()->json($message, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function deleteXoaLop($id)
    {
        $lop = Lop_Model::find($id);
        $old_name = $lop->tenlop;
        try {

            $lop->delete();

            $message = ['message' => 'Đã xóa thông tin lớp "' . $old_name . '"!!!'];

            return response()->json($message, 200);
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1451: Lỗi có dữ liệu bên trong
            return response()->json(['maloi' => $maLoi]);

        }
    }
}
