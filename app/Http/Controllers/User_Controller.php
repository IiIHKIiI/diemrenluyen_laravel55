<?php

namespace App\Http\Controllers;

use App\PhanQuyen_Model;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use JWTAuthException;
use Validator;

class User_Controller extends Controller
{
    //

    public function createAccountSV(Request $request)
    {
        DB::beginTransaction();
        try {

            $user = new User;
            $user->username = $request->mssv;
            $user->password = bcrypt(123);
            $user->trangthai = 1;
            $user->ms_sv_canbo = $request->mssv;
            $user->loaiuser = 1;
            $user->isfirst = 1;
            $user->save();

            $phanquyen = new PhanQuyen_Model;
            $phanquyen->id_user = $user->id;
            $phanquyen->id_nhomquyen = 1;
            $phanquyen->trangthai = 1;
            $phanquyen->save();

            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

            return response()->json(['err_msg' => 'Không thành công'], 401);
        }

        return response()->json(['message' => 'Tạo thành công!!'], 200);
    }

    public function createAccountCB(Request $request)
    {
        DB::beginTransaction();
        try {

            $user = new User;
            $user->username = $request->macanbo;
            $user->password = bcrypt(123);
            $user->trangthai = 1;
            $user->ms_sv_canbo = $request->macanbo;
            $user->loaiuser = 2;
            $user->isfirst = 1;
            $user->save();

            foreach ($request->id_nhomquyen as $pq) {
                $phanquyen = new PhanQuyen_Model;
                $phanquyen->id_user = $user->id;
                $phanquyen->id_nhomquyen = $pq;
                $phanquyen->trangthai = 1;
                $phanquyen->save();
            }

            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

            return response()->json(['err_msg' => 'Không thành công'], 401);
        }

        return response()->json(['message' => 'Tạo thành công!!'], 200);
    }

    public function dangnhap(Request $request)
    {
        $message = [
            'username.required' => 'Tên đăng nhập không được bỏ trống',
            'password.required' => 'Mật khẩu không được bỏ trống',
        ];

        $validator = Validator::make($request->all(),
            [
                'username' => 'required',
                'password' => 'required',
            ], $message);

        $token = null;

        $credentials = $request->only('username', 'password');

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'err' => 'true',
                    ], 401);
                }
            } catch (JWTAuthException $e) {
                return response()->json([
                    'message' => 'Không thể tạo được token!',
                ], 500);
            }
            return response()->json(['token' => $token, 200]);
        }
    }

    public function deleteAccount($id)
    {
        $phanquyen = PhanQuyen_Model::where('id_user', '=', $id)->get();
        $user = User::find($id);

        DB::beginTransaction();
        try {

            foreach ($phanquyen as $pq) {
                $pq->delete();
            }

            $user->delete();

            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

            return response()->json(['err_msg' => 'Không thành công'], 401);
        }

        return response()->json(['message' => 'Tạo thành công!!'], 200);
    }

    public function refreshToken()
    {
        $token = JWTAuth::getToken();
        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            throw new AccessDeniedHttpException('The token is invalid');
        }
        return $this->response->withArray(['token' => $token]);
    }

    public function getUser(Request $request)
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    public function getInfoUser(Request $request)
    {
        try {
            /* Tìm tài khoản user đăng nhập */
            $user = JWTAuth::toUser($request->token);

            /* Tìm quyền user trong bảng phân quyền */
            $userRights = PhanQuyen_Model::where([['id_user', '=', $user->id], ['trangthai', '=', 1]])
                ->select('id_nhomquyen')
                ->get();
            $tennhomquyen = DB::table('tbl_phanquyen')
                ->leftjoin('tbl_nhomquyen', 'tbl_nhomquyen.id', '=', 'tbl_phanquyen.id_nhomquyen')
                ->where([['id_user', '=', $user->id], ['trangthai', '=', 1]])
                ->select(DB::raw('group_concat( tbl_nhomquyen.tennhomquyen SEPARATOR \', \'   ) AS tennhomquyen'))
                ->first();

            /* Kiểm tra quyền hạn User */
            if ($userRights) {
                if ($user->loaiuser == 3) { // Nếu User đăng nhập với tài khoản Administrator

                    return response()->json(['quyenhan' => $userRights, 'user' => $user], 200);

                } else if ($user->loaiuser == 2) { // Nếu User đăng nhập với tài khoản Cán Bộ
                    $canbo = DB::table('tbl_canbo')
                        ->leftjoin('tbl_bomon_donvi', 'tbl_canbo.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
                        ->select('tbl_canbo.*', 'tbl_bomon_donvi.tenbomon')
                        ->where('tbl_canbo.macanbo', '=', $user->ms_sv_canbo)
                        ->first();
                    if ($canbo) { // Nếu tồn tại sinh viên thì trả về thông tin sinh viên
                        /* Tìm chức vụ của cán bộ */
                        $chucvu = DB::table('tbl_chucvu_quanly')
                            ->leftjoin('tbl_quanly', 'tbl_chucvu_quanly.id', '=', 'tbl_quanly.id_chucvu_quanly')
                            ->where('tbl_quanly.id_canbo', '=', $canbo->id)
                            ->select(DB::raw('group_concat( tbl_chucvu_quanly.tenchucvu_quanly SEPARATOR \', \'   ) AS chucvu'))
                            ->first();

                        return response()->json(['quyenhan' => $userRights, 'user' => $user, 'canbo' => $canbo, 'chucvu' => $chucvu, 'tennhomquyen' => $tennhomquyen], 200);
                    } else {
                        $message = ['message' => 'Thông tin cán bộ không tồn tại. Vui lòng kiểm tra lại...'];
                        return response()->json($message, 404);
                    }
                } else { // Nếu User đăng nhập với tài khoản Sinh Viên
                    /* Tìm thông tin sinh viên */
                    $sv = DB::table('tbl_sinhvien')->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
                        ->select('tbl_sinhvien.*', 'tbl_lop.tenlop')
                        ->where('tbl_sinhvien.mssv', '=', $user->ms_sv_canbo)
                        ->first();

                    if ($sv) { // Nếu tồn tại sinh viên thì trả về thông tin sinh viên
                        /* Tìm chức vụ của sinh viên */
                        $chucvu = DB::table('tbl_chucvu_bcs')
                            ->leftjoin('tbl_loaichucvu_bcs', 'tbl_loaichucvu_bcs.id', '=', 'tbl_chucvu_bcs.id_loaichucvu_bcs')
                            ->where('tbl_chucvu_bcs.id_sv', '=', $sv->id)
                            ->select(DB::raw('group_concat( tbl_loaichucvu_bcs.tenchucvu_bcs SEPARATOR \', \'   ) AS chucvu'))
                            ->first();

                        return response()->json(['quyenhan' => $userRights, 'user' => $user, 'sv' => $sv, 'chucvu' => $chucvu, 'tennhomquyen' => $tennhomquyen], 200);
                    } else {
                        $message = ['message' => 'Thông tin sinh viên không tồn tại. Vui lòng kiểm tra lại...'];
                        return response()->json($message, 404);
                    }
                }

            } else {
                $err = ['err' => 'Bạn không có quyền hạn để truy cập vào phân vùng này. Vui lòng liên hệ với người quản lý...'];
                return response()->json($err, 200);
            }

        } catch (JWTException $e) {

            return response()->json(
                ['message' => 'Không tìm được thông tin người dùng này', 'err' => $e]
            );

        }

    }

    public function getDanhSachUserByLop(Request $request)
    {
        $danhsachuser = DB::table('tbl_sinhvien')
            ->leftjoin('tbl_users', 'tbl_users.ms_sv_canbo', '=', 'tbl_sinhvien.mssv')
            ->leftjoin('tbl_phanquyen', 'tbl_phanquyen.id_user', '=', 'tbl_users.id')
            ->leftjoin('tbl_nhomquyen', 'tbl_phanquyen.id_nhomquyen', '=', 'tbl_nhomquyen.id')
            ->leftjoin('tbl_lop', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')
            ->where(function ($query) use ($request) {
                $query->where('tbl_lop.id', 'like', $request->id_lop)
                    ->whereNotNull('tbl_users.ms_sv_canbo');
            })
            ->select(DB::raw('group_concat( tbl_nhomquyen.tennhomquyen SEPARATOR \', \'   ) AS quyen'), 'tbl_users.id', 'tbl_lop.tenlop', 'tbl_sinhvien.mssv', 'tbl_sinhvien.hoten', 'tbl_users.trangthai')
            ->groupBy('tbl_users.id', 'tbl_lop.tenlop', 'tbl_sinhvien.mssv', 'tbl_sinhvien.hoten', 'tbl_users.trangthai')
            ->get();

        $danhsachuser_chuacotaikhoan = DB::table('tbl_sinhvien')->leftjoin('tbl_users', 'tbl_users.ms_sv_canbo', '=', 'tbl_sinhvien.mssv')
            ->leftjoin('tbl_phanquyen', 'tbl_phanquyen.id_user', '=', 'tbl_users.id')
            ->leftjoin('tbl_nhomquyen', 'tbl_phanquyen.id_nhomquyen', '=', 'tbl_nhomquyen.id')
            ->leftjoin('tbl_lop', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')
            ->where(function ($query) use ($request) {
                $query->where('tbl_lop.id', 'like', $request->id_lop)
                    ->whereNull('tbl_users.ms_sv_canbo');
            })
            ->select('tbl_sinhvien.mssv', 'tbl_sinhvien.hoten', 'tbl_lop.tenlop')
            ->distinct()->get();

        $response = ['danhsachuser' => $danhsachuser, 'danhsachuser_chuacotaikhoan' => $danhsachuser_chuacotaikhoan];
        return response()->json($response, 200);
    }

    public function getDanhSachUserByBoMon(Request $request)
    {
        $danhsachuser = DB::table('tbl_canbo')->leftjoin('tbl_users', 'tbl_users.ms_sv_canbo', '=', 'tbl_canbo.macanbo')
            ->leftjoin('tbl_phanquyen', 'tbl_phanquyen.id_user', '=', 'tbl_users.id')
            ->leftjoin('tbl_nhomquyen', 'tbl_phanquyen.id_nhomquyen', '=', 'tbl_nhomquyen.id')
            ->leftjoin('tbl_bomon_donvi', 'tbl_bomon_donvi.id', '=', 'tbl_canbo.id_bomon_donvi')
            ->where(function ($query) use ($request) {
                $query->where('tbl_bomon_donvi.id', 'like', $request->id_bomon_donvi)
                    ->whereNotNull('tbl_users.ms_sv_canbo');
            })
            ->select(DB::raw('group_concat( tbl_nhomquyen.tennhomquyen SEPARATOR \', \'   ) AS quyen'), 'tbl_users.id', 'tbl_canbo.macanbo', 'tbl_canbo.hotencanbo', 'tbl_bomon_donvi.tenbomon', 'tbl_users.trangthai')
            ->groupBy('tbl_users.id', 'tbl_canbo.macanbo', 'tbl_canbo.hotencanbo', 'tbl_bomon_donvi.tenbomon', 'tbl_users.trangthai')
            ->get();

        $danhsachuser_chuacotaikhoan = DB::table('tbl_canbo')->leftjoin('tbl_users', 'tbl_users.ms_sv_canbo', '=', 'tbl_canbo.macanbo')
            ->leftjoin('tbl_phanquyen', 'tbl_phanquyen.id_user', '=', 'tbl_users.id')
            ->leftjoin('tbl_nhomquyen', 'tbl_phanquyen.id_nhomquyen', '=', 'tbl_nhomquyen.id')
            ->leftjoin('tbl_bomon_donvi', 'tbl_bomon_donvi.id', '=', 'tbl_canbo.id_bomon_donvi')
            ->where(function ($query) use ($request) {
                $query->where('tbl_bomon_donvi.id', 'like', $request->id_bomon_donvi)
                    ->whereNull('tbl_users.ms_sv_canbo');
            })
            ->select('tbl_canbo.macanbo', 'tbl_canbo.hotencanbo', 'tbl_bomon_donvi.tenbomon')
            ->distinct()->get();

        $response = ['danhsachuser' => $danhsachuser, 'danhsachuser_chuacotaikhoan' => $danhsachuser_chuacotaikhoan];
        return response()->json($response, 200);
    }

    public function putLockOrOpenAccount(Request $request, $id)
    {
        $user = User::find($id);

        $user->trangthai = $request->trangthai;
        $user->save();

        if ($request->trangthai == 0) {
            return response()->json(['message' => 'Đã khóa tài khoản'], 200);
        } else {
            return response()->json(['message' => 'Đã kích hoạt tài khoản'], 200);
        }
    }

    public function resetPass($id)
    {
        $user = User::find($id);

        $user->password = bcrypt(123);
        $user->isfirst = 0;
        $user->save();

        return response()->json(['message' => 'Đã khôi phục lại mật khẩu mặc định cho tài khoản'], 200);
    }

    public function doiMatKhau(Request $request, $id_user)
    {
        $user = User::find($id_user);

        $user->password = bcrypt($request->matkhaumoi);
        $user->isfirst = 0;
        $user->save();

        return response()->json(['message' => 'Đã thay đổi mật khẩu'], 200);
    }

}
