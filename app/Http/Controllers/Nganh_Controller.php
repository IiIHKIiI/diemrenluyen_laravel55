<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;
use App\Nganh_Model;
use Illuminate\Database\QueryException;
class Nganh_Controller extends Controller
{
    //
    public function postDanhSachNganh_ByKhoa(Request $request) {
        try{
            $danhsachnganh = DB::table('tbl_khoa')->join('tbl_bomon_donvi', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
                                                ->join('tbl_nganh', 'tbl_bomon_donvi.id', '=', 'tbl_nganh.id_bomon_donvi')
                                                ->select('tbl_nganh.*', 'tbl_bomon_donvi.tenbomon', 'tbl_khoa.tenkhoa')
                                                ->where('tbl_khoa.id', '=', $request->id_khoa)
                                                ->get();

            $response = ['danhsach' => $danhsachnganh];
            
            return response()->json($response, 200);
        }catch(Exception $e){
                        
            return response()->json($e);
        }  
    }

    public function getDanhSachNganh() {
        try{
            $danhsachnganh = DB::table('tbl_khoa')->leftJoin('tbl_bomon_donvi', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
                                                ->join('tbl_nganh', 'tbl_bomon_donvi.id', '=', 'tbl_nganh.id_bomon_donvi')
                                                ->select('tbl_nganh.*', 'tbl_bomon_donvi.tenbomon', 'tbl_khoa.tenkhoa')
                                                ->get();

            $response = ['danhsach' => $danhsachnganh];
            
            return response()->json($response, 200);
        }catch(Exception $e){
                        
            return response()->json($e);
        }  
        
    }

    public function getNganhById($id){
        try{
            $danhsachnganh = DB::table('tbl_nganh')->join('tbl_bomon_donvi', 'tbl_nganh.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
                                                ->join('tbl_khoa', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
                                                ->select('tbl_nganh.*', 'tbl_bomon_donvi.tructhuoc')
                                                ->where('tbl_nganh.id', '=', $id)
                                                ->first();
            $response = ['nganh' => $danhsachnganh];
            return response()->json($response, 200);
        }catch(Exception $e){
                    
            return response()->json($e);
        }  
    }

    public function postThemNganh(Request $request) {
        $message = [
            'manganh.required' => 'Mã ngành không được bỏ trống',
            'tennganh.required' => 'Tên ngành không được bỏ trống',
            'id_bomon_donvi.required' => 'Chưa chọn tổ bộ môn của khoa',
        ];

        $validator = Validator::make($request->all(),
        [
            'manganh'   => 'required',
            'tennganh'  => 'required',
            'id_bomon_donvi' => 'required'
            

        ], $message);

        if($validator->fails()){
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors()
            ]);
        }else{
            try{
                $nganh           = new Nganh_Model;
                $nganh->manganh   = $request->manganh;
                $nganh->tennganh  = $request->tennganh;
                $nganh->id_bomon_donvi  = $request->id_bomon_donvi;
                $nganh->save();

                $message = ['message' => 'Đã thêm ngành "' . $nganh->tennganh . '" với mã ngành "' . $nganh->manganh . '"!!!'];
                return response()->json($message, 200);

            }catch(QueryException $e){   
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            } 
        }
    }

    public function putSuaNganh(Request $request, $id) {
        $nganh   = Nganh_Model::find($id);
        $message = [
            'manganh.required' => 'Mã ngành không được bỏ trống',
            'tennganh.required' => 'Tên ngành không được bỏ trống',
            'id_bomon_donvi.required' => 'Chưa chọn tổ bộ môn của khoa',
        ];

        $validator = Validator::make($request->all(),
        [
            'manganh'   => 'required',
            'tennganh'  => 'required',
            'id_bomon_donvi' => 'required'
            

        ], $message);

        if($validator->fails()){
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors()
            ]);
        }else{
            try{
                $nganh->manganh   = $request->manganh;
                $nganh->tennganh  = $request->tennganh;
                $nganh->id_bomon_donvi  = $request->id_bomon_donvi;
                $nganh->save();

                $message = ['message' => 'Đã cập nhật thông tin ngành "' .$nganh->tennganh . '"!!!'];

                return response()->json($message, 200);
            }catch(QueryException $e){   
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            } 
        }
    }

    public function deleteXoaNganh($id) {
        $nganh   = Nganh_Model::find($id);
        $old_name = $nganh->tennganh;
            try{
            
            $nganh->delete();

            $message = ['message' => 'Đã xóa thông tin ngành "' . $old_name . '"!!!'];

            return response()->json($message, 200);
        }catch(QueryException $e){   
            $maLoi = $e->errorInfo[1]; // 1451: Lỗi có dữ liệu bên trong
            return response()->json(['maloi' => $maLoi]);

        }
    }
}
