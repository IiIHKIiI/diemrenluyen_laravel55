<?php

namespace App\Http\Controllers;

use App\ThongBao_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ThongBao_Controller extends Controller
{
    //
    public function getAllDanhSachThongBao()
    {
        $danhsach = DB::table('tbl_thongbaochung')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id', '=', 'tbl_thongbaochung.id_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_thongbaochung.id_user')
            ->leftjoin('tbl_canbo', 'tbl_canbo.macanbo', '=', 'tbl_users.ms_sv_canbo')
            ->select('tbl_thongbaochung.*', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc', 'tbl_canbo.hotencanbo')
            ->get();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getDanhSachThongBaoHienThi()
    {
        $danhsach = DB::table('tbl_thongbaochung')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id', '=', 'tbl_thongbaochung.id_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_thongbaochung.id_user')
            ->leftjoin('tbl_canbo', 'tbl_canbo.macanbo', '=', 'tbl_users.ms_sv_canbo')
            ->where('tbl_thongbaochung.trangthai', '=', 1)
            ->select('tbl_thongbaochung.*', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc', 'tbl_canbo.hotencanbo')
            ->get();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getInfoThongBao($id)
    {
        $thongtin = DB::table('tbl_thongbaochung')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id', '=', 'tbl_thongbaochung.id_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_thongbaochung.id_user')
            ->leftjoin('tbl_canbo', 'tbl_canbo.macanbo', '=', 'tbl_users.ms_sv_canbo')
            ->where('tbl_thongbaochung.id', '=', $id)
            ->select('tbl_thongbaochung.*', 'tbl_hocky.id_namhoc',  'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc', 'tbl_canbo.hotencanbo')
            ->first();

        $response = ['thongtin' => $thongtin];

        return response()->json($response, 200);
    }
    
    public function postThemThongBao(Request $request)
    {
        $message = [
            'id_hocky.required' => 'Bạn chưa chọn học kỳ thông báo',
            'tieude.required' => 'Bạn chưa điền tiêu đề bài thông báo',
            'noidung.required' => 'Bạn chưa điền nội dung bài thông báo',
            'id_user.required' => 'Chưa có thông tin tài khoản đăng bài',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_hocky' => 'required',
                'tieude' => 'required',
                'noidung' => 'required',
                'id_user' => 'required',
                'trangthai' => 'required',

            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                $thongbao = new ThongBao_Model;
                $thongbao->id_hocky = $request->id_hocky;
                $thongbao->tieude = $request->tieude;
                $thongbao->noidung = $request->noidung;
                $thongbao->id_user = $request->id_user;
                $thongbao->trangthai = $request->trangthai;
                if ($request->hasFile('filename_thongbao')) {
                    $name = $request->filename_thongbao->getClientOriginalName();
                    // $path = $request->filename_quyetdinh->storeAs('kyluat/' . $lop->tenlop . '/', $name);
                    $path = $request->filename_thongbao->storeAs('ThongBaoDanhGia/', $name);
                    $kyluat->filename_quyetdinh = $name;
                }
                $thongbao->save();

                $message = ['message' => 'Đã thêm thông tin thông báo đánh giá điểm rèn luyện!!!'];
                return response()->json($message, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function putSuaThongBao(Request $request, $id)
    {
        $thongbao = ThongBao_Model::find($id);
        $message = [
            'id_hocky.required' => 'Bạn chưa chọn học kỳ thông báo',
            'tieude.required' => 'Bạn chưa điền tiêu đề bài thông báo',
            'noidung.required' => 'Bạn chưa điền nội dung bài thông báo',
            'id_user.required' => 'Chưa có thông tin tài khoản đăng bài',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_hocky' => 'required',
                'tieude' => 'required',
                'noidung' => 'required',
                'id_user' => 'required',
                'trangthai' => 'required',

            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {

                $thongbao->id_hocky = $request->id_hocky;
                $thongbao->tieude = $request->tieude;
                $thongbao->noidung = $request->noidung;
                $thongbao->id_user = $request->id_user;
                $thongbao->trangthai = $request->trangthai;
                if ($request->hasFile('filename_thongbao')) {
                    $name = $request->filename_thongbao->getClientOriginalName();
                    // $path = $request->filename_quyetdinh->storeAs('kyluat/' . $lop->tenlop . '/', $name);
                    $path = $request->filename_thongbao->storeAs('ThongBaoDanhGia/', $name);
                    $kyluat->filename_quyetdinh = $name;
                }
                $thongbao->save();

                $message = ['message' => 'Đã cập nhật thông tin thông báo đánh giá điểm rèn luyện!!!'];
                return response()->json($message, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function deleteXoaThongBao(Request $request, $id)
    {
        $thongbao = ThongBao_Model::find($id);
        try {

            $thongbao->delete();

            $message = ['message' => 'Đã xóa thông tin thông báo đánh giá điểm rèn luyện!!!'];
            return response()->json($message, 200);

        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }
    }

     public function putLockOrOpenThongBao(Request $request, $id)
    {
        $tb = ThongBao_Model::find($id);

        $tb->trangthai = $request->trangthai;
        $tb->save();

        if ($request->trangthai == 0) {
            return response()->json(['message' => 'Đã dừng hiển thị thông báo'], 200);
        } else {
            return response()->json(['message' => 'Đã hiển thị lại thông báo'], 200);
        }
    }

}
