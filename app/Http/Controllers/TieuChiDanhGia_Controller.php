<?php

namespace App\Http\Controllers;

use App\ChiTietTieuChiDanhGia_Model;
use App\QuyetDinhTieuChi_Model;
use App\TieuChiDanhGia_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class TieuChiDanhGia_Controller extends Controller
{
    //

    public function getDanhSachQuyetDinh()
    {
        $danhsach = DB::table('tbl_tieuchidanhgia')
            ->leftjoin('tbl_quyetdinhtieuchi', 'tbl_tieuchidanhgia.id_quyetdinhtieuchi', '=', 'tbl_quyetdinhtieuchi.id')
            ->select('tbl_tieuchidanhgia.*', 'tbl_quyetdinhtieuchi.maquyetdinh')
            ->get();

        $response = ['danhsach' => $danhsach];
        return response()->json($response, 200);
    }

    public function getInfoQD($id_quyetdinh)
    {
        $qd = DB::table('tbl_tieuchidanhgia')
            ->leftjoin('tbl_quyetdinhtieuchi', 'tbl_tieuchidanhgia.id_quyetdinhtieuchi', '=', 'tbl_quyetdinhtieuchi.id')
            ->where('tbl_tieuchidanhgia.id_quyetdinhtieuchi', '=', $id_quyetdinh)
            ->select('tbl_tieuchidanhgia.*', 'tbl_quyetdinhtieuchi.maquyetdinh', 'tbl_quyetdinhtieuchi.filename_quyetdinh')
            ->first();

        $response = ['thongtin' => $qd];
        return response()->json($response, 200);
    }

    public function postThemQuyetDinhTieuChi(Request $request)
    {
        $message = [
            'filename_quyetdinh.required' => 'Bạn chưa chọn tệp quyết định các tiêu chí đánh giá',
            'tgbatdau.required' => 'Bạn chưa chọn thời gian bắt đầu hiệu lực tiêu chí đánh giá',
        ];

        $validator = Validator::make($request->all(),
            [
                'filename_quyetdinh' => 'required',
                'tgbatdau' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                $qdtc = new QuyetDinhTieuChi_Model;
                if ($request->hasFile('filename_quyetdinh')) {
                    $name = $request->filename_quyetdinh->getClientOriginalName();
                    // $maquyetdinh = '16/2015/QĐ-BGDĐT';
                    $maquyetdinh =  basename($name, '.'.$request->filename_quyetdinh->getClientOriginalExtension());
                    $path = $request->filename_quyetdinh->storeAs('QuyetDinhTieuChiDanhGia/' . $maquyetdinh . '/', $name);
                    $qdtc->filename_quyetdinh = $name;
                    $qdtc->maquyetdinh = $maquyetdinh;
                }

                $qdtc->save();

                $tcdg = new TieuChiDanhGia_Model;
                $tcdg->id_quyetdinhtieuchi = $qdtc->id;
                $tcdg->trangthai = 1;
                $tcdg->tgbatdau = $request->tgbatdau;
                $tcdg->save();

                $response = ['message' => 'Đã thêm quyết định tiêu chí đánh giá thành công. Tiến hành thêm các tiều chí vào...!!!', 'id_quyetdinhtieuchi' => $qdtc->id];
                return response()->json($response, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function putCapNhatThoiGianKetThuc(Request $request, $id)
    {
        $tcdg = TieuChiDanhGia_Model::find($id);

        $message = [
            'tgketthuc.required' => 'Bạn chưa chọn thời gian kết thúc',
        ];

        $validator = Validator::make($request->all(),
            [
                'tgketthuc' => 'required',

            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                $tcdg->trangthai = 0;
                $tcdg->tgketthuc = $request->tgketthuc;
                $tcdg->save();

                $qd = QuyetDinhTieuChi_Model::find($tcdg->id_quyetdinhtieuchi);

                $message = ['message' => 'Đã cập nhật thời gian kết thúc và khóa quyết định "' . $qd->maquyetdinh . '!!!'];
                return response()->json($message, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function putSuaQuyetDinh(Request $request, $id)
    {
        $qd = QuyetDinhTieuChi_Model::find($id);
        $tcdg = TieuChiDanhGia_Model::find($id);
        $message = [
            'tgbatdau.required' => 'Bạn chưa chọn thời gian bắt đầu hiệu lực tiêu chí đánh giá',
        ];

        $validator = Validator::make($request->all(),
            [
                'tgbatdau' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                if ($request->hasFile('filename_quyetdinh')) {
                    $name = $request->filename_quyetdinh->getClientOriginalName();
                    $path = $request->filename_quyetdinh->storeAs('QuyetDinhTieuChiDanhGia/' . $name . '/', $name);
                    $qdtc->filename_quyetdinh = $name;
                    $qdtc->maquyetdinh = $name;
                }

                $qdtc->save();

                $tcdg->id_quyetdinhtieuchi = $qdtc->id;
                $tcdg->tgbatdau = $request->tgbatdau;
                $tcdg->save();

                $response = ['message' => 'Đã cập nhật quyết định thành lập tiêu chí đánh giá'];
                return response()->json($response, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function deleteXoaQuyetDinh($id)
    {
        $qd = QuyetDinhTieuChi_Model::find($id);
        $tcdg = TieuChiDanhGia_Model::find($id);
        $cttc = ChiTietTieuChiDanhGia_Model::where('id_quyetdinhtieuchi', '=', $id)->get();
        $old_name = $qd->maquyetdinh;
        try {

            foreach ($cttc as $ct) {
                $ct->delete();
            }

            $tcdg->delete();

            $qd->delete();

            $message = ['message' => 'Đã xóa thông tin quyết định và các tiêu chí liên quan đến quyết định "' . $old_name . '"!!!'];

            return response()->json($message, 200);
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1451: Lỗi có dữ liệu bên trong
            return response()->json(['maloi' => $maLoi]);

        }
    }
}
