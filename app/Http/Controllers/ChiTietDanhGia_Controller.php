<?php

namespace App\Http\Controllers;

use App\BangDiemDanhGia_Model;
use App\ChiTietDanhGia_Model;
use Excel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class ChiTietDanhGia_Controller extends Controller
{
    //

    public function getChiTietDanhGia(Request $request)
    {
        $ctdg = DB::table('tbl_chitietdanhgia as a')
            ->leftjoin('tbl_bangdiemdanhgia as b', 'b.id', '=', 'a.id_bangdiemdanhgia')
            ->leftjoin('tbl_thoigiandanhgia as c', 'c.id', '=', 'b.id_thoigiandanhgia')
            ->leftjoin('tbl_tieuchidanhgia as d', 'd.id', '=', 'c.id_tieuchidanhgia')
            ->leftjoin('tbl_chitiettieuchi as e', 'e.id', '=', 'a.id_tieuchi')
            ->where([['b.id_sv', '=', $request->id_sv], ['b.id_thoigiandanhgia', '=', $request->id_thoigiandanhgia], ['a.loaiuserdanhgia', '=', $request->loaiuserdanhgia]])
            ->select('a.id_tieuchi', 'a.diem', 'e.id_tieuchicha', 'a.filename_minhchung')
            ->get();

        $response = ['ctdg' => $ctdg];
        return response()->json($response, 200);
    }

    public function getChiTietDanhGiaByIdBangDiemDanhGia($id_bangdiemdanhgia)
    {
        $ctdg_sv = ChiTietDanhGia_Model::where([['id_bangdiemdanhgia', $id_bangdiemdanhgia], ['loaiuserdanhgia', 1]])->get();
        $ctdg_bcs = ChiTietDanhGia_Model::where([['id_bangdiemdanhgia', $id_bangdiemdanhgia], ['loaiuserdanhgia', 2]])->get();
        $ctdg_cvht = ChiTietDanhGia_Model::where([['id_bangdiemdanhgia', $id_bangdiemdanhgia], ['loaiuserdanhgia', 3]])->get();
        $ctdg_hdkhoa = ChiTietDanhGia_Model::where([['id_bangdiemdanhgia', $id_bangdiemdanhgia], ['loaiuserdanhgia', 4]])->get();
        $ctdg_hdtruong = ChiTietDanhGia_Model::where([['id_bangdiemdanhgia', $id_bangdiemdanhgia], ['loaiuserdanhgia', 5]])->get();

        $response = ['ctdg_sv' => $ctdg_sv, 'ctdg_bcs' => $ctdg_bcs, 'ctdg_cvht' => $ctdg_cvht, 'ctdg_hdkhoa' => $ctdg_hdkhoa, 'ctdg_hdtruong' => $ctdg_hdtruong];

        return response()->json($response, 200);
    }

    public function checkDanhGia(Request $request)
    {
        $check = DB::table('tbl_bangdiemdanhgia')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_bangdiemdanhgia.id_thoigiandanhgia', '=', 'tbl_thoigiandanhgia.id')
            ->where([['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky], ['tbl_bangdiemdanhgia.id_sv', '=', $request->id_sv]])
            ->select('tbl_bangdiemdanhgia.*')
            ->first();

        $response = ['bddanhgia' => $check];
        return response()->json($response, 200);
    }

    public function postTaoBangDiemDanhGia(Request $request)
    {

        $check = BangDiemDanhGia_Model::where([['id_sv', '=', $request->id_sv], ['id_thoigiandanhgia', '=', $request->id_thoigiandanhgia]])->count();

        if ($check == 0) {
            $bangdiemdanhgia = new BangDiemDanhGia_Model;
            $bangdiemdanhgia->id_sv = $request->id_sv;
            $bangdiemdanhgia->id_thoigiandanhgia = $request->id_thoigiandanhgia;
            $bangdiemdanhgia->trangthaichung = 0;
            $bangdiemdanhgia->trangthai_sv = 0;
            $bangdiemdanhgia->trangthai_bancansu = 0;
            $bangdiemdanhgia->trangthai_cvht = 0;
            $bangdiemdanhgia->trangthai_hoidongkhoa = 0;
            $bangdiemdanhgia->trangthai_hoidongtruong = 0;
            $bangdiemdanhgia->save();
        }

        $response = ['check' => $check];
        return response()->json($response, 200);
    }

    public function postThemDanhGia_SV(Request $request)
    {
        $bangdiemdanhgia = BangDiemDanhGia_Model::find($request->id_bangdiemdanhgia);

        $sinhvien = DB::table('tbl_sinhvien')->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->join('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
            ->join('tbl_bomon_donvi', 'tbl_nganh.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->join('tbl_khoa', 'tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
            ->select('tbl_sinhvien.mssv', 'tbl_lop.tenlop', 'tbl_nganh.tennganh', 'tbl_khoa.tenkhoa')
            ->where('tbl_sinhvien.id', '=', $request->id_sv)
            ->first();

        $message = [
            'id_user.required' => 'User không tồn tại',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_user' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();
            try {

                switch ($request->loaiuserdanhgia) {
                    case 1: /* Loại user đánh giá sinh viên */
                        $bangdiemdanhgia->tong_diemdanhgia_sv = $request->tongdiem;
                        $bangdiemdanhgia->trangthai_sv = 1;
                        $bangdiemdanhgia->save();
                        break;
                    case 2: /* Loại user đánh giá ban cán sự */
                        $bangdiemdanhgia->tong_diemdanhgia_bancansu = $request->tongdiem;
                        $bangdiemdanhgia->trangthai_bancansu = 1;
                        $bangdiemdanhgia->save();
                        break;

                    case 3: /* Loại user đánh giá cố vấn học tập */
                        $bangdiemdanhgia->tong_diemdanhgia_cvht = $request->tongdiem;
                        $bangdiemdanhgia->trangthai_cvht = 1;
                        $bangdiemdanhgia->save();
                        break;

                    case 4: /* Loại user đánh giá hội đồng khoa */
                        $bangdiemdanhgia->tong_diemdanhgia_hoidongkhoa = $request->tongdiem;
                        $bangdiemdanhgia->trangthai_hoidongkhoa = 1;
                        $bangdiemdanhgia->save();
                        break;

                    case 5: /* Loại user đánh giá hội đồng trường*/
                        $bangdiemdanhgia->tong_diemdanhgia_hoidongtruong = $request->tongdiem;
                        $bangdiemdanhgia->trangthai_hoidongtruong = 1;
                        $bangdiemdanhgia->trangthaichung = 1;
                        $bangdiemdanhgia->save();
                        break;
                }

                foreach (json_decode($request->ctdanhgia) as $key => $diemdg) {
                    $chitietdanhgia = new ChiTietDanhGia_Model;
                    $chitietdanhgia->id_user = $request->id_user;
                    $chitietdanhgia->id_bangdiemdanhgia = $request->id_bangdiemdanhgia;
                    $chitietdanhgia->id_tieuchi = $key;
                    $chitietdanhgia->diem = $diemdg;
                    $chitietdanhgia->loaiuserdanhgia = $request->loaiuserdanhgia;
                    $chitietdanhgia->save();
                }

                if ($request->dsminhchung != null) {
                    foreach ($request->dsminhchung as $key => $mc) {
                        $ctdg_tmp = DB::table('tbl_chitietdanhgia')
                            ->where([['id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['loaiuserdanhgia', '=', $request->loaiuserdanhgia], ['id_tieuchi', '=', $key]])
                            ->select('id')
                            ->first();
                        $ctdg_themminhchung = $ctdg_themminhchung = ChiTietDanhGia_Model::find($ctdg_tmp->id);
                        $name = $mc->getClientOriginalName();
                        $path = $mc->storeAs('MinhChung/' . $sinhvien->tenkhoa . '/' . $sinhvien->tennganh . '/' . $sinhvien->tenlop . '/' . $sinhvien->mssv, $name);
                        $ctdg_themminhchung->filename_minhchung = $name;
                        $ctdg_themminhchung->save();
                    }
                }

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['err_msg' => 'Không thành công'], 401);
            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã lưu thông tin đánh giá !!!'];

            return response()->json($message, 200);
        }
    }

    public function putSuaDanhGia(Request $request)
    {
        $bangdiemdanhgia = BangDiemDanhGia_Model::find($request->id_bangdiemdanhgia);

        $sinhvien = DB::table('tbl_sinhvien')->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->join('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
            ->join('tbl_bomon_donvi', 'tbl_nganh.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->join('tbl_khoa', 'tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
            ->select('tbl_sinhvien.mssv', 'tbl_lop.tenlop', 'tbl_nganh.tennganh', 'tbl_khoa.tenkhoa')
            ->where('tbl_sinhvien.id', '=', $request->id_sv)
            ->first();

        $danhgia = DB::table('tbl_chitietdanhgia')
            ->where([['tbl_chitietdanhgia.id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['tbl_chitietdanhgia.loaiuserdanhgia', '=', $request->loaiuserdanhgia]])
            ->select('tbl_chitietdanhgia.id')
            ->get();

        $message = [
            'id_user.required' => 'User không tồn tại',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_user' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();
            try {
                foreach ($danhgia as $dg) {
                    $tmp = ChiTietDanhGia_Model::find($dg->id);
                    $tmp->delete();
                }

                
                switch ($request->loaiuserdanhgia) {
                    case 1: /* Loại user đánh giá sinh viên */
                        $bangdiemdanhgia->tong_diemdanhgia_sv = $request->tongdiem;
                        $bangdiemdanhgia->save();
                        break;
                    case 2: /* Loại user đánh giá ban cán sự */
                        $bangdiemdanhgia->tong_diemdanhgia_bancansu = $request->tongdiem;
                        $bangdiemdanhgia->save();
                        break;

                    case 3: /* Loại user đánh giá cố vấn học tập */
                        $bangdiemdanhgia->tong_diemdanhgia_cvht = $request->tongdiem;
                        $bangdiemdanhgia->save();
                        break;

                    case 4: /* Loại user đánh giá hội đồng khoa */
                        $bangdiemdanhgia->tong_diemdanhgia_hoidongkhoa = $request->tongdiem;
                        $bangdiemdanhgia->save();
                        break;

                    case 5: /* Loại user đánh giá hội đồng trường*/
                        $bangdiemdanhgia->tong_diemdanhgia_hoidongtruong = $request->tongdiem;
                        $bangdiemdanhgia->save();
                        break;
                }

                foreach (json_decode($request->ctdanhgia) as $key => $diemdg) {
                    $chitietdanhgia = new ChiTietDanhGia_Model;
                    $chitietdanhgia->id_user = $request->id_user;
                    $chitietdanhgia->id_bangdiemdanhgia = $request->id_bangdiemdanhgia;
                    $chitietdanhgia->id_tieuchi = $key;
                    $chitietdanhgia->diem = $diemdg;
                    $chitietdanhgia->loaiuserdanhgia = $request->loaiuserdanhgia;
                    $chitietdanhgia->save();
                }

                if ($request->dsminhchung != null) {
                    foreach ($request->dsminhchung as $key => $mc) {
                        $ctdg_tmp = DB::table('tbl_chitietdanhgia')
                            ->where([['id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['loaiuserdanhgia', '=', $request->loaiuserdanhgia], ['id_tieuchi', '=', $key]])
                            ->select('id')
                            ->first();
                        $ctdg_themminhchung = $ctdg_themminhchung = ChiTietDanhGia_Model::find($ctdg_tmp->id);
                        $name = $mc->getClientOriginalName();
                        $path = $mc->storeAs('MinhChung/' . $sinhvien->tenkhoa . '/' . $sinhvien->tennganh . '/' . $sinhvien->tenlop . '/' . $sinhvien->mssv, $name);
                        $ctdg_themminhchung->filename_minhchung = $name;
                        $ctdg_themminhchung->save();
                    }
                }

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['err_msg' => 'Không thành công'], 401);
            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã lưu thông tin đánh giá !!!'];

            return response()->json($message, 200);
        }
    }

    public function getLichSuDanhGia($id_sv)
    {
        $danhsach = DB::table('tbl_bangdiemdanhgia')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id', '=', 'tbl_thoigiandanhgia.id_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_namhoc.id', '=', 'tbl_hocky.id_namhoc')
            ->where('tbl_bangdiemdanhgia.id_sv', '=', $id_sv)
            ->select('tbl_bangdiemdanhgia.*', 'tbl_thoigiandanhgia.id_hocky', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['danhsach' => $danhsach];
        return response()->json($response, 200);
    }

    public function duyetNhanhDanhGia(Request $request)
    {
        $ctdg = DB::table('tbl_chitietdanhgia')
            ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id', '=', 'tbl_chitietdanhgia.id_bangdiemdanhgia')
            ->where([['tbl_bangdiemdanhgia.id_sv', '=', $request->id_sv], ['tbl_bangdiemdanhgia.id_thoigiandanhgia', '=', $request->id_thoigiandanhgia], ['tbl_chitietdanhgia.loaiuserdanhgia', '=', $request->loaiuserdanhgia]])
            ->select('tbl_chitietdanhgia.*')
            ->get();
        $bangdiemdanhgia = BangDiemDanhGia_Model::where([['tbl_bangdiemdanhgia.id_sv', '=', $request->id_sv], ['tbl_bangdiemdanhgia.id_thoigiandanhgia', '=', $request->id_thoigiandanhgia]])->first();
        DB::beginTransaction();
        try {
            $tong = 0;
            foreach ($ctdg as $ct) {
                $tong += $ct->diem;
                $duyet = new ChiTietDanhGia_Model;
                $duyet->id_user = $request->id_user;
                $duyet->id_bangdiemdanhgia = $ct->id_bangdiemdanhgia;
                $duyet->id_tieuchi = $ct->id_tieuchi;
                $duyet->diem = $ct->diem;
                $duyet->filename_minhchung = $ct->filename_minhchung;
                $duyet->loaiuserdanhgia = $request->loaiuserduyet;
                $duyet->save();
            }

            switch ($request->loaiuserduyet) {
                case 2: /* Loại user đánh giá ban cán sự */
                    $bangdiemdanhgia->tong_diemdanhgia_bancansu = $tong;
                    $bangdiemdanhgia->trangthai_bancansu = 1;
                    $bangdiemdanhgia->save();
                    break;

                case 3: /* Loại user đánh giá cố vấn học tập */
                    $bangdiemdanhgia->tong_diemdanhgia_cvht = $tong;
                    $bangdiemdanhgia->trangthai_cvht = 1;
                    $bangdiemdanhgia->save();
                    break;

                case 4: /* Loại user đánh giá hội đồng khoa */
                    $bangdiemdanhgia->tong_diemdanhgia_hoidongkhoa = $tong;
                    $bangdiemdanhgia->trangthai_hoidongkhoa = 1;
                    $bangdiemdanhgia->save();
                    break;

                case 5: /* Loại user đánh giá hội đồng trường*/
                    $bangdiemdanhgia->tong_diemdanhgia_hoidongtruong = $tong;
                    $bangdiemdanhgia->trangthai_hoidongtruong = 1;
                    $bangdiemdanhgia->trangthaichung = 1;
                    $bangdiemdanhgia->save();
                    break;
            }

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
            return response()->json(['err_msg' => 'Không thành công'], 401);
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }

        $message = ['message' => 'Đã duyệt nhanh thông tin đánh giá !!!'];

        return response()->json($message, 200);
    }

    public function exportKetQuaDanhGia(Request $request)
    {
        $dscttieuchi = DB::table('tbl_chitiettieuchi')
            ->leftjoin('tbl_tieuchidanhgia', 'tbl_tieuchidanhgia.id_quyetdinhtieuchi', '=', 'tbl_chitiettieuchi.id_quyetdinhtieuchi')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id_tieuchidanhgia', '=', 'tbl_tieuchidanhgia.id')
            ->where('tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky)
            ->select('tbl_chitiettieuchi.id', 'tbl_chitiettieuchi.tentieuchi', 'tbl_chitiettieuchi.diemtoida') //, 'tbl_chitiettieuchi.tentieuchi'
            ->get();
        $ctdg_sv = DB::table('tbl_chitietdanhgia')
            ->where([['tbl_chitietdanhgia.id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['tbl_chitietdanhgia.loaiuserdanhgia', '=', 1]])
            ->select('tbl_chitietdanhgia.id_tieuchi', 'tbl_chitietdanhgia.diem')
            ->get();
        $ctdg_bcs = DB::table('tbl_chitietdanhgia')
            ->where([['tbl_chitietdanhgia.id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['tbl_chitietdanhgia.loaiuserdanhgia', '=', 2]])
            ->select('tbl_chitietdanhgia.id_tieuchi', 'tbl_chitietdanhgia.diem')
            ->get();
        $ctdg_cvht = DB::table('tbl_chitietdanhgia')
            ->where([['tbl_chitietdanhgia.id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['tbl_chitietdanhgia.loaiuserdanhgia', '=', 3]])
            ->select('tbl_chitietdanhgia.id_tieuchi', 'tbl_chitietdanhgia.diem')
            ->get();
        $ctdg_hdk = DB::table('tbl_chitietdanhgia')
            ->where([['tbl_chitietdanhgia.id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['tbl_chitietdanhgia.loaiuserdanhgia', '=', 4]])
            ->select('tbl_chitietdanhgia.id_tieuchi', 'tbl_chitietdanhgia.diem')
            ->get();
        $ctdg_hdt = DB::table('tbl_chitietdanhgia')
            ->where([['tbl_chitietdanhgia.id_bangdiemdanhgia', '=', $request->id_bangdiemdanhgia], ['tbl_chitietdanhgia.loaiuserdanhgia', '=', 5]])
            ->select('tbl_chitietdanhgia.id_tieuchi', 'tbl_chitietdanhgia.diem')
            ->get();

        $SL_Row = count($dscttieuchi);

        foreach ($dscttieuchi as $key => $value) {

            foreach ($ctdg_sv as $sv) {
                if ($value->id == $sv->id_tieuchi) {
                    $value->diem_sv = $sv->diem;
                } else {
                    $value->diem_sv = '';
                }
            }
            foreach ($ctdg_bcs as $bcs) {
                if ($value->id == $bcs->id_tieuchi) {
                    $value->diem_bcs = $bcs->diem;
                } else {
                    $value->diem_bcs = '';
                }
            }
            foreach ($ctdg_cvht as $cvht) {
                if ($value->id == $cvht->id_tieuchi) {
                    $value->diem_cvht = $cvht->diem;
                } else {
                    $value->diem_cvht = '';
                }
            }
            foreach ($ctdg_hdk as $hdk) {
                if ($value->id == $hdk->id_tieuchi) {
                    $value->diem_hdk = $hdk->diem;
                } else {
                    $value->diem_hdk = '';
                }
            }
            foreach ($ctdg_hdt as $hdt) {
                if ($value->id == $hdt->id_tieuchi) {
                    $dscttieuchi[$key]->id = $key;
                    $value->diem_hdt = $hdt->diem;
                } else {
                    $value->diem_hdt = '';
                }
            }
        }

        $col_STT = 'A3:A' . $SL_Row;
        $col_DiemToiDa = 'C3:C' . $SL_Row;
        $border = 'A1:H' . ($SL_Row + 2);

        Excel::create('danhsachtieuchi', function ($excel) use ($dscttieuchi, $col_STT, $col_DiemToiDa, $border) {
            $excel->setTitle('Bảng điểm đánh giá');
            $excel->sheet('Sheet 1', function ($sheet) use ($dscttieuchi, $col_STT, $col_DiemToiDa, $border) {
                $sheet->setFontFamily('Times New Roman');

                $sheet->setBorder($border, 'medium');

                $sheet->fromArray(json_decode($dscttieuchi, true), null, 'A2', false);

                $sheet->row(1, ['STT', 'Tên Tiêu Chí', 'Điểm Tối Đa', 'Điểm Đánh Giá']);
                $sheet->mergeCells('D1:H1');

                $sheet->mergeCells('D1:Q1');
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('UBND TỈNH AN GIANG');
                    $cell->setAlignment('center');
                    $cell->setBackground('#ffffff');
                });

                $sheet->mergeCells('D2:Q2');
                $sheet->cell('D2', function ($cell) {
                    $cell->setValue('TRƯỜNG ĐẠI HỌC AN GIANG');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('AL1:AY1');
                $sheet->cell('AL1', function ($cell) {
                    $cell->setValue('CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM');
                    $cell->setFontWeight('bold');
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('AL2:AY2');
                $sheet->cell('AL2', function ($cell) {
                    $cell->setValue('Độc lập - Tự do - Hạnh phúc');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('R4:AK4');
                $sheet->cell('R4', function ($cell) {
                    $cell->setValue('BẢNG TỔNG HỢP KẾT QUẢ RÈN LUYỆN SINH VIÊN');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('W5:AD5');
                $sheet->cell('W5', function ($cell) use ($nh_hk) {
                    $cell->setValue('Năm học: ' . $nh_hk->manamhoc);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('P7:R7');
                $sheet->cell('P7', function ($cell) use ($nh_hk) {
                    $cell->setValue($nh_hk->mahocky);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->setMergeColumn(array(
                    'columns' => array('A', 'B', 'C'),
                    'rows' => array(
                        array(1, 2),
                    ),
                ));

                $sheet->row(2, ['', '', '', 'Sinh Viên', 'Ban Cán Sự', 'Cố Vấn Học Tập', 'HD Khoa', 'HD Trường']);

                $sheet->cell('A1:H1', function ($cell) {
                    $cell->setFontWeight('bold');
                    $cell->setAlignment('center');
                    $cell->setBackground('#d8d8d8');
                });

                $sheet->cell('D2:H2', function ($cell) {
                    $cell->setFontWeight('bold');
                    $cell->setAlignment('center');
                    $cell->setBackground('#d8d8d8');
                });

                $sheet->cell('D1', function ($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                    $cell->setBackground('#d8d8d8');
                });

                $sheet->cell($col_STT, function ($cell) {
                    $cell->setAlignment('center');
                });

                $sheet->cell($col_DiemToiDa, function ($cell) {
                    $cell->setAlignment('center');
                });

            });
        })->export($request->ext);
    }

    /* Hàm Đệ Quy Hiển Thị Đa Cấp */
    public function deQuyDaCap($danhsach, $parent, $level, &$newDanhSach)
    {
        if (count($danhsach) > 0) {
            foreach ($danhsach as $key => $value) {
                if ($value->id_tieuchicha == $parent) {
                    $value->level = $level;
                    $newDanhSach[] = $value;
                    unset($danhsach[$key]);
                    $newParent = $value->id;
                    $this->deQuyDaCap($danhsach, $newParent, $level + 1, $newDanhSach);
                }
            }
        }
        return $newDanhSach;
    }

    public function exportBangDiemDanhGia(Request $request)
    {
        $dssinhvien = DB::table('tbl_sinhvien')
            ->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->select('tbl_sinhvien.id', 'tbl_sinhvien.mssv', 'tbl_sinhvien.hoten')
            ->where('tbl_sinhvien.id_lop', '=', $request->id_lop)
            ->get();

        $dscttieuchi = DB::table('tbl_chitiettieuchi')
            ->leftjoin('tbl_tieuchidanhgia', 'tbl_tieuchidanhgia.id_quyetdinhtieuchi', '=', 'tbl_chitiettieuchi.id_quyetdinhtieuchi')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id_tieuchidanhgia', '=', 'tbl_tieuchidanhgia.id')
            ->where([['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky]])
            ->select('tbl_chitiettieuchi.id', 'tbl_chitiettieuchi.chimuc_tieuchi', 'tbl_chitiettieuchi.id_tieuchicha')
            ->get();

        $dscttieuchi = $this->deQuyDaCap($dscttieuchi, null, 1, $newDanhSach);
        $dschimuc = $dscttieuchi;

        $dstongdiem = DB::table('tbl_bangdiemdanhgia')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
            ->where([['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky]])
            ->select('tbl_bangdiemdanhgia.id_sv', 'tbl_bangdiemdanhgia.tong_diemdanhgia_hoidongtruong')
            ->get();

        foreach ($dssinhvien as $sv) {
            $new_arr = [];

            foreach ($dscttieuchi as $key => $value) {;
                $new_arr[$value->id] = (object) array(
                    'id' => $value->id,
                    'chimuc' => $value->chimuc_tieuchi,
                    'diem' => '',
                );
            }

            $ctdg_sv = DB::table('tbl_chitietdanhgia')
                ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id', '=', 'tbl_chitietdanhgia.id_bangdiemdanhgia')
                ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
                ->where([['tbl_chitietdanhgia.loaiuserdanhgia', '=', 5], ['tbl_bangdiemdanhgia.id_sv', '=', $sv->id]])
                ->select('tbl_chitietdanhgia.id_tieuchi', 'tbl_chitietdanhgia.diem', 'tbl_bangdiemdanhgia.id_sv', 'tbl_bangdiemdanhgia.tong_diemdanhgia_hoidongtruong')
                ->get();

            foreach ($new_arr as $key => $value) {
                foreach ($ctdg_sv as $dg) {
                    if ($key === $dg->id_tieuchi) {
                        $new_arr[$key]->diem = $dg->diem;
                    }
                }
            }
            $mssv_tmp = $sv->mssv;
            $tmp[$mssv_tmp] = $new_arr;
        }

        $count_XS = 0;
        $count_T = 0;
        $count_K = 0;
        $count_TB = 0;
        $count_Y = 0;
        $count_Kem = 0;
        foreach ($dssinhvien as $sv) {
            foreach ($tmp as $key => $value) {
                if ($sv->mssv == $key) {
                    foreach ((object) $tmp[$key] as $cc) {
                        $chimuc = $cc->id . '-' . $cc->chimuc;
                        $diem = $cc->diem;
                        $sv->$chimuc = $diem;
                    }
                }
            }

            foreach ($dstongdiem as $td) {
                if ($sv->id == $td->id_sv) {
                    $sv->tongdiem = $td->tong_diemdanhgia_hoidongtruong;
                    if ($td->tong_diemdanhgia_hoidongtruong >= 90 && $td->tong_diemdanhgia_hoidongtruong <= 100) {
                        $sv->xeploai = 'Xuất Sắc';
                        $count_XS += 1;
                    } else if ($td->tong_diemdanhgia_hoidongtruong >= 80 && $td->tong_diemdanhgia_hoidongtruong < 90) {
                        $sv->xeploai = 'Tốt';
                        $count_T += 1;
                    } else if ($td->tong_diemdanhgia_hoidongtruong >= 65 && $td->tong_diemdanhgia_hoidongtruong < 80) {
                        $sv->xeploai = 'Khá';
                        $count_K += 1;
                    } else if ($td->tong_diemdanhgia_hoidongtruong >= 50 && $td->tong_diemdanhgia_hoidongtruong < 65) {
                        $sv->xeploai = 'Trung Bình';
                        $count_TB += 1;
                    } else if ($td->tong_diemdanhgia_hoidongtruong >= 35 && $td->tong_diemdanhgia_hoidongtruong < 50) {
                        $sv->xeploai = 'Yếu';
                        $count_Y += 1;
                    } else if ($td->tong_diemdanhgia_hoidongtruong < 35) {
                        $sv->xeploai = 'Kém';
                        $count_Kem += 1;
                    }
                }
            }
        }

        $sl_XL = (object) array(
            'XS' => $count_XS,
            'T' => $count_T,
            'K' => $count_K,
            'TB' => $count_TB,
            'Y' => $count_Y,
            'Kem' => $count_Kem,
        );

        $arr_chimuc = ['ID', 'MSSV', 'Họ Tên'];
        foreach ($dschimuc as $value) {
            array_push($arr_chimuc, $value->chimuc_tieuchi);
        }
        array_push($arr_chimuc, 'Tổng Điểm');
        array_push($arr_chimuc, 'Xếp Loại');
        $stt = 0;
        foreach ($dssinhvien as &$sv) {
            $sv->id = ++$stt;
        }

        $SL_Row = count($dssinhvien);
        $border = 'A9:BO' . ($SL_Row + 9);

        $nh_hk = DB::table('tbl_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->where('tbl_hocky.id', '=', $request->id_hocky)
            ->select('tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->first();

        $lop = DB::table('tbl_lop')
            ->leftjoin('tbl_nganh', 'tbl_nganh.id', '=', 'tbl_lop.id_nganh')
            ->leftjoin('tbl_bomon_donvi', 'tbl_bomon_donvi.id', '=', 'tbl_nganh.id_bomon_donvi')
            ->leftjoin('tbl_khoa', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
            ->where('tbl_lop.id', '=', $request->id_lop)
            ->select('tbl_lop.tenlop', 'tbl_khoa.tenkhoa')
            ->first();

        Excel::create('bangdiemdanhgia', function ($excel) use ($dssinhvien, $arr_chimuc, $border, $lop, $nh_hk, $SL_Row, $sl_XL) {
            $excel->setTitle('Bảng tổng hợp điểm đánh giá');
            $excel->sheet('Sheet 1', function ($sheet) use ($dssinhvien, $arr_chimuc, $border, $lop, $nh_hk, $SL_Row, $sl_XL) {

                $sheet->setFontFamily('Times New Roman');
                $sheet->setBorder($border, 'thin');
                $sheet->fromArray(json_decode($dssinhvien, true), null, 'A9', false);
                $sheet->row(9, $arr_chimuc);

                $sheet->mergeCells('D1:Q1');
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('UBND TỈNH AN GIANG');
                    $cell->setAlignment('center');
                    $cell->setBackground('#ffffff');
                });

                $sheet->mergeCells('D2:Q2');
                $sheet->cell('D2', function ($cell) {
                    $cell->setValue('TRƯỜNG ĐẠI HỌC AN GIANG');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('AL1:AY1');
                $sheet->cell('AL1', function ($cell) {
                    $cell->setValue('CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM');
                    $cell->setFontWeight('bold');
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('AL2:AY2');
                $sheet->cell('AL2', function ($cell) {
                    $cell->setValue('Độc lập - Tự do - Hạnh phúc');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('R4:AK4');
                $sheet->cell('R4', function ($cell) {
                    $cell->setValue('BẢNG TỔNG HỢP KẾT QUẢ RÈN LUYỆN SINH VIÊN');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('W5:AD5');
                $sheet->cell('W5', function ($cell) use ($nh_hk) {
                    $cell->setValue('Năm học: ' . $nh_hk->manamhoc);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('P7:R7');
                $sheet->cell('P7', function ($cell) use ($nh_hk) {
                    $cell->setValue($nh_hk->mahocky);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('U7:X7');
                $sheet->cell('U7', function ($cell) use ($SL_Row) {
                    $cell->setValue('Số lượng: ' . $SL_Row);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('Z7:AD7');
                $sheet->cell('Z7', function ($cell) use ($lop) {
                    $cell->setValue('Lớp: ' . $lop->tenlop);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('AG7:AP7');
                $sheet->cell('AG7', function ($cell) use ($lop) {
                    $cell->setValue('Khoa: ' . $lop->tenkhoa);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->cell('D1:Q1', function ($cell) {
                    $cell->setFontWeight('bold');
                    $cell->setAlignment('center');
                    $cell->setBackground('#d8d8d8');
                });

                $sheet->cell('A9:BO9', function ($cell) {
                    $cell->setFontWeight('bold');
                    $cell->setAlignment('center');
                    $cell->setBackground('#d8d8d8');
                });

                $sheet->mergeCells('D' . ($SL_Row + 11) . ':M' . ($SL_Row + 11));
                $sheet->cell('D' . ($SL_Row + 11), function ($cell) use ($SL_Row) {
                    $cell->setValue('Tổng hợp kết quả rèn luyện:');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                //Bảng tổng kết
                // Xuất sắc
                $sheet->mergeCells('D' . ($SL_Row + 13) . ':I' . ($SL_Row + 13));
                $sheet->cell('D' . ($SL_Row + 13), function ($cell) {
                    $cell->setValue('* Xuất sắc:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('J' . ($SL_Row + 13) . ':K' . ($SL_Row + 13));
                $sheet->cell('J' . ($SL_Row + 13), function ($cell) use ($sl_XL) {
                    $cell->setValue($sl_XL->XS);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('L' . ($SL_Row + 13) . ':N' . ($SL_Row + 13));
                $sheet->cell('L' . ($SL_Row + 13), function ($cell) {
                    $cell->setValue('Tỉ lệ:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('O' . ($SL_Row + 13) . ':Q' . ($SL_Row + 13));
                $sheet->cell('O' . ($SL_Row + 13), function ($cell) use ($sl_XL, $SL_Row) {
                    $tl = ($sl_XL->XS / $SL_Row) * 100;
                    $cell->setValue(number_format($tl, 0, ',', '.') . '%');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                // Trung bình
                $sheet->mergeCells('R' . ($SL_Row + 13) . ':W' . ($SL_Row + 13));
                $sheet->cell('R' . ($SL_Row + 13), function ($cell) {
                    $cell->setValue('* Trung bình:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('X' . ($SL_Row + 13) . ':Y' . ($SL_Row + 13));
                $sheet->cell('X' . ($SL_Row + 13), function ($cell) use ($sl_XL) {
                    $cell->setValue($sl_XL->TB);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('Z' . ($SL_Row + 13) . ':AB' . ($SL_Row + 13));
                $sheet->cell('Z' . ($SL_Row + 13), function ($cell) {
                    $cell->setValue('Tỉ lệ:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('AC' . ($SL_Row + 13) . ':AE' . ($SL_Row + 13));
                $sheet->cell('AC' . ($SL_Row + 13), function ($cell) use ($sl_XL, $SL_Row) {
                    $tl = ($sl_XL->TB / $SL_Row) * 100;
                    $cell->setValue(number_format($tl, 0, ',', '.') . '%');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                // Tốt
                $sheet->mergeCells('D' . ($SL_Row + 14) . ':I' . ($SL_Row + 14));
                $sheet->cell('D' . ($SL_Row + 14), function ($cell) {
                    $cell->setValue('* Tốt:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('J' . ($SL_Row + 14) . ':K' . ($SL_Row + 14));
                $sheet->cell('J' . ($SL_Row + 14), function ($cell) use ($sl_XL) {
                    $cell->setValue($sl_XL->T);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('L' . ($SL_Row + 14) . ':N' . ($SL_Row + 14));
                $sheet->cell('L' . ($SL_Row + 14), function ($cell) {
                    $cell->setValue('Tỉ lệ:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('O' . ($SL_Row + 14) . ':Q' . ($SL_Row + 14));
                $sheet->cell('O' . ($SL_Row + 14), function ($cell) use ($sl_XL, $SL_Row) {
                    $tl = ($sl_XL->T / $SL_Row) * 100;
                    $cell->setValue(number_format($tl, 0, ',', '.') . '%');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                // Yếu
                $sheet->mergeCells('R' . ($SL_Row + 14) . ':W' . ($SL_Row + 14));
                $sheet->cell('R' . ($SL_Row + 14), function ($cell) {
                    $cell->setValue('* Yếu:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('X' . ($SL_Row + 14) . ':Y' . ($SL_Row + 14));
                $sheet->cell('X' . ($SL_Row + 14), function ($cell) use ($sl_XL) {
                    $cell->setValue($sl_XL->Y);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('Z' . ($SL_Row + 14) . ':AB' . ($SL_Row + 14));
                $sheet->cell('Z' . ($SL_Row + 14), function ($cell) {
                    $cell->setValue('Tỉ lệ:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('AC' . ($SL_Row + 14) . ':AE' . ($SL_Row + 14));
                $sheet->cell('AC' . ($SL_Row + 14), function ($cell) use ($sl_XL, $SL_Row) {
                    $tl = ($sl_XL->Y / $SL_Row) * 100;
                    $cell->setValue(number_format($tl, 0, ',', '.') . '%');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                // Khá
                $sheet->mergeCells('D' . ($SL_Row + 15) . ':I' . ($SL_Row + 15));
                $sheet->cell('D' . ($SL_Row + 15), function ($cell) {
                    $cell->setValue('* Khá:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('J' . ($SL_Row + 15) . ':K' . ($SL_Row + 15));
                $sheet->cell('J' . ($SL_Row + 15), function ($cell) use ($sl_XL) {
                    $cell->setValue($sl_XL->K);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('L' . ($SL_Row + 15) . ':N' . ($SL_Row + 15));
                $sheet->cell('L' . ($SL_Row + 15), function ($cell) {
                    $cell->setValue('Tỉ lệ:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('O' . ($SL_Row + 15) . ':Q' . ($SL_Row + 15));
                $sheet->cell('O' . ($SL_Row + 15), function ($cell) use ($sl_XL, $SL_Row) {
                    $tl = ($sl_XL->K / $SL_Row) * 100;
                    $cell->setValue(number_format($tl, 0, ',', '.') . '%');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('R' . ($SL_Row + 15) . ':W' . ($SL_Row + 15));
                $sheet->cell('R' . ($SL_Row + 15), function ($cell) {
                    $cell->setValue('* Kém:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('X' . ($SL_Row + 15) . ':Y' . ($SL_Row + 15));
                $sheet->cell('X' . ($SL_Row + 15), function ($cell) use ($sl_XL) {
                    $cell->setValue($sl_XL->Kem);
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('Z' . ($SL_Row + 15) . ':AB' . ($SL_Row + 15));
                $sheet->cell('Z' . ($SL_Row + 15), function ($cell) {
                    $cell->setValue('Tỉ lệ:');
                    $cell->setAlignment('left');
                });
                $sheet->mergeCells('AC' . ($SL_Row + 15) . ':AE' . ($SL_Row + 15));
                $sheet->cell('AC' . ($SL_Row + 15), function ($cell) use ($sl_XL, $SL_Row) {
                    $tl = ($sl_XL->Kem / $SL_Row) * 100;
                    $cell->setValue(number_format($tl, 0, ',', '.') . '%');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->setBorder('D' . ($SL_Row + 13) . ':AE' . ($SL_Row + 15), 'thin');

                $sheet->mergeCells('AZ' . ($SL_Row + 17) . ':BL' . ($SL_Row + 17));
                $sheet->cell('AZ' . ($SL_Row + 17), function ($cell) use ($SL_Row) {
                    $cell->setValue('An Giang, ngày....tháng.....năm 20...');
                    $cell->setAlignment('center');
                    $cell->setFont(array(
                        'italic' => true,
                    ));
                });

                $sheet->mergeCells('AZ' . ($SL_Row + 18) . ':BL' . ($SL_Row + 18));
                $sheet->cell('AZ' . ($SL_Row + 18), function ($cell) use ($SL_Row) {
                    $cell->setValue('Ban Đại Diện Lớp');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('D' . ($SL_Row + 18) . ':J' . ($SL_Row + 18));
                $sheet->cell('D' . ($SL_Row + 18), function ($cell) use ($SL_Row) {
                    $cell->setValue('Hội Đồng Khoa');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('AC' . ($SL_Row + 18) . ':AO' . ($SL_Row + 18));
                $sheet->cell('AC' . ($SL_Row + 18), function ($cell) use ($SL_Row) {
                    $cell->setValue('Chủ Nhiệm Lớp');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
            });
        })->export('xlsx');
    }

    public function postThongKeDiemDanhGiaTheoHocKy(Request $request)
    {
        switch ($request->loaichucvudanhgia) {
            case 1:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->where('tbl_sinhvien.id_lop', '=', $request->id_lop)
                    ->select(DB::raw('tbl_sinhvien.mssv as label'), DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_sv as value'))
                    ->get();
                break;
            case 2:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->where('tbl_sinhvien.id_lop', '=', $request->id_lop)
                    ->select(DB::raw('tbl_sinhvien.mssv as label'), DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_bancansu as value'))
                    ->get();
                break;
            case 3:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->where('tbl_sinhvien.id_lop', '=', $request->id_lop)
                    ->select(DB::raw('tbl_sinhvien.mssv as label'), DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_cvht as value'))
                    ->get();
                break;
            case 4:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->where('tbl_sinhvien.id_lop', '=', $request->id_lop)
                    ->select(DB::raw('tbl_sinhvien.mssv as label'), DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_hoidongkhoa as value'))
                    ->get();
                break;
            case 5:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->where('tbl_sinhvien.id_lop', '=', $request->id_lop)
                    ->select(DB::raw('tbl_sinhvien.mssv as label'), DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_hoidongtruong as value'))
                    ->get();
                break;
            default:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->where('tbl_sinhvien.id_lop', '=', $request->id_lop)
                    ->select(DB::raw('tbl_sinhvien.mssv as label'), DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_sv as value'))
                    ->get();
                break;
        }

        $response = ['data' => $data];

        return response()->json($response, 200);
    }

    public function postThongKeDiemDanhGiaTheoLop_HocKy(Request $request)
    {
        $dssv = DB::table('tbl_sinhvien')
            ->where([['tbl_sinhvien.id_lop', '=', $request->id_lop]])
            ->select('tbl_sinhvien.id', DB::raw('tbl_sinhvien.mssv as label'))
            ->get();

            // dd($dssv);
        switch ($request->loaichucvudanhgia) {
            case 1:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
                    ->where([['tbl_sinhvien.id_lop', '=', $request->id_lop], ['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky]])
                    ->select('tbl_bangdiemdanhgia.id_sv', DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_sv as value'))
                    ->get();
                foreach ($dssv as $sv) {
                    foreach ($data as $dt) {
                        if ($sv->id == $dt->id_sv) {
                            $sv->value = $dt->value;
                            break;
                        }
                    }
                }
                break;
            case 2:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
                    ->where([['tbl_sinhvien.id_lop', '=', $request->id_lop], ['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky]])
                    ->select('tbl_bangdiemdanhgia.id_sv', DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_bancansu as value'))
                    ->get();
                foreach ($dssv as $sv) {
                    foreach ($data as $dt) {
                        if ($sv->id == $dt->id_sv) {
                            $sv->value = $dt->value;
                        }
                    }
                }
                break;
            case 3:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
                    ->where([['tbl_sinhvien.id_lop', '=', $request->id_lop], ['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky]])
                    ->select('tbl_bangdiemdanhgia.id_sv', DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_cvht as value'))
                    ->get();
                foreach ($dssv as $sv) {
                    foreach ($data as $dt) {
                        if ($sv->id == $dt->id_sv) {
                            $sv->value = $dt->value;
                        }
                    }
                }
                break;
            case 4:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
                    ->where([['tbl_sinhvien.id_lop', '=', $request->id_lop], ['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky]])
                    ->select('tbl_bangdiemdanhgia.id_sv', DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_hoidongkhoa as value'))
                    ->get();
                foreach ($dssv as $sv) {
                    foreach ($data as $dt) {
                        if ($sv->id == $dt->id_sv) {
                            $sv->value = $dt->value;
                        }
                    }
                }
                break;
            case 5:
                $data = DB::table('tbl_sinhvien')
                    ->leftjoin('tbl_bangdiemdanhgia', 'tbl_bangdiemdanhgia.id_sv', '=', 'tbl_sinhvien.id')
                    ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id', '=', 'tbl_bangdiemdanhgia.id_thoigiandanhgia')
                    ->where([['tbl_sinhvien.id_lop', '=', $request->id_lop], ['tbl_thoigiandanhgia.id_hocky', '=', $request->id_hocky]])
                    ->select('tbl_bangdiemdanhgia.id_sv', DB::raw('tbl_bangdiemdanhgia.tong_diemdanhgia_hoidongtruong as value'))
                    ->get();
                foreach ($dssv as $sv) {
                    foreach ($data as $dt) {
                        if ($sv->id == $dt->id_sv) {
                            $sv->value = $dt->value;
                        }
                    }
                }
                break;
            default:
                break;
        }


        $response = ['data' => $dssv];

        return response()->json($response, 200);
    }

    public function getFileMinhChung(Request $request)
    {

        $sinhvien = DB::table('tbl_sinhvien')
            ->leftjoin('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->leftjoin('tbl_nganh', 'tbl_lop.id_nganh', '=', 'tbl_nganh.id')
            ->leftjoin('tbl_bomon_donvi', 'tbl_nganh.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->leftjoin('tbl_khoa', 'tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
            ->select('tbl_sinhvien.mssv', 'tbl_lop.tenlop', 'tbl_nganh.tennganh', 'tbl_khoa.tenkhoa')
            ->where('tbl_sinhvien.id', '=', $request->id_sv)
            ->first();

        $ct = (object) $request->ctdg;

        return Storage::download('MinhChung/' . $sinhvien->tenkhoa . '/' . $sinhvien->tennganh . '/' . $sinhvien->tenlop . '/' . $sinhvien->mssv . '/' . $ct->filename_minhchung);
    }
}
