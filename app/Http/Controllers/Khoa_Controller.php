<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Khoa_Model;
use JWTAuth;
use Validator;
use Illuminate\Database\QueryException;
class Khoa_Controller extends Controller
{
    //

    public function getDanhSachKhoa(){
        try{
            $getDSKhoa = Khoa_Model::all();
            $response = ['danhsach' => $getDSKhoa];
            return response()->json($response, 200);
        }catch(Exception $e){
                    
            return response()->json($e);
        }  
    }

    public function getKhoaById($id){
        try{
            $getDSKhoa = Khoa_Model::find($id);
            $response = ['khoa' => $getDSKhoa];
            return response()->json($response, 200);
        }catch(Exception $e){
                    
            return response()->json($e);
        }  
    }

    public function postThemKhoa(Request $request) {
        $message = [
            'makhoa.required' => 'Mã khoa không được bỏ trống',
            'tenkhoa.required' => 'Tên khoa không được bỏ trống',
        ];

        $validator = Validator::make($request->all(),
        [
            'makhoa' => 'required',
            'tenkhoa' => 'required',
            

        ], $message);

        if($validator->fails()){
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors()
            ]);
        }else{
            try{
                $khoa           = new Khoa_Model;
                $khoa->makhoa   = $request->makhoa;
                $khoa->tenkhoa  = $request->tenkhoa;
                $khoa->save();

                $message = ['message' => 'Đã thêm khoa "' . $khoa->tenkhoa . '" với mã khoa "' . $khoa->makhoa . '"!!!'];
                return response()->json($message, 200);

            }catch(QueryException $e){   
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }  
    }

    public function putSuaKhoa(Request $request, $id) {
        $khoa   = Khoa_Model::find($id);
        $message = [
            'makhoa.required' => 'Mã khoa không được bỏ trống',
            'tenkhoa.required' => 'Tên khoa không được bỏ trống'
        ];

        $validator = Validator::make($request->all(),
        [
            'makhoa' => 'required',
            'tenkhoa' => 'required',
        ], $message);

        if($validator->fails()){
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors()
            ]);
        }else{
            try{
                $khoa->makhoa   = $request->get('makhoa');
                $khoa->tenkhoa  = $request->get('tenkhoa');
                $khoa->save();

                $message = ['message' => 'Đã cập nhật thông tin khoa "' . $khoa->tenkhoa . '"!!!'];

                return response()->json($message, 200);
            }catch(QueryException $e){   
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function deleteXoaKhoa($id) {
        $khoa   = Khoa_Model::find($id);
        $old_name = $khoa->tenkhoa;
        try{
            $khoa->delete();

            $message = ['message' => 'Đã xóa thông tin khoa "' . $old_name . '"!!!'];

            return response()->json($message, 200);
        }catch(QueryException $e){   
            $maLoi = $e->errorInfo[1]; // 1451: Lỗi có dữ liệu bên trong
            return response()->json(['maloi' => $maLoi]);

        }
    }
}
