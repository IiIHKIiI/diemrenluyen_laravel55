<?php

namespace App\Http\Controllers;

use App\CanBo_Model;
use App\CoVanHocTap_Model;
use App\PhanQuyen_Model;
use App\QuanLy_Model;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Excel;
class CanBo_Controller extends Controller
{
    //
    public function importDanhSachCanBo(Request $request)
    {
        set_time_limit(0);
        if ($request->hasFile('file_danhsach')) {
            $path = $request->file('file_danhsach')->getRealPath();

            $data = Excel::load($path, function ($reader) {
                $reader->setDateFormat('Y-m-d');
            }, 'UTF-8')->get();

            if (!empty($data) && $data->count()) {
                DB::beginTransaction();
                try {
                    foreach ($data as $key => $value) {
                        if (!empty($value)) {
                            $canbo = new CanBo_Model;
                            $canbo->hotencanbo = (string) $value->ho_va_ten;
                            $canbo->macanbo = (string) $value->ma_can_bo;
                            if ($value->gioi_tinh == "Nam") {
                                $canbo->gioitinh = 0;
                            } else {
                                $canbo->gioitinh = 1;
                            }
                            $canbo->ngaysinh = $value->ngay_sinh;
                            $canbo->cmnd = (int) $value->cmnd;
                            if ($value->sdt != null) {
                                $canbo->sdt = $value->sdt;
                            } else {
                                $canbo->sdt = '0';
                            }
                            $canbo->email = (string) $value->email;
                            if ($value->dia_chi != null) {
                                $canbo->diachi = (string) $value->dia_chi;
                            } else {
                                $canbo->diachi = 'Unknown';
                            }
                            $canbo->id_bomon_donvi = $request->id_bomon_donvi;
                            $canbo->save();

                            $user = new User;
                            $user->username = $canbo->macanbo;
                            // $user->password     = bcrypt($request->cmnd); // thay đổi
                            $user->password = bcrypt('123');
                            $user->trangthai = 1;
                            $user->ms_sv_canbo = $canbo->macanbo;
                            $user->loaiuser = 2;
                            $user->isfirst = 1;
                            $user->save();

                            switch ($value->ma_chuc_vu) {
                                case 'TK':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 1;
                                    $ql->save();
                                    break;
                                case 'PK':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 2;
                                    $ql->save();
                                    break;
                                case 'TBM':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 3;
                                    $ql->save();
                                    break;

                                case 'PBM':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 4;
                                    $ql->save();
                                    break;
                                case 'VT':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 5;
                                    $ql->save();
                                    break;
                                case 'KTV':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 6;
                                    $ql->save();
                                    break;
                                case 'CV':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 7;
                                    $ql->save();
                                    break;
                                case 'GV':
                                    $ql = new QuanLy_Model;
                                    $ql->id_canbo = $canbo->id;
                                    $ql->id_chucvu_quanly = 8;
                                    $ql->save();
                                    break;
                                default:
                                    break;
                            }

                        }
                    }

                    DB::commit();

                } catch (\Throwable $e) {
                    DB::rollback();
                    throw $e;

                    return response()->json(['err_msg' => 'Không thành công'], 401);

                } catch (QueryException $e) {
                    $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                    return response()->json(['maloi' => $maLoi]);
                }
            }
        }

        $message = ['message' => 'Đã thêm danh sách sinh viên!!!'];
        return response()->json($message, 200);
    }

    public function postDanhSachCanBo_BoMon(Request $request)
    {
        $danhsachcanbo = DB::table('tbl_canbo')
            ->leftjoin('tbl_bomon_donvi', 'tbl_canbo.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->leftjoin('tbl_khoa', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
            ->select('tbl_canbo.id', 'tbl_canbo.macanbo', 'tbl_canbo.hotencanbo', 'tbl_canbo.ngaysinh', 'tbl_canbo.cmnd', 'tbl_canbo.email', 'tbl_bomon_donvi.tenbomon')
            ->where('tbl_bomon_donvi.id', 'like', $request->id_bomon)
            ->get();
        $response = ['danhsach' => $danhsachcanbo];
        return response()->json($response, 200);
    }

    public function getThongTinCanBoById($id)
    {
        $thontincb = DB::table('tbl_canbo')
            ->leftjoin('tbl_bomon_donvi', 'tbl_canbo.id_bomon_donvi', '=', 'tbl_bomon_donvi.id')
            ->leftjoin('tbl_khoa', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
            ->select('tbl_canbo.*', 'tbl_bomon_donvi.tenbomon', 'tbl_bomon_donvi.tructhuoc', 'tbl_khoa.tenkhoa')
            ->where('tbl_canbo.id', '=', $id)
            ->first();

        $thongtinchucvu = DB::table('tbl_canbo')
            ->leftjoin('tbl_quanly', 'tbl_quanly.id_canbo', '=', 'tbl_canbo.id')
            ->leftjoin('tbl_chucvu_quanly', 'tbl_chucvu_quanly.id', '=', 'tbl_quanly.id_chucvu_quanly')
            ->select('tbl_quanly.id_chucvu_quanly', 'tbl_chucvu_quanly.tenchucvu_quanly')
            ->where('tbl_canbo.id', '=', $id)
            ->get();

        $response = ['thontincb' => $thontincb, 'thongtinchucvu' => $thongtinchucvu];
        return response()->json($response, 200);
    }

    public function postDanhSachCanBo_ChuaCoVan(Request $request)
    {
        $covan = CoVanHocTap_Model::select('id_canbo')->get();
        $danhsachcb = DB::table('tbl_canbo')
            ->where(function ($query) use ($covan, $request) {
                $query->where('tbl_canbo.id_bomon_donvi', 'like', $request->id_bomon_donvi);
            })
            ->select('tbl_canbo.id', 'tbl_canbo.hotencanbo', 'tbl_canbo.macanbo')
            ->get();

        $lop = CoVanHocTap_Model::select('id_lop')->get();
        $danhsachlop = DB::table('tbl_lop')
            ->where(function ($query) use ($lop) {
                $query->where('tbl_lop.id_nganh', 'like', '%')
                    ->whereNotIn('tbl_lop.id', $lop);
            })
            ->select('tbl_lop.id', 'tbl_lop.tenlop')
            ->get();

        $response = ['danhsachcb' => $danhsachcb, 'danhsachlop' => $danhsachlop];
        return response()->json($response, 200);
    }

    public function postThemCanBo(Request $request)
    {
        $message = [
            'macanbo.required' => 'Mã số cán bộ không được bỏ trống',
            'hotencanbo.required' => 'Họ tên không được bỏ trống',
            'cmnd.required' => 'CMND không được bỏ trống',
            'sdt.required' => 'Số điện thoại cá nhân không được bỏ trống',
            'diachi.required' => 'Địa chỉ không được bỏ trống',
            'email.required' => 'Email không được bỏ trống',
            'id_bomon_donvi.required' => 'Chưa chọn bộ môn trực thuộc cho cán bộ',
            'id_chucvu_quanly' => 'Chưa chọn chức vụ cho cán bộ',
        ];

        $validator = Validator::make($request->all(),
            [
                'macanbo' => 'required',
                'hotencanbo' => 'required',
                'gioitinh' => 'required',
                'ngaysinh' => 'required',
                'cmnd' => 'required',
                'sdt' => 'required',
                'diachi' => 'required',
                'email' => 'required',
                'id_bomon_donvi' => 'required',
                'id_chucvu_quanly' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();
            try {

                $canbo = new CanBo_Model;
                $canbo->macanbo = $request->macanbo;
                $canbo->hotencanbo = $request->hotencanbo;
                $canbo->gioitinh = $request->gioitinh;
                $canbo->ngaysinh = $request->ngaysinh;
                $canbo->cmnd = $request->cmnd;
                $canbo->sdt = $request->sdt;
                $canbo->diachi = $request->diachi;
                $canbo->email = $request->email;
                $canbo->id_bomon_donvi = $request->id_bomon_donvi;
                $canbo->save();

                $user = new User;
                $user->username = $request->macanbo;
                // $user->password     = bcrypt($request->cmnd); // thay đổi
                $user->password = bcrypt('123');
                $user->trangthai = 1;
                $user->ms_sv_canbo = $request->macanbo;
                $user->loaiuser = 2;
                $user->isfirst = 1;
                $user->save();

                if ($request->id_chucvu_quanly != null || $request->id_chucvu_quanly != '') {
                    foreach ($request->id_chucvu_quanly as $cv) {
                        $ql = new QuanLy_Model;
                        $ql->id_canbo = $canbo->id;
                        $ql->id_chucvu_quanly = $cv;
                        $ql->save();
                    }
                } else {
                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 7;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();
                }

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;

                return response()->json(['err_msg' => 'Không thành công'], 401);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã thêm cán bộ "' . $canbo->hotencanbo . '"!!!'];
            return response()->json($message, 200);
        }
    }

    public function putSuaCanBo(Request $request, $id)
    {
        $canbo = CanBo_Model::find($id);
        $user = User::where('ms_sv_canbo', '=', $canbo->macanbo)->first();
        $quanly = QuanLy_Model::where('id_canbo', '=', $canbo->id)->get();
        $message = [
            'macanbo.required' => 'Mã số cán bộ không được bỏ trống',
            'hotencanbo.required' => 'Họ tên không được bỏ trống',
            'cmnd.required' => 'CMND không được bỏ trống',
            'sdt.required' => 'Số điện thoại cá nhân không được bỏ trống',
            'diachi.required' => 'Địa chỉ không được bỏ trống',
            'email.required' => 'Email không được bỏ trống',
            'id_bomon_donvi.required' => 'Chưa chọn bộ môn trực thuộc cho cán bộ',
        ];

        $validator = Validator::make($request->all(),
            [
                'macanbo' => 'required',
                'hotencanbo' => 'required',
                'gioitinh' => 'required',
                'ngaysinh' => 'required',
                'cmnd' => 'required',
                'sdt' => 'required',
                'diachi' => 'required',
                'email' => 'required',
                'id_bomon_donvi' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();
            try {

                $canbo->macanbo = $request->macanbo;
                $canbo->hotencanbo = $request->hotencanbo;
                $canbo->gioitinh = $request->gioitinh;
                $canbo->ngaysinh = $request->ngaysinh;
                $canbo->cmnd = $request->cmnd;
                $canbo->sdt = $request->sdt;
                $canbo->diachi = $request->diachi;
                $canbo->email = $request->email;
                $canbo->id_bomon_donvi = $request->id_bomon_donvi;
                $canbo->save();

                $user->username = $request->macanbo;
                // $user->password     = bcrypt($request->cmnd); // thay đổi
                $user->ms_sv_canbo = $request->macanbo;
                $user->save();

                foreach ($quanly as $ql) {
                    $ql->delete();
                }

                if ($request->id_chucvu_quanly != null || $request->id_chucvu_quanly != '') {
                    foreach ($request->id_chucvu_quanly as $cv) {
                        $ql = new QuanLy_Model;
                        $ql->id_canbo = $canbo->id;
                        $ql->id_chucvu_quanly = $cv;
                        $ql->save();
                    }
                } else {
                    $phanquyen = new PhanQuyen_Model;
                    $phanquyen->id_user = $user->id;
                    $phanquyen->id_nhomquyen = 7;
                    $phanquyen->trangthai = 1;
                    $phanquyen->save();
                }

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;

                return response()->json(['err_msg' => 'Không thành công'], 401);
            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã cập nhật thông tin cán bộ "' . $canbo->hotencanbo . '"!!!'];

            return response()->json($message, 200);
        }
    }

    public function deleteXoaCanBo($id)
    {
        $canbo = CanBo_Model::find($id);
        $user = User::where('ms_sv_canbo', '=', $canbo->macanbo)->first();
        $quanly = QuanLy_Model::where('id_canbo', '=', $canbo->id)->get();
        $phanquyen = PhanQuyen_Model::where('id_user', '=', $user->id)->get();
        DB::beginTransaction();
        try {
            foreach ($phanquyen as $pq) {
                $pq->delete();
            }

            foreach ($quanly as $cv) {
                $cv->delete();
            }

            $user->delete();

            $canbo->delete();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
            return response()->json(['err_msg' => 'Không thành công'], 401);
        }

        $message = ['message' => 'Đã xóa thông tin cán bộ "' . $canbo->hotencanbo . '"!!!'];

        return response()->json($message, 200);
    }
}
