<?php

namespace App\Http\Controllers;

use App\HocKy_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
class Chung_Controller extends Controller
{
    //

    public function kiemTraHocKy(Request $request)
    {
        $hk = DB::table('tbl_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->where([['tbl_hocky.tgbatdau', '<=', $request->tg], ['tbl_hocky.tgketthuc', '>=', $request->tg]])
            ->select('tbl_hocky.id', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->first();

        $response = ['hk' => $hk];

        return response()->json($response, 200);
    }

    public function getThongTinBangDiem(Request $request)
    {
        $bangdiem = DB::table('tbl_bangdiemdanhgia')
            ->where([['id_sv', '=', $request->id_sv], ['id_thoigiandanhgia', '=', $request->id_thoigiandanhgia]])
            ->select('id')
            ->first();

        $response = ['bangdiem' => $bangdiem];

        return response()->json($response, 200);
    }

}
