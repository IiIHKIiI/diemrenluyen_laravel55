<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\NhomQuyen_Model;

class NhomQuyen_Controller extends Controller
{
    //

    public function getDSNhomQuyenToSVAdd()
    {
        $dsnq = DB::table('tbl_nhomquyen')->whereNotIn('tbl_nhomquyen.id', [4, 5, 6, 7, 8])->get();
        $response = ['danhsach' => $dsnq];
        return response()->json($response, 200);
    }

    public function getDSNhomQuyenToCBAdd()
    {
        $dsnq = NhomQuyen_Model::all();
        $response = ['danhsach' => $dsnq];
        return response()->json($response, 200);
    }
}
