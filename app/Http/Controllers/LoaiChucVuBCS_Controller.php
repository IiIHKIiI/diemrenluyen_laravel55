<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\LoaiChucVuBCS_Model;
class LoaiChucVuBCS_Controller extends Controller
{
    //

    public function getDanhSachLoaiChucVuBCS() {
        $danhsach = LoaiChucVuBCS_Model::all();
        $response = ['danhsach' => $danhsach];
        return response()->json( $response, 200);
    }
}
