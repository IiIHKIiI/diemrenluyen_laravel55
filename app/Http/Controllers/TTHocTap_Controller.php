<?php

namespace App\Http\Controllers;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use App\SinhVien_Model;
use Illuminate\Support\Facades\DB;
use App\TinhTrangHocTap_Model;
use Validator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;

class TTHocTap_Controller extends Controller
{
    //

    public function getAllDanhSachTHHT(Request $request) {
        $ttht = DB::table('tbl_tinhtranghoctap')->join('tbl_loaitinhtranghoctap', 'tbl_tinhtranghoctap.id_loaitthoctap', '=', 'tbl_loaitinhtranghoctap.id')
                                                ->join('tbl_sinhvien', 'tbl_tinhtranghoctap.id_sv', '=', 'tbl_sinhvien.id')
                                                ->join('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
                                                ->select('tbl_tinhtranghoctap.*', 'tbl_sinhvien.mssv', 'tbl_sinhvien.hoten' , 'tbl_sinhvien.id_lop','tbl_lop.tenlop', 'tbl_loaitinhtranghoctap.tenloai_tthoctap')
                                                ->where('tbl_sinhvien.id_lop', 'LIKE', $request->id_lop)
                                                ->get();
                                                
        $response = ['danhsach' => $ttht];
        return response()->json($response, 200);
    }

    public function getTTHTById($id) {
        $ttht = DB::table('tbl_tinhtranghoctap')->join('tbl_loaitinhtranghoctap', 'tbl_tinhtranghoctap.id_loaitthoctap', '=', 'tbl_loaitinhtranghoctap.id')
                                                ->join('tbl_sinhvien', 'tbl_tinhtranghoctap.id_sv', '=', 'tbl_sinhvien.id')
                                                ->join('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
                                                ->select('tbl_tinhtranghoctap.*', 'tbl_sinhvien.mssv', 'tbl_sinhvien.hoten' , 'tbl_sinhvien.id_lop','tbl_lop.tenlop', 'tbl_loaitinhtranghoctap.tenloai_tthoctap')
                                                ->where('tbl_tinhtranghoctap.id', '=', $id)
                                                ->get();
                                                
        $response = ['thongtin' => $ttht];
        return response()->json($response, 200);
    }

    public function putSuaTTHT(Request $request, $id) {
        $ttht = TinhTrangHocTap_Model::find($id);
        $lop  = DB::table('tbl_lop')
        ->join('tbl_sinhvien', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')                      
        ->select('tbl_lop.tenlop', 'tbl_sinhvien.mssv')                              
        ->where('tbl_sinhvien.id','=', $ttht->id_sv)
        ->first();

        $message = [
            'id_loaitthoctap.required' => 'Bạn chưa chọn loại tình trạng học tập',
            'tgbatdau.required' => 'Bạn chưa chọn thời gian bắt đầu',
            'tgketthuc.required' => 'Bạn chưa chọn thời gian kết thúc',
        ];

        $validator = Validator::make($request->all(),
        [
            'id_loaitthoctap' => 'required',
            'tgbatdau' => 'required',
            'tgketthuc' => 'required',
        ], $message);

        if($validator->fails()){
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors()
            ]);
        }else{
            try{
                $ttht->id_loaitthoctap              = $request->id_loaitthoctap;
                $ttht->tgbatdau                     = $request->tgbatdau;
                $ttht->tgketthuc                    = $request->tgketthuc;
                $ttht->trangthai                    = $request->trangthai;

                if($request->hasFile('filename_quyetdinh')){
                    $name = $request->filename_quyetdinh->getClientOriginalName();
                    $path = $request->filename_quyetdinh->storeAs('ThongTinHocTap/' . $lop->tenlop . '/' . $lop->mssv, $name);
                    $ttht->filename_quyetdinh       = $name;              
                }else {
                    $ttht->filename_quyetdinh       = null;         
                }

                $ttht->save();

                return response()->json(['message' => 'Đã chỉnh sửa thành công tình trạng học tập của sinh viên'], 200);

            }catch(QueryException $e){   
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }  
    }

    public function deleteXoaTTHocTap($id) {
        try{
           
            $ttht = TinhTrangHocTap_Model::find($id);
            $ttht->delete();

            return response()->json(['message' => 'Đã xóa thành công tình trạng học tập của sinh viên'], 200);

        }catch(QueryException $e){   
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }
    }

    public function demSoLuong(Request $request) {
        $dem = DB::table('tbl_loaitinhtranghoctap')->select(DB::raw('tbl_loaitinhtranghoctap.tenloai_tthoctap'),DB::raw('count(tbl_tinhtranghoctap.id_sv) as soluong'))
                                        ->leftjoin('tbl_tinhtranghoctap', 'tbl_loaitinhtranghoctap.id', '=', 'tbl_tinhtranghoctap.id_loaitthoctap')
                                        ->join('tbl_sinhvien', 'tbl_sinhvien.id', '=', 'tbl_tinhtranghoctap.id_sv')
                                        ->where('tbl_sinhvien.id_lop','like', $request->id_lop)
                                        ->groupBy('tbl_loaitinhtranghoctap.tenloai_tthoctap')
                                        ->get();
        $response = ['soluong' => $dem];
        return response()->json($response, 200);
                                        
                                        
    }

    public function getFileTHHT($id_sv)
    {
        $hoctap = DB::table('tbl_lop')
        ->leftjoin('tbl_sinhvien', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')
        ->leftjoin('tbl_tinhtranghoctap', 'tbl_tinhtranghoctap.id_sv', '=', 'tbl_sinhvien.id')
        ->where('tbl_sinhvien.id', '=', $id_sv)
        ->select('tbl_tinhtranghoctap.filename_quyetdinh', 'tbl_sinhvien.mssv', 'tbl_lop.tenlop')
        ->first();

        return Storage::download('ThongTinHocTap/' . $hoctap->tenlop . '/' . $hoctap->mssv . '/'. $hoctap->filename_quyetdinh);

    }
}
