<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BoMon_DonVi_Model;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
class BoMon_DonVi_Controller extends Controller
{
    //

    public function getDanhSachBoMonDonVi(){
        try{
            $getDSBM = DB::table('tbl_bomon_donvi')->leftjoin('tbl_khoa','tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
            ->select('tbl_bomon_donvi.id','tbl_bomon_donvi.mabomon', 'tbl_bomon_donvi.tenbomon', 'tbl_khoa.tenkhoa')
            ->get();
            $response = ['danhsach' => $getDSBM];
            return response()->json($response, 200);
        }catch(Exception $e){ 
            return response()->json($e);
        }
    }

    public function getDanhSachBoMonDonVi_TrucThuoc($tructhuoc) {
        try{
            $getDSBM = DB::table('tbl_bomon_donvi')->leftjoin('tbl_khoa','tbl_bomon_donvi.tructhuoc', '=', 'tbl_khoa.id')
                                                    ->select('tbl_bomon_donvi.id','tbl_bomon_donvi.mabomon', 'tbl_bomon_donvi.tenbomon', 'tbl_khoa.tenkhoa')
                                                    ->where('tbl_bomon_donvi.tructhuoc','=', $tructhuoc)
                                                    ->get();
            $response = ['danhsach' => $getDSBM];
            return response()->json($response, 200);
        }catch(Exception $e){ 
            return response()->json($e);
        }
    }

    public function getBoMonById($id){
        try{
            $getDSBM = BoMon_DonVi_Model::find($id);
            $response = ['bomon' => $getDSBM];
            return response()->json($response, 200);
        }catch(Exception $e){ 
            return response()->json($e);
        }
    }

    public function postThemBoMon(Request $request) 
    {
        $message = [
            'mabomon.required' => 'Mã bộ môn không được bỏ trống',
            'tenbomon.required' => 'Tên bộ môn không được bỏ trống',
            'tructhuoc.required' => 'Chưa chọn đơn vị trực thuộc',
        ];

        $validator = Validator::make($request->all(),
        [
            'mabomon'   => 'required',
            'tenbomon'  => 'required',
            'tructhuoc' => 'required'       
        ], $message);

        if($validator->fails()){
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors()
            ]);
        }else{
            try{
                $bomon           = new BoMon_DonVi_Model;
                $bomon->mabomon   = $request->mabomon;
                $bomon->tenbomon  = $request->tenbomon;
                $bomon->tructhuoc  = $request->tructhuoc;
                
                $bomon->save();

                $message = ['message' => 'Đã thêm bộ môn mới: "' . $bomon->tenbomon . '" với mã bộ môn: "' . $bomon->mabomon . '"!!!'];
                return response()->json($message, 200);
            }catch(QueryException $e){   
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function putSuaBoMon(Request $request, $id)
    {
        $bomon   = BoMon_DonVi_Model::find($id);
        $old_name = $bomon->tenbomon;
        $message = [
            'mabomon.required' => 'Mã bộ môn không được bỏ trống',
            'tenbomon.required' => 'Tên bộ môn không được bỏ trống',
            'tructhuoc.required' => 'Chưa chọn đơn vị trực thuộc',
        ];

        $validator = Validator::make($request->all(),
        [
            'mabomon'   => 'required',
            'tenbomon'  => 'required',
            'tructhuoc' => 'required'
            

        ], $message);

        if($validator->fails()){
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors()
            ]);
        }else{
            try{

                $bomon->tenbomon  = $request->tenbomon;
                $bomon->mabomon   = $request->mabomon;
                $bomon->tructhuoc  = $request->tructhuoc;
                $bomon->save();

                $message = ['message' => 'Đã cập nhật thông tin bộ môn "' . $bomon->tenbomon . '"!!!'];

                return response()->json($message, 200);
            }catch(QueryException $e){   
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function deleteXoaBoMon($id) {
        $bomon   = BoMon_DonVi_Model::find($id);
        $old_name = $bomon->tenbomon;
        try{
            $bomon->delete();

            $message = ['message' => 'Đã xóa thông tin bộ môn "' . $old_name . '"!!!'];

            return response()->json($message, 200);
        }catch(QueryException $e){   
            $maLoi = $e->errorInfo[1]; // 1451: Lỗi có dữ liệu bên trong
            return response()->json(['maloi' => $maLoi]);

        }
    }
}
