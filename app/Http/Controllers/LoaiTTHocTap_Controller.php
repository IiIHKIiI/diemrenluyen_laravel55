<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\LoaiTTHocTap_Model;
class LoaiTTHocTap_Controller extends Controller
{
    //
    public function getAllLoaiTTHT() {
        $danhsach = LoaiTTHocTap_Model::all();
        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

   
}
