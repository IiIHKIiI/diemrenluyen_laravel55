<?php

namespace App\Http\Controllers;

use App\CoVanHocTap_Model;
use App\Lop_Model;
use App\PhanQuyen_Model;
use App\CanBo_Model;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class CoVanHocTap_Controller extends Controller
{
    //
    public function getDanhSachCoVanHocTap(Request $request)
    {
        $danhsach = DB::table('tbl_chitiet_cvht')
            ->leftjoin('tbl_hocky as a', 'a.id', '=', 'tbl_chitiet_cvht.id_hockybatdau')
            ->leftjoin('tbl_namhoc as c', 'a.id_namhoc', '=', 'c.id')
            ->leftjoin('tbl_hocky as b', 'b.id', '=', 'tbl_chitiet_cvht.id_hockyketthuc')
            ->leftjoin('tbl_namhoc as d', 'b.id_namhoc', '=', 'd.id')
            ->leftjoin('tbl_lop as e', 'e.id', '=', 'tbl_chitiet_cvht.id_lop')
            ->leftjoin('tbl_canbo as f', 'f.id', '=', 'tbl_chitiet_cvht.id_canbo')
            ->leftjoin('tbl_nganh', 'tbl_nganh.id', '=', 'e.id_nganh')
            ->where('tbl_nganh.id_bomon_donvi', 'like', $request->id_bomon_donvi)
            ->select('f.id', 'tbl_chitiet_cvht.id_lop', 'a.mahocky as hockybatdau', 'c.manamhoc as namhocbatdau', 'b.mahocky as hockyketthuc', 'd.manamhoc as namhocketthuc', 'e.tenlop', 'f.hotencanbo', 'f.macanbo')
            ->get();

        $response = ['danhsach' => $danhsach];
        return response()->json($response, 200);
    }

    public function getThongTinCoVanHocTapById($id)
    {
        $cvht = DB::table('tbl_chitiet_cvht')
            ->leftjoin('tbl_hocky as a', 'a.id', '=', 'tbl_chitiet_cvht.id_hockybatdau')
            ->leftjoin('tbl_namhoc as c', 'a.id_namhoc', '=', 'c.id')
            ->leftjoin('tbl_hocky as b', 'b.id', '=', 'tbl_chitiet_cvht.id_hockyketthuc')
            ->leftjoin('tbl_namhoc as d', 'b.id_namhoc', '=', 'd.id')
            ->leftjoin('tbl_lop as e', 'e.id', '=', 'tbl_chitiet_cvht.id_lop')
            ->leftjoin('tbl_nganh', 'tbl_nganh.id', '=', 'e.id_nganh')
            ->leftjoin('tbl_bomon_donvi', 'tbl_bomon_donvi.id', '=', 'tbl_nganh.id_bomon_donvi')
            ->leftjoin('tbl_khoa', 'tbl_khoa.id', '=', 'tbl_bomon_donvi.tructhuoc')
            ->leftjoin('tbl_canbo as f', 'f.id', '=', 'tbl_chitiet_cvht.id_canbo')

            ->where('f.id', '=', $id)
            ->select('tbl_bomon_donvi.tructhuoc', 'tbl_nganh.id_bomon_donvi', 'tbl_chitiet_cvht.*')
            ->first();

        $response = ['cvht' => $cvht];
        return response()->json($response, 200);
    }

    public function postThemCoVanHocTap(Request $request)
    {
        $message = [
            'id_hockybatdau.required' => 'Chưa chọn học kỳ bắt đầu',
            'id_hockyketthuc.required' => 'Chưa chọn học kỳ kết thúc',
            'id_lop.required' => 'Chưa chọn lớp',
            'id_canbo.required' => 'Chưa chọn cán bộ làm cố vấn học tập',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_hockybatdau' => 'required',
                'id_hockyketthuc' => 'required',
                'id_lop' => 'required',
                'id_canbo' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                DB::beginTransaction();
                $cvht = new CoVanHocTap_Model;
                $cvht->id_hockybatdau = $request->id_hockybatdau;
                $cvht->id_hockyketthuc = $request->id_hockyketthuc;
                $cvht->id_lop = $request->id_lop;
                $cvht->id_canbo = $request->id_canbo;
                $cvht->save();

                $canbo = DB::table('tbl_canbo')->where('tbl_canbo.id','=', $request->id_canbo)->select('tbl_canbo.macanbo')->first();
                $user = User::where('ms_sv_canbo', '=', $canbo->macanbo)->select('id')->first();

                $phanquyen = new PhanQuyen_Model;
                $phanquyen->id_user = $user->id;
                $phanquyen->id_nhomquyen = 3;
                $phanquyen->trangthai = 1;
                $phanquyen->save();

                $lop = Lop_Model::find($request->id_lop);

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['err_msg' => 'Không thành công'], 401);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã thêm cố vấn học tập cho lớp  "' . $lop->tenlop . '"!!!'];

            return response()->json($message, 200);
        }
    }

    public function putSuaCoVanHocTap(Request $request, $id)
    {

        $message = [
            'id_hockybatdau.required' => 'Chưa chọn học kỳ bắt đầu',
            'id_hockyketthuc.required' => 'Chưa chọn học kỳ kết thúc',
            'id_lop.required' => 'Chưa chọn lớp',
            'id_canbo.required' => 'Chưa chọn cán bộ làm cố vấn học tập',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_hockybatdau' => 'required',
                'id_hockyketthuc' => 'required',
                'id_lop' => 'required',
                'id_canbo' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                DB::beginTransaction();
                $cvht->id_hockybatdau = $request->id_hockybatdau;
                $cvht->id_hockyketthuc = $request->id_hockyketthuc;
                $cvht->id_lop = $request->id_lop;
                $cvht->id_canbo = $request->id_canbo;

                $cvht->save();

                $lop = Lop_Model::find($request->id_lop);

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['err_msg' => 'Không thành công'], 401);
            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã cập nhật thông tin cố vấn học tập cho lớp "' . $lop->tenlop . '"!!!'];

            return response()->json($message, 200);
        }
    }

    public function postXoaCoVanHocTap(Request $request)
    {
        $cvht = CoVanHocTap_Model::where([['id_canbo', '=', $request->id_canbo], ['id_lop', '=', $request->id_lop]])->first();

        try {

            $cvht->delete();

            $message = ['message' => 'Đã xóa thông tin cố vấn học tập!!!'];

            return response()->json($message, 200);
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }
    }

}
