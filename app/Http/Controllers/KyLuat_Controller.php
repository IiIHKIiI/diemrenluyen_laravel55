<?php

namespace App\Http\Controllers;

use App\KyLuat_Model;
use App\SinhVien_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Storage;

class KyLuat_Controller extends Controller
{
    //
    public function getAllDanhSachKyLuat(Request $request)
    {
        $kyluat = DB::table('tbl_kyluat')->join('tbl_loaikyluat', 'tbl_kyluat.id_loaikyluat', '=', 'tbl_loaikyluat.id')
            ->join('tbl_sinhvien', 'tbl_kyluat.id_sv', '=', 'tbl_sinhvien.id')
            ->join('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->select('tbl_kyluat.*', 'tbl_sinhvien.mssv', 'tbl_sinhvien.hoten', 'tbl_sinhvien.id_lop', 'tbl_lop.tenlop', 'tbl_loaikyluat.tenloai_kyluat')
            ->where('tbl_sinhvien.id_lop', 'LIKE', $request->id_lop)
            ->get();

        $response = ['danhsach' => $kyluat];
        return response()->json($response, 200);
    }

    public function getKyLuatById($id)
    {
        $kyluat = DB::table('tbl_kyluat')->join('tbl_loaikyluat', 'tbl_kyluat.id_loaikyluat', '=', 'tbl_loaikyluat.id')
            ->join('tbl_sinhvien', 'tbl_kyluat.id_sv', '=', 'tbl_sinhvien.id')
            ->join('tbl_lop', 'tbl_sinhvien.id_lop', '=', 'tbl_lop.id')
            ->select('tbl_kyluat.*', 'tbl_sinhvien.mssv', 'tbl_sinhvien.hoten', 'tbl_sinhvien.id_lop', 'tbl_lop.tenlop', 'tbl_loaikyluat.tenloai_kyluat')
            ->where('tbl_kyluat.id', '=', $id)
            ->first();

        $response = ['thongtin' => $kyluat];
        return response()->json($response, 200);
    }

    public function postThemKyLuat(Request $request)
    {
        $lop = DB::table('tbl_lop')
            ->leftjoin('tbl_sinhvien', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')
            ->where('tbl_sinhvien.id', '=', $request->id_sv)
            ->select('tbl_lop.tenlop', 'tbl_sinhvien.mssv')
            ->first();

        $message = [
            'id_loaikyluat.required' => 'Bạn chưa chọn loại kỷ luật',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_loaikyluat' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {

                // dd($request->all());
                $kyluat = new KyLuat_Model;
                $kyluat->id_loaikyluat = $request->id_loaikyluat;
                $kyluat->tgbatdau = $request->tgbatdau;
                $kyluat->tgketthuc = $request->tgketthuc;
                $kyluat->trangthai = $request->trangthai;
                $kyluat->id_sv = $request->id_sv;

                if ($request->hasFile('filename_quyetdinh')) {
                    $name = $request->filename_quyetdinh->getClientOriginalName();
                    $path = $request->filename_quyetdinh->storeAs('ThongTinKyLuat/' . $lop->tenlop . '/' . $lop->mssv, $name);
                    $kyluat->filename_quyetdinh = $name;
                }

                $kyluat->save();

                $sinhvien = SinhVien_Model::find($request->id_sv);

                $message = ['message' => 'Đã thêm thông tin kỷ luật cho sinh viên "' . $sinhvien->hoten . '"!!!'];
                return response()->json($message, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function putSuaKyLuat(Request $request, $id)
    {
        $kyluat = KyLuat_Model::find($id);
        $lop = DB::table('tbl_lop')
            ->leftjoin('tbl_sinhvien', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')
            ->where('tbl_sinhvien.id', '=', $request->id_sv)
            ->select('tbl_lop.tenlop', 'tbl_sinhvien.mssv')
            ->first();

        $message = [
            'id_loaikyluat.required' => 'Bạn chưa chọn loại kỷ luật',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_loaikyluat' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                $kyluat->id_loaikyluat = $request->id_loaikyluat;
                $kyluat->tgbatdau = $request->tgbatdau;
                $kyluat->tgketthuc = $request->tgketthuc;
                $kyluat->trangthai = $request->trangthai;

                if ($request->hasFile('filename_quyetdinh')) {
                    $name = $request->filename_quyetdinh->getClientOriginalName();
                    $path = $request->filename_quyetdinh->storeAs('ThongTinKyLuat/' . $lop->tenlop . '/' . $lop->mssv, $name);

                    $kyluat->filename_quyetdinh = $name;
                }

                $kyluat->save();

                $sinhvien = SinhVien_Model::find($request->id_sv);

                $message = ['message' => 'Đã cập nhật thông tin kỷ luật cho sinh viên "' . $sinhvien->hoten . '"!!!'];
                return response()->json($message, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function deleteXoaKyLuat($id)
    {
        $kl = KyLuat_Model::find($id);
        $sinhvien = SinhVien_Model::find($kl->id_sv);

        try {

            $kl->delete();

            $message = ['message' => 'Đã xóa thông tin kỷ luật của sinh viên "' . $sinhvien->hoten . '"!!!'];
            return response()->json($message, 200);

        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }
    }

    public function demSoLuong(Request $request)
    {
        $dem = DB::table('tbl_loaikyluat')->select(DB::raw('tbl_loaikyluat.tenloai_kyluat'), DB::raw('count(tbl_kyluat.id_sv) as soluong'))
            ->leftjoin('tbl_kyluat', 'tbl_loaikyluat.id', '=', 'tbl_kyluat.id_loaikyluat')
            ->join('tbl_sinhvien', 'tbl_sinhvien.id', '=', 'tbl_kyluat.id_sv')
            ->where('tbl_sinhvien.id_lop', 'like', $request->id_lop)
            ->groupBy('tbl_loaikyluat.tenloai_kyluat')
            ->get();
        $response = ['soluong' => $dem];
        return response()->json($response, 200);

    }

    public function getFileKyLuat($id_sv)
    {
        $kyluat = DB::table('tbl_lop')
        ->leftjoin('tbl_sinhvien', 'tbl_lop.id', '=', 'tbl_sinhvien.id_lop')
        ->leftjoin('tbl_kyluat', 'tbl_kyluat.id_sv', '=', 'tbl_sinhvien.id')
        ->where('tbl_sinhvien.id', '=', $id_sv)
        ->select('tbl_kyluat.filename_quyetdinh', 'tbl_sinhvien.mssv', 'tbl_lop.tenlop')
        ->first();

        return Storage::download('ThongTinKyLuat/' . $kyluat->tenlop . '/' . $kyluat->mssv . '/'. $kyluat->filename_quyetdinh);
    }
}
