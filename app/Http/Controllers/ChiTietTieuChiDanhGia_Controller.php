<?php

namespace App\Http\Controllers;

use App\ChiTietTieuChiDanhGia_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ChiTietTieuChiDanhGia_Controller extends Controller
{
    //
    public function getDanhSachLoaiDiem()
    {
        $danhsach = DB::table('tbl_loaidiem')->get();

        $response = ['danhsach' => $danhsach];
        return response()->json($response, 200);
    }

    /* Hàm Đệ Quy Hiển Thị Đa Cấp */
    public function deQuyDaCap($danhsach, $parent, $level, &$newDanhSach)
    {
        if (count($danhsach) > 0) {
            foreach ($danhsach as $key => $value) {
                if ($value->id_tieuchicha == $parent) {
                    $value->level = $level;
                    $newDanhSach[] = $value;
                    unset($danhsach[$key]);
                    $newParent = $value->id;
                    $this->deQuyDaCap($danhsach, $newParent, $level + 1, $newDanhSach);
                }
            }
        }
        return $newDanhSach;
    }

    public function getTieuChiDanhGiaByIdTieuChi($id_tieuchidanhgia)
    {

        $tieuchi = DB::table('tbl_chitiettieuchi')
            ->leftjoin('tbl_tieuchidanhgia', 'tbl_tieuchidanhgia.id_quyetdinhtieuchi', '=', 'tbl_chitiettieuchi.id_quyetdinhtieuchi')
            ->where('tbl_tieuchidanhgia.id', '=', $id_tieuchidanhgia)
            ->select('tbl_chitiettieuchi.*')
            ->get();

        $response = ['tieuchi' => $this->deQuyDaCap($tieuchi, null, 1, $newDanhSach)];
        return response()->json($response, 200);
    }

    public function getDanhSachCacTieuChiVuaThem($id_quyetdinhtieuchi)
    {
        $danhsach = DB::table('tbl_chitiettieuchi as a')
            ->leftjoin('tbl_loaidiem', 'tbl_loaidiem.id', '=', 'a.id_loaidiem')
            ->where('a.id_quyetdinhtieuchi', '=', $id_quyetdinhtieuchi)
            ->select('a.*', 'tbl_loaidiem.tenloaidiem')
            ->get();


        $response = ['danhsach' => $this->deQuyDaCap($danhsach, null, 1, $newDanhSach)];
        
        return response()->json($response, 200);
    }

    public function getDanhSachCacChiMucVuaThem($id_quyetdinhtieuchi)
    {
        $danhsach = DB::table('tbl_chitiettieuchi as a')
            ->leftjoin('tbl_chitiettieuchi as b', 'b.id', '=', 'a.id_tieuchicha')
            ->where('a.id_quyetdinhtieuchi', '=', $id_quyetdinhtieuchi)
            ->select('a.id', 'a.chimuc_tieuchi', 'a.diemthuong', DB::raw('b.chimuc_tieuchi as chimuc_tieuchicha'))
            ->get();

        $response = ['danhsach' => $danhsach];
        return response()->json($response, 200);
    }

    public function getInfoTieuChi($id)
    {
        $tc = ChiTietTieuChiDanhGia_Model::find($id);
        $response = ['thongtin' => $tc];
        return response()->json($response, 200);
    }

    public function postThemTieuChi(Request $request)
    {
        $message = [
            'chimuc_tieuchi.required' => 'Bạn chưa điền chỉ mục tiêu chí đánh giá',
            'khongdanhgia.required' => 'Bạn chưa chọn loại tiêu chí',
            'id_quyetdinhtieuchi.required' => 'Bạn chưa chọn quyết định tiêu chí đánh giá',
            'tentieuchi.required' => 'Bạn chưa điền tên tiêu chí đánh giá',
            'diemtoida.required' => 'Bạn chưa điền điểm tối đa',
            'can_minhchung.required' => 'Bạn chưa chọn có cần minh chứng hay không',
        ];

        $validator = Validator::make($request->all(),
            [
                'chimuc_tieuchi' => 'required',
                'khongdanhgia' => 'required',
                'id_quyetdinhtieuchi' => 'required',
                'tentieuchi' => 'required',
                'diemtoida' => 'required',
                'can_minhchung' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {
                $cttc = new ChiTietTieuChiDanhGia_Model;
                $cttc->id_quyetdinhtieuchi = $request->id_quyetdinhtieuchi;
                $cttc->chimuc_tieuchi = $request->chimuc_tieuchi;
                $cttc->khongdanhgia = $request->khongdanhgia;
                $cttc->tentieuchi = $request->tentieuchi;
                $cttc->id_tieuchicha = $request->id_tieuchicha;
                $cttc->id_loaidiem = $request->id_loaidiem;
                $cttc->is_tieuchigoc = $request->is_tieuchigoc;
                $cttc->diemtoida = $request->diemtoida;
                $cttc->can_minhchung = $request->can_minhchung;
                $cttc->diemthuong = $request->diemthuong;

                $cttc->save();

                $response = ['message' => 'Đã thêm tiêu chí !!!'];
                return response()->json($response, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function putSuaTieuChi(Request $request, $id)
    {
        $cttc = ChiTietTieuChiDanhGia_Model::find($id);
        $message = [
            'chimuc_tieuchi.required' => 'Bạn chưa điền chỉ mục tiêu chí đánh giá',
            'khongdanhgia.required' => 'Bạn chưa chọn loại tiêu chí',
            'id_quyetdinhtieuchi.required' => 'Bạn chưa chọn quyết định tiêu chí đánh giá',
            'tentieuchi.required' => 'Bạn chưa điền tên tiêu chí đánh giá',
            'diemtoida.required' => 'Bạn chưa điền điểm tối đa',
            'can_minhchung.required' => 'Bạn chưa chọn có cần minh chứng hay không',
        ];

        $validator = Validator::make($request->all(),
            [
                'chimuc_tieuchi' => 'required',
                'khongdanhgia' => 'required',
                'id_quyetdinhtieuchi' => 'required',
                'tentieuchi' => 'required',
                'diemtoida' => 'required',
                'can_minhchung' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            try {

                $cttc->id_quyetdinhtieuchi = $request->id_quyetdinhtieuchi;
                $cttc->chimuc_tieuchi = $request->chimuc_tieuchi;
                $cttc->khongdanhgia = $request->khongdanhgia;
                $cttc->tentieuchi = $request->tentieuchi;
                $cttc->id_tieuchicha = $request->id_tieuchicha;
                $cttc->id_loaidiem = $request->id_loaidiem;
                $cttc->is_tieuchigoc = $request->is_tieuchigoc;
                $cttc->diemtoida = $request->diemtoida;
                $cttc->can_minhchung = $request->can_minhchung;
                $cttc->diemthuong = $request->diemthuong;

                $cttc->save();

                $response = ['message' => 'Đã cập nhật tiêu chí !!!'];
                return response()->json($response, 200);

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }
        }
    }

    public function deleteXoaTieuChi($idtieuchi)
    {
        $tc = ChiTietTieuChiDanhGia_Model::find($idtieuchi);
        $old_name = $tc->tentieuchi;
        try {
            $tc->delete();

            $message = ['message' => 'Đã xóa tiêu chí "' . $old_name . '"!!!'];

            return response()->json($message, 200);
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1451: Lỗi có dữ liệu bên trong
            return response()->json(['maloi' => $maLoi]);

        }
    }
}
