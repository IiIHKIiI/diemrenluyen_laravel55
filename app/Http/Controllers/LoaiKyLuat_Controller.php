<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\LoaiKyLuat_Model;
class LoaiKyLuat_Controller extends Controller
{
    //
    public function getAllLoaiKyLuat() {
        $danhsach = LoaiKyLuat_Model::all();
        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }
}
