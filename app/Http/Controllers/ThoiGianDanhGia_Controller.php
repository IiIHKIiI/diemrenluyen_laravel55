<?php

namespace App\Http\Controllers;

use App\ThoiGianDanhGia_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ThoiGianDanhGia_Controller extends Controller
{
    //
    public function getDanhSachThoiGianDanhGia()
    {

        $danhsach = DB::table('tbl_thoigiandanhgia')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id', '=', 'tbl_thoigiandanhgia.id_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->select('tbl_thoigiandanhgia.*', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getInfoThoiGianDanhGiaById($id)
    {
        $thongtin = DB::table('tbl_hocky')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_hocky.id', '=', 'tbl_thoigiandanhgia.id_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->where('tbl_thoigiandanhgia.id', '=', $id)
            ->select('tbl_thoigiandanhgia.*', 'tbl_hocky.id_namhoc')
            ->first();
        $response = ['thongtin' => $thongtin];

        return response()->json($response, 200);
    }

    /* Hàm Đệ Quy Hiển Thị Đa Cấp */
    public function deQuyDaCap($danhsach, $parent, $level, &$newDanhSach)
    {
        if (count($danhsach) > 0) {
            foreach ($danhsach as $key => $value) {
                if ($value->id_tieuchicha == $parent) {
                    $value->level = $level;
                    $newDanhSach[] = $value;
                    unset($danhsach[$key]);
                    $newParent = $value->id;
                    $this->deQuyDaCap($danhsach, $newParent, $level + 1, $newDanhSach);
                }
            }
        }
        return $newDanhSach;
    }

    public function getInfoThoiGianDanhGiaByIdHocKy($id_hocky)
    {
        $thongtin = DB::table('tbl_hocky')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_hocky.id', '=', 'tbl_thoigiandanhgia.id_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->where('tbl_thoigiandanhgia.id_hocky', '=', $id_hocky)
            ->select('tbl_thoigiandanhgia.*', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->first();

        $tieuchi = DB::table('tbl_chitiettieuchi')
            ->leftjoin('tbl_tieuchidanhgia', 'tbl_tieuchidanhgia.id_quyetdinhtieuchi', '=', 'tbl_chitiettieuchi.id_quyetdinhtieuchi')
            ->where('tbl_tieuchidanhgia.id', '=', $thongtin->id_tieuchidanhgia)
            ->select('tbl_chitiettieuchi.*')
            ->get();

        $response = ['thongtin' => $thongtin];
        return response()->json($response, 200);
    }

    public function postThemThoiGianDanhGia(Request $request)
    {
        $message = [
            'id_hocky.required' => 'Chưa chọn học kỳ mở đánh giá',
            'id_tieuchidanhgia.required' => 'Chưa chọn tiêu chí đánh giá',
            'tgbatdau_sv.required' => 'Chưa chọn ngày bắt đầu đánh giá của sinh viên',
            'tgketthuc_sv.required' => 'Chưa chọn ngày kết thúc đánh giá của sinh viên',
            'tgbatdau_bancansu.required' => 'Chưa chọn ngày bắt đầu đánh giá của ban cán sự',
            'tgketthuc_bancansu.required' => 'Chưa chọn ngày kết thúc đánh giá của ban cán sự',
            'tgbatdau_cvht.required' => 'Chưa chọn ngày bắt đầu đánh giá của cố vấn học tập',
            'tgketthuc_cvht.required' => 'Chưa chọn ngày kết thúc đánh giá của cố vấn học tập',
            'tgbatdau_hoidongkhoa.required' => 'Chưa chọn ngày bắt đầu đánh giá của hội đồng khoa',
            'tgketthuc_hoidongkhoa.required' => 'Chưa chọn ngày kết thúc đánh giá của hội đồng khoa',
            'tgbatdau_hoidongtruong.required' => 'Chưa chọn ngày bắt đầu đánh giá của hội đồng trường',
            'tgketthuc_hoidongtruong.required' => 'Chưa chọn ngày kết thúc đánh giá của hội đồng trường',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_hocky' => 'required',
                'id_tieuchidanhgia' => 'required',
                'tgbatdau_sv' => 'required',
                'tgketthuc_sv' => 'required',
                'tgbatdau_bancansu' => 'required',
                'tgketthuc_bancansu' => 'required',
                'tgbatdau_cvht' => 'required',
                'tgketthuc_cvht' => 'required',
                'tgbatdau_hoidongkhoa' => 'required',
                'tgketthuc_hoidongkhoa' => 'required',
                'tgbatdau_hoidongtruong' => 'required',
                'tgketthuc_hoidongtruong' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {

            try {
                $namhoc_hocky = DB::table('tbl_hocky')
                    ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
                    ->where('tbl_hocky.id', '=', $request->id_hocky)
                    ->select('tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
                    ->first();

                $thoigiandanhgia = new ThoiGianDanhGia_Model;
                $thoigiandanhgia->id_hocky = $request->id_hocky;
                $thoigiandanhgia->id_tieuchidanhgia = $request->id_tieuchidanhgia;
                $thoigiandanhgia->chopheptre = $request->chopheptre;
                $thoigiandanhgia->diemtru = $request->diemtru;
                $thoigiandanhgia->tgbatdau_sv = $request->tgbatdau_sv;
                $thoigiandanhgia->tgketthuc_sv = $request->tgketthuc_sv;
                $thoigiandanhgia->tgbatdau_bancansu = $request->tgbatdau_bancansu;
                $thoigiandanhgia->tgketthuc_bancansu = $request->tgketthuc_bancansu;
                $thoigiandanhgia->tgbatdau_cvht = $request->tgbatdau_cvht;
                $thoigiandanhgia->tgketthuc_cvht = $request->tgketthuc_cvht;
                $thoigiandanhgia->tgbatdau_hoidongkhoa = $request->tgbatdau_hoidongkhoa;
                $thoigiandanhgia->tgketthuc_hoidongkhoa = $request->tgketthuc_hoidongkhoa;
                $thoigiandanhgia->tgbatdau_hoidongtruong = $request->tgbatdau_hoidongtruong;
                $thoigiandanhgia->tgketthuc_hoidongtruong = $request->tgketthuc_hoidongtruong;

                $thoigiandanhgia->save();

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã thêm thời gian đánh giá cho học kỳ "' . $namhoc_hocky->mahocky . '" năm học "' . $namhoc_hocky->manamhoc . '"!!!'];
            return response()->json($message, 200);
        }
    }

    public function putThemThoiGianDanhGia(Request $request, $id)
    {
        $thoigiandanhgia = ThoiGianDanhGia_Model::find($id);

        $message = [
            'id_hocky.required' => 'Chưa chọn học kỳ mở đánh giá',
            'id_tieuchidanhgia.required' => 'Chưa chọn tiêu chí đánh giá',
            'tgbatdau_sv.required' => 'Chưa chọn ngày bắt đầu đánh giá của sinh viên',
            'tgketthuc_sv.required' => 'Chưa chọn ngày kết thúc đánh giá của sinh viên',
            'tgbatdau_bancansu.required' => 'Chưa chọn ngày bắt đầu đánh giá của ban cán sự',
            'tgketthuc_bancansu.required' => 'Chưa chọn ngày kết thúc đánh giá của ban cán sự',
            'tgbatdau_cvht.required' => 'Chưa chọn ngày bắt đầu đánh giá của cố vấn học tập',
            'tgketthuc_cvht.required' => 'Chưa chọn ngày kết thúc đánh giá của cố vấn học tập',
            'tgbatdau_hoidongkhoa.required' => 'Chưa chọn ngày bắt đầu đánh giá của hội đồng khoa',
            'tgketthuc_hoidongkhoa.required' => 'Chưa chọn ngày kết thúc đánh giá của hội đồng khoa',
            'tgbatdau_hoidongtruong.required' => 'Chưa chọn ngày bắt đầu đánh giá của hội đồng trường',
            'tgketthuc_hoidongtruong.required' => 'Chưa chọn ngày kết thúc đánh giá của hội đồng trường',
        ];

        $validator = Validator::make($request->all(),
            [
                'id_hocky' => 'required',
                'id_tieuchidanhgia' => 'required',
                'tgbatdau_sv' => 'required',
                'tgketthuc_sv' => 'required',
                'tgbatdau_bancansu' => 'required',
                'tgketthuc_bancansu' => 'required',
                'tgbatdau_cvht' => 'required',
                'tgketthuc_cvht' => 'required',
                'tgbatdau_hoidongkhoa' => 'required',
                'tgketthuc_hoidongkhoa' => 'required',
                'tgbatdau_hoidongtruong' => 'required',
                'tgketthuc_hoidongtruong' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {

            try {
                $namhoc_hocky = DB::table('tbl_hocky')
                    ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
                    ->where('tbl_hocky.id', '=', $request->id_hocky)
                    ->select('tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
                    ->first();

                $thoigiandanhgia->id_hocky = $request->id_hocky;
                $thoigiandanhgia->id_tieuchidanhgia = $request->id_tieuchidanhgia;
                $thoigiandanhgia->chopheptre = $request->chopheptre;
                $thoigiandanhgia->diemtru = $request->diemtru;
                $thoigiandanhgia->tgbatdau_sv = $request->tgbatdau_sv;
                $thoigiandanhgia->tgketthuc_sv = $request->tgketthuc_sv;
                $thoigiandanhgia->tgbatdau_bancansu = $request->tgbatdau_bancansu;
                $thoigiandanhgia->tgketthuc_bancansu = $request->tgketthuc_bancansu;
                $thoigiandanhgia->tgbatdau_cvht = $request->tgbatdau_cvht;
                $thoigiandanhgia->tgketthuc_cvht = $request->tgketthuc_cvht;
                $thoigiandanhgia->tgbatdau_hoidongkhoa = $request->tgbatdau_hoidongkhoa;
                $thoigiandanhgia->tgketthuc_hoidongkhoa = $request->tgketthuc_hoidongkhoa;
                $thoigiandanhgia->tgbatdau_hoidongtruong = $request->tgbatdau_hoidongtruong;
                $thoigiandanhgia->tgketthuc_hoidongtruong = $request->tgketthuc_hoidongtruong;

                $thoigiandanhgia->save();

            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã thêm cập nhật thời gian đánh giá cho học kỳ "' . $namhoc_hocky->mahocky . '" năm học "' . $namhoc_hocky->manamhoc . '"!!!'];
            return response()->json($message, 200);
        }
    }

    public function deleteThoiGianDanhGia($id)
    {
        $thoigiandanhgia = ThoiGianDanhGia_Model::find($id);
        $namhoc_hocky = DB::table('tbl_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', '=', 'tbl_namhoc.id')
            ->where('tbl_hocky.id', '=', $thoigiandanhgia->id_hocky)
            ->select('tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->first();
        try {

            $thoigiandanhgia->delete();

        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }

        $message = ['message' => 'Đã hủy thời gian đánh giá cho học kỳ "' . $namhoc_hocky->mahocky . '" năm học "' . $namhoc_hocky->manamhoc . '"!!!'];
        return response()->json($message, 200);
    }
}
