<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ChucVuQuanLy_Model;
class ChucVuQuanLy_Controller extends Controller
{
    //
    public function getDanhSachChucVuQuanLy() {
        $danhsach = ChucVuQuanLy_Model::all();

        $response = ['danhsach' => $danhsach];
        return response()->json($response,200);
    }
}
