<?php

namespace App\Http\Controllers;

use App\PhanQuyen_Model;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PhanQuyen_Controller extends Controller
{
    //
    public function getPhanQuyenByIDUser($id_user)
    {
        $dsphanquyen = PhanQuyen_Model::where('id_user', '=', $id_user)->select('id_nhomquyen')->get();
        $response = ['danhsach' => $dsphanquyen];
        return response()->json($response, 200);
    }

    public function postThemQuyen(Request $request, $id)
    {
        $pq_canbo = PhanQuyen_Model::where('id_user', '=', $id)->get();
        $user = User::find($id);
        DB::beginTransaction();
        try {

            if ($pq_canbo != null) {
                foreach ($pq_canbo as $pqcb) {
                    $pqcb->delete();
                }
            }

            foreach($request->id_nhomquyen as $pq) {
                $phanquyen = new PhanQuyen_Model;
                $phanquyen->id_user = $id;
                $phanquyen->id_nhomquyen = $pq;
                $phanquyen->trangthai = 1;
                $phanquyen->save();
            }
            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

            return response()->json(['err_msg' => 'Không thành công'], 401);

        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }

        $message = ['message' => 'Đã cập nhật phân quyền cho tài khoản "' . $user->username . '"!!!'];

        return response()->json($message, 200);
    }

}
