<?php

namespace App\Http\Controllers;

use App\HocKy_Model;
use App\NamHoc_Model;
use App\ThoiGianDanhGia_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class NamHocHocKy_Controller extends Controller
{
    //
    public function getDanhSachNamHoc()
    {
        $danhsach = NamHoc_Model::all();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getDanhSachHocKyByNamHoc($id_namhoc)
    {
        $hocky = ThoiGianDanhGia_Model::select('id_hocky')->get();
        $danhsach = DB::table('tbl_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', 'tbl_namhoc.id')
            ->where(function ($query) use ($hocky, $id_namhoc) {
                $query->where('tbl_hocky.id_namhoc', 'like', $id_namhoc)
                    ->whereNotIn('tbl_hocky.id', $hocky);
            }) 
            ->select('tbl_hocky.id', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getDanhSachAllHocKyByNamHoc($id_namhoc)
    {
        $danhsach = DB::table('tbl_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', 'tbl_namhoc.id')
            ->where('tbl_hocky.id_namhoc', 'like', $id_namhoc)
            ->select('tbl_hocky.id', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getDanhSachNamHocHocKy()
    {
        $danhsach = DB::table('tbl_namhoc')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id_namhoc', 'tbl_namhoc.id')
            ->select(DB::raw('group_concat( tbl_hocky.mahocky SEPARATOR \', \'   ) AS sohocky'), 'tbl_namhoc.id', 'tbl_namhoc.manamhoc')
            ->groupBy('tbl_namhoc.id', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getDanhSachHocKy()
    {
        $danhsach = DB::table('tbl_namhoc')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id_namhoc', 'tbl_namhoc.id')
            ->select('tbl_hocky.id', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->groupBy('tbl_hocky.id', 'tbl_hocky.mahocky', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['danhsach' => $danhsach];

        return response()->json($response, 200);
    }

    public function getInfoNamHocHocKy($id)
    {
        $hk = DB::table('tbl_namhoc')
            ->leftjoin('tbl_hocky', 'tbl_hocky.id_namhoc', 'tbl_namhoc.id')
            ->where('tbl_namhoc.id', '=', $id)
            ->select('tbl_hocky.*', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['hk' => $hk];

        return response()->json($response, 200);
    }

    public function postThemNamHocHocKy(Request $request)
    {
        $message = [
            'manamhoc.required' => 'Mã năm học không được bỏ trống',
            'tgbatdau_hk1.required' => 'Chưa chọn thời gian bắt đầu học kỳ 1',
            'tgketthuc_hk1.required' => 'Chưa chọn thời gian kết thúc học kỳ 1',
            'tgbatdau_hk2.required' => 'Chưa chọn thời gian bắt đầu học kỳ 2',
            'tgketthuc_hk2.required' => 'Chưa chọn thời gian kết thúc học kỳ 2',
        ];

        $validator = Validator::make($request->all(),
            [
                'manamhoc' => 'required',
                'tgbatdau_hk1' => 'required',
                'tgketthuc_hk1' => 'required',
                'tgbatdau_hk2' => 'required',
                'tgketthuc_hk2' => 'required',
            ], $message);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();
            try {
                $namhoc = new NamHoc_Model;
                $namhoc->manamhoc = $request->manamhoc;
                $namhoc->save();

                $hocky = new HocKy_Model;
                $hocky->mahocky = 'Học Kỳ I';
                $hocky->tgbatdau = $request->tgbatdau_hk1;
                $hocky->tgketthuc = $request->tgketthuc_hk1;
                $hocky->id_namhoc = $namhoc->id;
                $hocky->save();

                $hocky2 = new HocKy_Model;
                $hocky2->mahocky = 'Học Kỳ II';
                $hocky2->tgbatdau = $request->tgbatdau_hk2;
                $hocky2->tgketthuc = $request->tgketthuc_hk2;
                $hocky2->id_namhoc = $namhoc->id;
                $hocky2->save();

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['err_msg' => 'Không thành công'], 401);
            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã thêm năm học "' . $namhoc->manamhoc . '"!!!'];
            return response()->json($message, 200);
        }
    }

    public function putSuaNamHocHocKy(Request $request, $id)
    {
        $namhoc = NamHoc_Model::find($id);

        $hocky = HocKy_Model::where('id_namhoc', '=', $namhoc->id)->get();

        $message = [
            'manamhoc.required' => 'Mã năm học không được bỏ trống',
            'tgbatdau_hk1.required' => 'Chưa chọn thời gian bắt đầu học kỳ 1',
            'tgketthuc_hk1.required' => 'Chưa chọn thời gian kết thúc học kỳ 1',
            'tgbatdau_hk2.required' => 'Chưa chọn thời gian bắt đầu học kỳ 2',
            'tgketthuc_hk2.required' => 'Chưa chọn thời gian kết thúc học kỳ 2',
        ];

        $validator = Validator::make($request->all(),
            [
                'manamhoc' => 'required',
                'tgbatdau_hk1' => 'required',
                'tgketthuc_hk1' => 'required',
                'tgbatdau_hk2' => 'required',
                'tgketthuc_hk2' => 'required',
            ], $message);


        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed.',
                'error' => $validator->errors(),
            ]);
        } else {
            DB::beginTransaction();
            try {

                foreach ($hocky as $hk) {
                    $hk->delete();
                }


                $namhoc->manamhoc = $request->manamhoc;
                $namhoc->save();

                $hocky = new HocKy_Model;
                $hocky->mahocky = 'Học Kỳ I';
                $hocky->tgbatdau = $request->tgbatdau_hk1;
                $hocky->tgketthuc = $request->tgketthuc_hk1;
                $hocky->id_namhoc = $namhoc->id;
                $hocky->save();

                $hocky2 = new HocKy_Model;
                $hocky2->mahocky = 'Học Kỳ II';
                $hocky2->tgbatdau = $request->tgbatdau_hk2;
                $hocky2->tgketthuc = $request->tgketthuc_hk2;
                $hocky2->id_namhoc = $namhoc->id;
                $hocky2->save();

                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['err_msg' => 'Không thành công'], 401);
            } catch (QueryException $e) {
                $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
                return response()->json(['maloi' => $maLoi]);
            }

            $message = ['message' => 'Đã cập nhật thông tin năm học và các học kỳ của năm học "' . $namhoc->manamhoc . '"!!!'];

            return response()->json($message, 200);
        }
    }

    public function deleteXoaNamHocHocKy($id)
    {
        $namhoc = NamHoc_Model::find($id);
        $old_name = $namhoc->manamhoc;
        $hocky = HocKy_Model::where('id_namhoc', '=', $namhoc->id)->get();
        DB::beginTransaction();
        try {

            foreach ($hocky as $hk) {
                $hk->delete();
            }

            $namhoc->delete();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
            return response()->json(['err_msg' => 'Không thành công'], 401);
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1]; // 1062: Lỗi trùng mã
            return response()->json(['maloi' => $maLoi]);
        }

        $message = ['message' => 'Đã xóa thông tin năm học và các học kỳ của năm học "' . $old_name . '"!!!'];

        return response()->json($message, 200);
    }

    public function getHocKy_TGDG() {
        $hk_nh = DB::table('tbl_hocky')
            ->leftjoin('tbl_namhoc', 'tbl_hocky.id_namhoc', 'tbl_namhoc.id')
            ->leftjoin('tbl_thoigiandanhgia', 'tbl_thoigiandanhgia.id_hocky', 'tbl_hocky.id')
            ->select('tbl_hocky.*', 'tbl_namhoc.manamhoc')
            ->get();

        $response = ['hk' => $hk_nh];

        return response()->json($response, 200);

    }
}
