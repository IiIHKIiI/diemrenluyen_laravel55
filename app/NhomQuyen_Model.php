<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NhomQuyen_Model extends Model
{
    //
    protected $table = 'tbl_nhomquyen';
    public $timestamps = false;
}
