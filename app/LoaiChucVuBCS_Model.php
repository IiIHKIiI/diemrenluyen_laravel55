<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiChucVuBCS_Model extends Model
{
    //
    protected $table = 'tbl_loaichucvu_bcs';
    public $timestamps = false;
}
