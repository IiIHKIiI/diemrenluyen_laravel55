<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThoiGianDanhGia_Model extends Model
{
    //
    protected $table = "tbl_thoigiandanhgia";
    public $timestamps = false;
}
