<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoMon_DonVi_Model extends Model
{
    //

    protected $table = "tbl_bomon_donvi";
    public $timestamps = false;
}
