<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoVanHocTap_Model extends Model
{
    //
    protected $table = "tbl_chitiet_cvht";
    public $timestamps = false;
}
