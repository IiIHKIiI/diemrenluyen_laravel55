<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nganh_Model extends Model
{
    //
    protected $table = 'tbl_nganh';
    public $timestamps = false;
}
