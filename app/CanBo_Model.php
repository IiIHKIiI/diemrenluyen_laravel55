<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CanBo_Model extends Model
{
    //
    protected $table = "tbl_canbo";
    public $timestamps = false;
}
