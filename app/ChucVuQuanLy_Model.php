<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChucVuQuanLy_Model extends Model
{
    //
    protected $table = 'tbl_chucvu_quanly';
    public $timestamps = false;
}
