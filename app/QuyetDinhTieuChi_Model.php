<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuyetDinhTieuChi_Model extends Model
{
    //
    protected $table = "tbl_quyetdinhtieuchi";
    public $timestamps = false;
}
