<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhanQuyen_Model extends Model
{
    //
    protected $table = 'tbl_phanquyen';
    public $timestamps = false;
}
